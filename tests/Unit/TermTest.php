<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

use MiamiOH\RESTng\App;

class TermTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $term;

    private $api;
    private $configObj;

    private $config = array();

    protected function setUp(): void
    {
        $this->config = array(
            'feeDetailCode' => '3956',
            'wasClientUpdateUrl' => 'http://example.com/update?ss=bob&uid=',
        );

        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->configObj = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Config')
            ->setMethods(array('getConfig', 'getDefaultTermCode'))
            ->getMock();

        $this->configObj->method('getConfig')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->term = new \MiamiOH\ProjectsInsurancewaiver\Services\Term();

        $this->term->setConfigObj($this->configObj);

    }

    public function testGetDefaultTerm()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->getMock();

        $this->config = array(
            'campusCodeList' => array('L', 'O'),
            'feeDetailCode' => '3956',
            'studentStatusCodeList' => array('AS'),
            'studentTypeCodeList' => array('N', 'T', 'J', 'C', 'D', 'R', 'E'),
            'defaultTermCode' => '201610',
        );

        $this->configObj->expects($this->once())->method('getDefaultTermCode')
            ->willReturn('201610');

        $this->term->setConfigObj($this->configObj);

        $this->term->setRequest($request);

        $resp = $this->term->getDefaultTerm();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

    }

    public function getConfigMock()
    {
        return $this->config;
    }

}
