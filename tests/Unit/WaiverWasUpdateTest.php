<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

class WaiverWasUpdateTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $waiver;

    private $configObj;
    private $http;

    private $config = array();
    private $data = array();

    private $url = '';

    protected function setUp(): void
    {

        $this->url = '';

        $this->config = array(
            'wasClientUpdateUrl' => 'http://ws/stuins/updateTest.php?extapp=StudentInsurance&ss=bob',
        );

        $this->data = array();

        $this->configObj = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Config')
            ->setMethods(array('getConfig', 'getDefaultTermCode'))
            ->getMock();

        $this->configObj->method('getConfig')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->getMock();

        $this->http = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Http')
            ->setMethods(array('get'))
            ->getMock();

        $this->waiver = new \MiamiOH\ProjectsInsurancewaiver\Services\Waiver();

        $this->waiver->setHttp($this->http);

    }

    public function testUpdateWasUrlAll()
    {

        $this->http->expects($this->once())->method('get')
            ->with($this->callback(array($this, 'updateUrlWith')))
            ->will($this->returnCallback(array($this, 'updateUrlWill')));

        $this->data = array(
            'id' => 'bob',
            'eligible' => 'T',
            'completed' => '20150504',
        );

        $this->waiver->setConfigObj($this->configObj);
        $resp = $this->waiver->updateWas($this->data);

    }

    public function testUpdateWasUrlNone()
    {

        $this->http->expects($this->once())->method('get')
            ->with($this->callback(array($this, 'updateUrlWith')))
            ->will($this->returnCallback(array($this, 'updateUrlWill')));

        $this->data = array(
            'id' => 'bob',
        );

        $this->waiver->setConfigObj($this->configObj);
        $resp = $this->waiver->updateWas($this->data);

    }


    public function testUpdateWasUrlMissingUid()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Missing id for updateWas');

        $this->http->expects($this->never())->method('get')
            ->with($this->callback(array($this, 'updateUrlWith')))
            ->will($this->returnCallback(array($this, 'updateUrlWill')));

        $this->data = array(
            'eligible' => 'T',
            'completed' => '20150504',
        );

        $this->waiver->setConfigObj($this->configObj);
        $resp = $this->waiver->updateWas($this->data);

    }


    public function testUpdateWasArrayError()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('MiamiOH\ProjectsInsurancewaiver\Services\Waiver::updateWas parameter must be an array');

        $this->http->expects($this->never())->method('get')
            ->with($this->callback(array($this, 'updateUrlWith')))
            ->will($this->returnCallback(array($this, 'updateUrlWill')));

        $this->data = '';

        $this->waiver->setConfigObj($this->configObj);
        $resp = $this->waiver->updateWas($this->data);

    }

    public function xtestUpdateWasUrlMissingUrl()
    {
        $this->config = array();

        $this->http->expects($this->never())->method('get')
            ->with($this->callback(array($this, 'updateUrlWith')))
            ->will($this->returnCallback(array($this, 'updateUrlWill')));

        $this->data = array(
            'id' => 'bob',
            'eligible' => 'T',
            'completed' => '20150504',
        );

        $this->waiver->setConfigObj($this->configObj);
        $resp = $this->waiver->updateWas($this->data);

    }

    public function getConfigMock()
    {
        return $this->config;
    }

    public function updateUrlWith($subject)
    {

        // There should be a single '?'
        $this->assertTrue(substr_count($subject, '?') === 1);
        $this->assertTrue(strpos($subject, 'uid=' . $this->data['id']) !== false);

        if (isset($this->data['eligible'])) {
            $this->assertTrue(strpos($subject,
                    'eligible=' . $this->data['eligible']) !== false);
        }

        if (isset($this->data['completed'])) {
            $this->assertTrue(strpos($subject,
                    'completed=' . $this->data['completed']) !== false);
        }

        $this->url = $subject;

        return true;
    }

    public function updateUrlWill()
    {
        return true;
    }
}
