<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

use MiamiOH\RESTng\App;

class ProfileTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $profile;
    private $waiver;

    private $querySql = '';
    private $queryParams = array();
    private $queryParam = '';
    private $queryId = '';
    private $queryBannerId = '';
    private $queryTermCode = '';
    private $queryWaiverStatus = '';
    private $waiverStatusResponse = array();
    private $configObj;

    private $testZipCode = '';

    private $testRecords = array(
        '+11112345' => array(
            'recordCount' => '0'
        ),
        '+11119999' => array(
            'recordCount' => '1'
        ),
    );

    protected function setUp(): void
    {
        $this->config = array("feeDetailCode" => '3956');
        $this->querySql = '';
        $this->queryParams = array();
        $this->queryParam = '';
        $this->waiverStatusResponse = array();

        $this->testZipCode = '12000-1234';

        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->api->method('locationFor')
            ->with($this->callback(array($this, 'locationForNameWith')),
                $this->callback(array($this, 'locationForParamsWith')))
            ->will($this->returnCallback(array($this, 'locationForMock')));

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array(
                'queryall_array',
                'queryfirstrow_assoc',
                'queryfirstcolumn',
                'perform'
            ))
            ->getMock();

        $this->dbh->error_string = '';


        $this->configObj = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Config')
            ->setMethods(array('getConfig', 'getDefaultTermCode'))
            ->getMock();

        $this->configObj->method('getConfig')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->waiver = $this->getMockBuilder('MiamiOH\ProjectsInsurancewaiver\Services\Waiver')
            ->setMethods(array('waiverQuery'))
            ->getMock();


        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->profile = new \MiamiOH\ProjectsInsurancewaiver\Services\Profile();

        $this->profile->setConfigObj($this->configObj);
        $this->profile->setDatabase($db);
        $this->profile->setWaiver($this->waiver);

    }

    public function testGetProfileCollection()
    {

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array(
                $this,
                'queryall_arrayCollectionMock'
            )));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array());

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertGreaterThan(1, count($payload));

        $this->assertRegExp("/spbpers_birth_date, 'MMDDYYYY'/", $this->querySql);

        $first = $payload[0];
        $this->assertEquals(18, count(array_keys($first)),
            'Assert 15 elements in the array');
        $this->assertTrue(array_key_exists('id', $first), 'Assert id exists');
        $this->assertTrue(array_key_exists('firstName', $first),
            'Assert firstName exists');
        $this->assertTrue(array_key_exists('lastName', $first),
            'Assert lastName exists');
        $this->assertTrue(array_key_exists('mi', $first), 'Assert mi exists');
        $this->assertTrue(array_key_exists('bannerId', $first),
            'Assert bannerId exists');
        $this->assertTrue(array_key_exists('emailAddress', $first),
            'Assert emailAddress exists');
        $this->assertTrue(array_key_exists('birthDate', $first),
            'Assert birthDate exists');
        $this->assertTrue(array_key_exists('sex', $first), 'Assert sex exists');
        $this->assertTrue(array_key_exists('address1', $first),
            'Assert address1 exists');
        $this->assertTrue(array_key_exists('address2', $first),
            'Assert address2 exists');
        $this->assertTrue(array_key_exists('city', $first), 'Assert city exists');
        $this->assertTrue(array_key_exists('state', $first), 'Assert state exists');
        $this->assertTrue(array_key_exists('zip', $first), 'Assert zip exists');
        $this->assertTrue(array_key_exists('country', $first),
            'Assert country exists');
        $this->assertTrue(array_key_exists('waiverStatus', $first),
            'Assert waiverStatus exists');
        $this->assertTrue(array_key_exists('termCode', $first),
            'Assert termCode exists');
        $this->assertTrue(array_key_exists('studentType', $first),
            'Assert studentType exists');
        $this->assertTrue(array_key_exists('degree', $first),
            'Assert degree exists');

    }

    public function testGetProfileCollectionWithBannerIdFilter()
    {

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->stringContains('szbuniq_banner_id in ('))
            ->will($this->returnCallback(array(
                $this,
                'queryall_arrayCollectionMock'
            )));

        $this->waiver->expects($this->any())->method('waiverQuery')
            ->will($this->returnCallback(array($this, 'waiverQueryMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())
            ->method('getOptions')
            ->willReturn(array(
                'bannerId' => array('+11119999', '+11118888'),
                'termCode' => '201520'
            ));

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertGreaterThan(1, count($payload));

        $first = $payload[0];
        $this->assertEquals(18, count(array_keys($first)),
            'Assert 15 elements in the array');
        $this->assertTrue(array_key_exists('id', $first), 'Assert id exists');
        $this->assertTrue(array_key_exists('firstName', $first),
            'Assert firstName exists');
        $this->assertTrue(array_key_exists('lastName', $first),
            'Assert lastName exists');
        $this->assertTrue(array_key_exists('mi', $first), 'Assert mi exists');
        $this->assertTrue(array_key_exists('bannerId', $first),
            'Assert bannerId exists');
        $this->assertTrue(array_key_exists('emailAddress', $first),
            'Assert emailAddress exists');
        $this->assertTrue(array_key_exists('birthDate', $first),
            'Assert birthDate exists');
        $this->assertTrue(array_key_exists('sex', $first), 'Assert sex exists');
        $this->assertTrue(array_key_exists('address1', $first),
            'Assert address1 exists');
        $this->assertTrue(array_key_exists('address2', $first),
            'Assert address2 exists');
        $this->assertTrue(array_key_exists('city', $first), 'Assert city exists');
        $this->assertTrue(array_key_exists('state', $first), 'Assert state exists');
        $this->assertTrue(array_key_exists('zip', $first), 'Assert zip exists');
        $this->assertTrue(array_key_exists('country', $first),
            'Assert country exists');
        $this->assertTrue(array_key_exists('waiverStatus', $first),
            'Assert waiverStatus exists');
        $this->assertTrue(array_key_exists('termCode', $first),
            'Assert termCode exists');
        $this->assertTrue(array_key_exists('studentType', $first),
            'Assert studentType exists');
        $this->assertTrue(array_key_exists('degree', $first),
            'Assert degree exists');
    }

    public function testGetProfileModel()
    {

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayModelMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->will($this->returnCallback(array($this, 'returnUID')));

        $request->expects($this->once())->method('getOptions');

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertRegExp("/spbpers_birth_date, 'MMDDYYYY'/", $this->querySql);

        $this->assertEquals(16, count(array_keys($payload)),
            'Assert 15 elements in the array');
        $this->assertTrue(array_key_exists('id', $payload), 'Assert id exists');
        $this->assertTrue(array_key_exists('firstName', $payload),
            'Assert firstName exists');
        $this->assertTrue(array_key_exists('lastName', $payload),
            'Assert lastName exists');
        $this->assertTrue(array_key_exists('bannerId', $payload),
            'Assert bannerId exists');
        $this->assertTrue(array_key_exists('emailAddress', $payload),
            'Assert emailAddress exists');
        $this->assertTrue(array_key_exists('birthDate', $payload),
            'Assert birthDate exists');
        $this->assertTrue(array_key_exists('address1', $payload),
            'Assert address1 exists');
        $this->assertTrue(array_key_exists('address2', $payload),
            'Assert address2 exists');
        $this->assertTrue(array_key_exists('city', $payload), 'Assert city exists');
        $this->assertTrue(array_key_exists('state', $payload),
            'Assert state exists');
        $this->assertTrue(array_key_exists('zip', $payload), 'Assert zip exists');
        $this->assertTrue(array_key_exists('country', $payload),
            'Assert country exists');
        $this->assertTrue(array_key_exists('waiverStatus', $payload),
            'Assert waiverStatus exists');
        $this->assertTrue(array_key_exists('termCode', $payload),
            'Assert termCode exists');
        $this->assertTrue(array_key_exists('studentType', $payload),
            'Assert studentType exists');
        $this->assertTrue(array_key_exists('degree', $payload),
            'Assert degree exists');

    }

    public function testGetProfileModelNotFoundError()
    {

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array(
                $this,
                'queryall_arrayEmptyModelMock'
            )));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->will($this->returnCallback(array($this, 'returnUID')));

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_NOTFOUND, $resp->getStatus());

    }

    public function testGetProfileModelTooManyRecordsError()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Too many records found for unique ID');

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array(
                $this,
                'queryall_arrayCollectionMock'
            )));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->will($this->returnCallback(array($this, 'returnUID')));

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_FAILED, $resp->getStatus());

    }

    public function testGetProfileDegree()
    {
        $this->testDegree = '103';

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayModelMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->will($this->returnCallback(array($this, 'returnUID')));

        $request->expects($this->once())->method('getOptions');

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertEquals($this->testDegree, $payload['degree'],
            'Assert degree is retrieved');

    }

    public function testGetProfileZipPlusFour()
    {
        $this->testZipCode = '12000-1234';

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayModelMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->will($this->returnCallback(array($this, 'returnUID')));

        $request->expects($this->once())->method('getOptions');

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertTrue(strpos($payload['zip'], '-') === false,
            'Assert zip does not contain a hyphen');

    }

    public function testGetProfileZipInternational()
    {
        $this->testZipCode = 'MSR 1110-1350';

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayModelMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->will($this->returnCallback(array($this, 'returnUID')));

        $request->expects($this->once())->method('getOptions');

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertEquals($this->testZipCode, $payload['zip'],
            'Assert zip is not changed');

    }

    public function testGetProfileWithBirthDateFormat()
    {
        $this->testDateFormat = 'YYYYMMDD';

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayModelMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->will($this->returnCallback(array($this, 'returnUID')));

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('birthDateFormat' => $this->testDateFormat));

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertRegExp("/spbpers_birth_date, '" . $this->testDateFormat . "'/",
            $this->querySql);

    }

    public function testGetProfileCollectionWithBirthDateFormat()
    {
        $this->testDateFormat = 'YYYYMMDD';

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array(
                $this,
                'queryall_arrayCollectionMock'
            )));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('birthDateFormat' => $this->testDateFormat));

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertGreaterThan(1, count($payload));

        $this->assertRegExp("/spbpers_birth_date, '" . $this->testDateFormat . "'/",
            $this->querySql);

    }

    public function testGetProfileCollectionWithInvalidBirthDateFormat()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Invalid character in birthDateFormat');

        $this->testDateFormat = 'YYYY\'MMDD';

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(array('birthDateFormat' => $this->testDateFormat));

        $this->profile->setRequest($request);

        $resp = $this->profile->getProfile();

        $payload = $resp->getPayload();

    }

    public function returnUID()
    {
        $this->currentUID = 'smithj';

        return $this->currentUID;
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayCollectionMock()
    {
        $records = array(
            array(
                'id' => 'smithj',
                'first_name' => 'Jane',
                'last_name' => 'Smith',
                'mi' => 'G',
                'banner_id' => '+11119999',
                'email_address' => 'smithj@miamioh.edu',
                'birth_date' => '20041998',
                'sex' => 'F',
                'address_1' => '456 Mayberry',
                'address_2' => 'Suite 3',
                'city' => 'Big City',
                'state' => 'NY',
                'zip' => '12000',
                'country' => 'USA',
                'student_type' => 'I',
                'degree' => '103',
            ),
            array(
                'id' => 'joed',
                'first_name' => 'John',
                'last_name' => 'Doe',
                'mi' => 'D',
                'banner_id' => '+11118888',
                'email_address' => 'doej@miamioh.edu',
                'birth_date' => '09121999',
                'sex' => 'M',
                'address_1' => '123 Main Street',
                'address_2' => '',
                'city' => 'Hometown',
                'state' => 'OH',
                'zip' => '45000',
                'country' => 'USA',
                'student_type' => 'D',
                'degree' => '103',
            ),
        );

        return $records;
    }

    public function queryall_arrayModelMock()
    {
        $records = array(
            array(
                'id' => 'smithj',
                'first_name' => 'Jane',
                'last_name' => 'Smith',
                'banner_id' => '+11119999',
                'email_address' => 'smithj@miamioh.edu',
                'birth_date' => '20041998',
                'address_1' => '456 Mayberry',
                'address_2' => 'Suite 3',
                'city' => 'Big City',
                'state' => 'NY',
                'zip' => $this->testZipCode,
                'country' => 'USA',
                'student_type' => 'I',
                'degree' => '103',
            )
        );

        return $records;
    }

    public function queryall_arrayEmptyModelMock()
    {
        $records = array();

        return $records;
    }

    public function waiverQueryMock()
    {
        $records = array(
            array(
                'id' => 'doej',
                'term_code' => '201510',
                'waiver_status' => 'WA',
                'eligible' => '',
                'activity_date' => '07-MAY-15'
            )
        );

        return $records;
    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryfirstcolumnWithId($subject)
    {
        return $subject === $this->queryId;
    }

    public function queryfirstcolumnWithBannerId($subject)
    {
        return $subject === $this->queryBannerId;
    }

    public function queryfirstcolumnMock()
    {
        return $this->testRecords[$this->queryBannerId]['recordCount'];
    }

    public function performWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function performWithId($subject)
    {
        return $subject === $this->queryId;
    }

    public function performWithTermCode($subject)
    {
        return $subject === $this->queryTermCode;
    }

    public function performWithWaiverStatus($subject)
    {
        return $subject === $this->queryWaiverStatus;
    }

    public function performMock()
    {
        return true;
    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryfirstrow_assocWithParam($subject)
    {
        $this->queryParam = $subject;

        return true;
    }

    public function queryfirstrow_assocWithId($subject)
    {
        return $subject === $this->queryId;
    }

    public function queryfirstrow_assocWithTermCode($subject)
    {
        return $subject === $this->queryTermCode;
    }

    /*public function queryfirstrow_assocModelMock() {
      $this->waiverStatusResponse = array();
      switch ($this->queryParam) {
        case '+11119999':
          $this->waiverStatusResponse = array('waiver_status' => 'Y');
      }

      return $this->waiverStatusResponse;
    }*/

    public function queryfirstrow_assocEmptyRecordMock()
    {
        return \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET;
    }

    public function queryfirstrow_assocRecordMock()
    {
        $result = array('stuins_status' => $this->queryWaiverStatus);

        return $result;
    }

    public function getConfigMock()
    {
        return $this->config;
    }

    public function locationForNameWith($subject)
    {
        $this->locationForNameBeingCalled = $subject;

        return true;
    }

    public function locationForParamsWith($subject)
    {
        $this->locationForParamsBeingCalled = $subject;

        return true;
    }

    public function locationForMock()
    {
        $path = str_replace('.', '/', $this->locationForNameBeingCalled);
        $params = $this->locationForParamsBeingCalled;
        $qsFunction = function ($key) use ($params) {
            return $key . '=' . $params[$key];
        };
        $queryString = implode('&',
            array_map($qsFunction, array_keys($this->locationForParamsBeingCalled)));

        return 'http://example.com/' . $path . ($queryString ? '?' . $queryString : '');
    }

    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName = $subject;

        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs = $subject;

        return true;
    }

    public function callResourceMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();
        $response->setStatus(App::API_OK);

        return $response;
    }

    public function callResourceFailureMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();
        $response->setStatus(App::API_FAILED);
        $response->setPayload(array('data' => array('message', '')));

        return $response;
    }

}
