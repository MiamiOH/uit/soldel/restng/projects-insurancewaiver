<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

use MiamiOH\RESTng\App;

class FeewaiverreversalTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $mockedApp;
    private $configObj;
    private $config = array();


    protected function setUp(): void
    {
        $this->config = array(
            'feeDetailCode' => '3956',
        );

        $this->configObj = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Config')
            ->setMethods(array('getConfig'))
            ->getMock();

        $this->mockedApp = $this->createMock(App::class);

        $this->mockedApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->configObj->method('getConfig')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->waiver = new \MiamiOH\ProjectsInsurancewaiver\Services\Waiver();

        $this->waiver->setConfigObj($this->configObj);
        $this->waiver->setApp($this->mockedApp);
    }

    public function testFeeReversalApplied()
    {

        $this->mockedApp->expects($this->once())->method('callResource')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->config['feewaiverReverseSwitch'] = '1';
        $this->config['feeDetailCodeFall'] = '3956';
        $this->config['feeDetailCodeSpring'] = '395S';
        
        $this->waiver->setConfigObj($this->configObj);

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->getMock();

        $data = ['termCode' => '202310'];
        $resp = $this->waiver->feeReversal($data);

    }

    public function testFeeReversalNotApplied()
    {

        $this->mockedApp->expects($this->never())->method('callResource');

        $this->config['feewaiverReverseSwitch'] = '0';
        $this->waiver->setConfigObj($this->configObj);

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->getMock();

        $data = array();
        $resp = $this->waiver->feeReversal($data);

    }

    public function getConfigMock()
    {
        return $this->config;
    }
}
