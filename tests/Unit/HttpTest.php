<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

class HttpTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $http;

    protected function setUp(): void
    {

        $this->http = new \MiamiOH\ProjectsInsurancewaiver\Services\Http();

    }

    public function testClass()
    {

        $this->assertTrue(isset($this->http));
    }

}
