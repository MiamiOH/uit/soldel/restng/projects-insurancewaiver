<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

use MiamiOH\RESTng\App;

class PopulationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $population;

    private $mockedApp;
    private $configObj;

    private $config = array();


    protected function setUp(): void
    {
        $this->config = array();

        $this->querySql = '';
        $this->queryParams = array();
        $this->resourceBeingCalledName = '';
        $this->resourceBeingCalledArgs = array();
        $this->lastQueryType = '';

        $this->mockedApp = $this->createMock(App::class);

        $this->mockedApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->mockedApp->method('locationFor')
            ->with($this->callback(array($this, 'locationForNameWith')),
                $this->callback(array($this, 'locationForParamsWith')))
            ->will($this->returnCallback(array($this, 'locationForMock')));

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'queryall_list'))
            ->getMock();

        $this->dbh->error_string = '';

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->configObj = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Config')
            ->setMethods(array('getConfig', 'getDefaultTermCode', 'getPlanConfig'))
            ->getMock();

        $this->configObj->method('getConfig')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->population = new \MiamiOH\ProjectsInsurancewaiver\Services\Population();

        $this->population->setDatabase($db);
        $this->population->setApp($this->mockedApp);
    }

    public function testGetPopulationList()
    {

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayContactMock')));

        $this->configObj->expects($this->once())->method('getDefaultTermCode')
            ->willReturn('201610');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions');

        $this->config = array(
            'campusCodeList' => array('L', 'O'),
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'studentStatusCodeList' => array('AS'),
            'currentStudentTypeCodeList' => array('C', 'D', 'R', 'E'),
            'newStudentTypeCodeList' => array('N', 'T', 'J'),
            'courseRegistrationStatusCodes' => array('RE', 'RW'),
            'defaultTermCode' => '201610',
        );

        $this->population->setConfigObj($this->configObj);

        $this->population->setRequest($request);

        $resp = $this->population->getPopulationList();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(1, count($payload));

        $first = $payload[0];
        $this->assertEquals(2, count(array_keys($first)));
        $this->assertTrue(array_key_exists('id', $first));
        $this->assertTrue(array_key_exists('locationFor', $first));

    }

    public function testGetPopulationListWithFullTimeEligible()
    {

        $this->dbh->method('queryall_array')
            ->with($this->stringContains("tbraccd inner join szbuniq on tbraccd_pidm = szbuniq_pidm"))
            ->will($this->returnCallback(array($this, 'queryall_arrayContactMock')));

        $this->configObj->expects($this->once())->method('getDefaultTermCode')
            ->willReturn('201610');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')->willReturn(array('fullTimeEligible' => 'true'));

        $this->config = array(
            'campusCodeList' => array('L', 'O'),
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'studentStatusCodeList' => array('AS'),
            'currentStudentTypeCodeList' => array('C', 'D', 'R', 'E'),
            'newStudentTypeCodeList' => array('N', 'T', 'J'),
            'courseRegistrationStatusCodes' => array('RE', 'RW'),
            'defaultTermCode' => '201610',
        );

        $this->population->setConfigObj($this->configObj);

        $this->population->setRequest($request);

        $resp = $this->population->getPopulationList();
    }


    public function testGetPopulationListWithTermCode()
    {

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayContactMock')));

        $this->configObj->expects($this->never())->method('getDefaultTermCode')
            ->willReturn('201610');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();


        $request->expects($this->once())->method('getOptions')->willReturn(array('termCode' => '201510'));

        $this->config = array(
            'campusCodeList' => array('L', 'O'),
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'studentStatusCodeList' => array('AS'),
            'currentStudentTypeCodeList' => array('C', 'D', 'R', 'E'),
            'newStudentTypeCodeList' => array('N', 'T', 'J'),
            'courseRegistrationStatusCodes' => array('RE', 'RW'),
            'defaultTermCode' => '201610',
        );

        $this->population->setConfigObj($this->configObj);

        $this->population->setRequest($request);

        $resp = $this->population->getPopulationList();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(1, count($payload));

        $first = $payload[0];
        $this->assertEquals(2, count(array_keys($first)));
        $this->assertTrue(array_key_exists('id', $first));
        $this->assertTrue(array_key_exists('locationFor', $first));

    }



    public function testGetPopulationListOfEnrolled()
    {
        $this->dbh->method('queryall_list')
            ->with($this->callback(array($this, 'enrolledQuerySqlWith')),
                $this->callback(array($this, 'enrolledQueryParamsWith')))
            ->will($this->returnCallback(array($this, 'queryall_listEnrolledMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')->willReturn(array('type' => 'enrolled'));
        $this->config = array(
            'campusCodeList' => array('L', 'O'),
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'studentStatusCodeList' => array('AS'),
            'currentStudentTypeCodeList' => array('C', 'D', 'R', 'E'),
            'newStudentTypeCodeList' => array('N', 'T', 'J'),
            'courseRegistrationStatusCodes' => array('RE', 'RW'),
            'defaultTermCode' => '201610',
        );

        $this->population->setConfigObj($this->configObj);
        $this->population->setRequest($request);

        $resp = $this->population->getPopulationList();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(3, count($payload));

    }

    //returns flattened array containing values of "id" elements only
    public function getFlattenedPayload(array $payload)
    {
        $records_list = array();
        foreach ($payload as $records) {
            foreach ($records as $key => $value) {
                $key == "id" ? array_push($records_list, $value) : null;
            }
        }

        return $records_list;
    }


    public function testGetPlanConfig()
    {

        $this->configObj->expects($this->once())->method('getPlanConfig')
            ->will($this->returnCallback(array($this, 'getPlanConfigMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->getMock();

        $this->planConfig = array(
            'aetnaCoverageOption' => 'E',
            'aetnaEffectiveDate' => '20150810',
            'aetnaGroupNumber' => '867929',
            'aetnaLocation' => 'Oxford, OH',
            'aetnaPlanCodeDomestic' => '101',
            'aetnaPlanCodeInternational' => '103',
            'aetnaTermCode' => '36',
            'aetnaTermDate' => '20160809',
        );

        $this->population->setConfigObj($this->configObj);

        $this->population->setRequest($request);

        $resp = $this->population->getPlanConfig();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(8, count($payload));

    }

    public function getConfigMock()
    {
        return $this->config;
    }

    public function getPlanConfigMock()
    {
        return $this->planConfig;
    }

    public function locationForNameWith($subject)
    {
        $this->locationForNameBeingCalled = $subject;

        return true;
    }

    public function locationForParamsWith($subject)
    {
        $this->locationForParamsBeingCalled = $subject;

        return true;
    }

    public function locationForMock()
    {
        $path = str_replace('.', '/', $this->locationForNameBeingCalled);
        $params = $this->locationForParamsBeingCalled;
        $qsFunction = function ($key) use ($params) {
            return $key . '=' . $params[$key];
        };
        $queryString = implode('&',
            array_map($qsFunction, array_keys($this->locationForParamsBeingCalled)));

        return 'http://example.com/' . $path . ($queryString ? '?' . $queryString : '');
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }


    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayContactMock()
    {
        $records = array(
            array(
                'id' => 'doej',
            ),
        );

        return $records;
    }

    public function queryall_arrayContactEmailMock()
    {
        $records = array(
            array(
                'id' => 'doej',
                'email_address' => 'doej@example.com',
            ),
        );

        return $records;
    }

    public function enrolledQuerySqlWith($subject)
    {
        $this->lastQueryType = strpos($subject,
            'stuins_status is null') ? 'nonActor' : 'enrolled';

        return true;
    }

    public function enrolledQueryParamsWith($subject)
    {
        return true;
    }

    public function queryall_listEnrolledMock()
    {
        $records = array(
            'smithr',
            'howardj',
            'doej',
        );

        return $records;
    }

    public function queryall_arrayChargedMock()
    {
        $records = array(
            array(
                'id' => 'doej',
            ),
            array(
                'id' => 'fishb',
            ),
            array(
                'id' => 'smithr',
            ),
        );

        return $records;
    }

    public function sampleDateProvider()
    {
        return array(
            array(20160801, 20160801, 3),
        );
    }


}
