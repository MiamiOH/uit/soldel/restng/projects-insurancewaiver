<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

use Exception;
use MiamiOH\RESTng\App;

class ConfigTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $configObj;

    private $defaultTermCode = '';
    private $configData = array();
    private $mockedApp;

    private $dbh;

    protected function setUp(): void
    {

        $this->defaultTermCode = '';
        $this->configData = array();

        $this->mockedApp = $this->createMock(App::class);

        $this->dbh = $this->createMock('\MiamiOH\RESTng\Legacy\DB\DBH');

        $db = $this->createMock('\MiamiOH\RESTng\Connector\DatabaseFactory');

        $db->method('getHandle')->willReturn($this->dbh);

        $this->configObj = new \MiamiOH\ProjectsInsurancewaiver\Services\Config();

        $this->configObj->setApp($this->mockedApp);
        $this->configObj->setDatabase($db);

    }

    public function testGetConfig()
    {

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'campusCodeList' => "L,O",
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
            'wasClientUpdateUrl' => 'http://example.com',
            'nonActorEnrollmentStartDate' => '2016-08-01',
            'lateChargeStartDate' => '2016-08-01',
            'feewaiverReverseSwitch' => '1',
        );

        $config = $this->configObj->getConfig();

        $this->assertTrue(is_array($config));
        $this->assertEquals(count($this->configData), count($config));
        $this->assertEquals($this->configData['defaultTermCode'],
            $config['defaultTermCode']);
        $this->assertEquals($this->configData['feeDetailCodeFall'],
            $config['feeDetailCodeFall']);
        $this->assertEquals($this->configData['feeDetailCodeSpring'],
            $config['feeDetailCodeSpring']);
        $this->assertEquals($this->configData['notifyFrom'], $config['notifyFrom']);
        $this->assertEquals($this->configData['notifySubject'],
            $config['notifySubject']);
        $this->assertEquals($this->configData['notifyBody'], $config['notifyBody']);
        $this->assertEquals($this->configData['wasClientUpdateUrl'],
            $config['wasClientUpdateUrl']);
        $this->assertEquals(explode(',', $this->configData['campusCodeList']),
            $config['campusCodeList']);
        $this->assertEquals(explode(',', $this->configData['studentStatusCodeList']),
            $config['studentStatusCodeList']);
        $this->assertEquals(explode(',',
            $this->configData['currentStudentTypeCodeList']),
            $config['currentStudentTypeCodeList']);
        $this->assertEquals(explode(',',
            $this->configData['newStudentTypeCodeList']),
            $config['newStudentTypeCodeList']);
    }

    public function testGetDefaultTermCodeFromConfig()
    {

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';

        $this->dbh->expects($this->never())->method('queryfirstcolumn')->willReturn($this->defaultTermCode);

        $this->configData = array(
            'campusCodeList' => "L,O",
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
            'wasClientUpdateUrl' => 'http://example.com',
        );

        $termCode = $this->configObj->getDefaultTermCode();

        $this->assertEquals($this->defaultTermCode, $termCode);

    }

    public function testGetDefaultTermCodeFromDb()
    {

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '';
        $dbTerm = '201610';

        $this->dbh->expects($this->once())->method('queryfirstcolumn')->willReturn($dbTerm);

        $this->configData = array(
            'campusCodeList' => "L,O",
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
            'wasClientUpdateUrl' => 'http://example.com',
            'fullTimeHours' => "12",
            'calcRegistrationHoursWhichTotal' => "TOTAL",
            'calcRegistrationHoursWhichField' => "CREDIT",
            'healthCareProvider' => 'aetna',
        );

        $termCode = $this->configObj->getDefaultTermCode();

        $this->assertEquals($dbTerm, $termCode);

    }

    public function testGetConfigMissingCourseRegistrationStatusCodes()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for courseRegistrationStatusCodes");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '202210';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingFeeDetailCodeFall()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for feeDetailCodeFall");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }

    public function testGetConfigMissingFeeDetailCodeSpring()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for feeDetailCodeSpring");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingCampusCodeList()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for campusCodeList");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'off',
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingStudentStatusCodeList()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for studentStatusCodeList");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'true',
            'campusCodeList' => "L,O",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingCurrentStudentTypeCodeListList()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for currentStudentTypeCodeList");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingNewStudentTypeCodeListList()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for newStudentTypeCodeList");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingNotifyFrom()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for notifyFrom");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'newStudentTypeCodeList' => "N,T,J",
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingNotifySubject()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for notifySubject");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'newStudentTypeCodeList' => "N,T,J",
            'notifyFrom' => 'test@miamioh.edu',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingNotifyBody()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for notifyBody");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'newStudentTypeCodeList' => "N,T,J",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notificationOn' => 'on',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }


    public function testGetConfigMissingNotificationOn()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Missing config value for notificationOn");

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'newStudentTypeCodeList' => "N,T,J",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test content',
            'campusCodeList' => "L,O",
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
        );

        $config = $this->configObj->getConfig();
    }

    public function testGetPlanConfig()
    {

        $this->mockedApp->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->defaultTermCode = '201610';
        $this->configData = array(
            'campusCodeList' => "L,O",
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test email body',
            'notificationOn' => 'on',
            'studentStatusCodeList' => "AS",
            'currentStudentTypeCodeList' => "C,D,R,E",
            'newStudentTypeCodeList' => "N,T,J",
            'courseRegistrationStatusCodes' => "RE,RW",
            'defaultTermCode' => $this->defaultTermCode,
            'wasClientUpdateUrl' => 'http://example.com',
            'UHperiodCode' => 'A-',
            'UHpolicyNumber' => '2024-109-1',
            'UHtermCodeYear' => '2025'
        );

        $config = $this->configObj->getPlanConfig();

        $this->assertTrue(is_array($config));
        $this->assertEquals(3, count($config));

        $this->assertEquals($this->configData['UHperiodCode'],
            $config['UHperiodCode']);
        $this->assertEquals($this->configData['UHpolicyNumber'],
            $config['UHpolicyNumber']);
        $this->assertEquals($this->configData['UHtermCodeYear'],
            $config['UHtermCodeYear']);
    }


    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName = $subject;

        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs = $subject;

        return true;
    }

    public function callResourceMock()
    {
        if ($this->resourceBeingCalledName === 'config.v1') {
            $response = new \MiamiOH\RESTng\Util\Response();
            $response->setPayload($this->configData);

            return $response;
        }

        return null;
    }

}
