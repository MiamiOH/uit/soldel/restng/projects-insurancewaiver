<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

use MiamiOH\RESTng\App;

class WaiverCollectionUpdateTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $waiver;

    private $mockedApp;
    private $configObj;
    private $http;

    private $config = array();

    private $querySql = '';
    private $queryId = '';
    private $queryTermCode = '';

    private $sampleStudentIds = array(
        'DAVISD' => array(
            'bannerId' => '+11120000',
            'pidm' => 11120000,
            'status' => '',
            'eligible' => 1,
            'termCode' => '201610',
        ),
        'HOWARDJ' => array(
            'bannerId' => '+11120001',
            'pidm' => 11120001,
            'status' => '',
            'eligible' => 1,
            'termCode' => '201610',
        ),
        'JONESJ' => array(
            'bannerId' => '+11120002',
            'pidm' => 11120002,
            'status' => '',
            'eligible' => 1,
            'termCode' => '201610',
        ),
    );

    protected function setUp(): void
    {
        $this->config = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'wasClientUpdateUrl' => 'http://example.com/update?ss=bob&uid=',
            'feewaiverReverseSwitch' => '1',
        );

        $this->querySql = '';
        $this->queryId = '';
        $this->queryTermCode = '';

        $this->mockedApp = $this->createMock(App::class);

        $this->mockedApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array(
                'queryall_array',
                'queryfirstcolumn',
                'queryfirstrow_assoc',
                'perform'
            ))
            ->getMock();

        $this->dbh->error_string = '';

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->configObj = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Config')
            ->setMethods(array('getConfig', 'getDefaultTermCode'))
            ->getMock();

        $this->configObj->method('getConfig')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->http = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Http')
            ->setMethods(array('get'))
            ->getMock();

        $this->waiver = new \MiamiOH\ProjectsInsurancewaiver\Services\Waiver();

        $this->waiver->setConfigObj($this->configObj);
        $this->waiver->setDatabase($db);
        $this->waiver->setHttp($this->http);
        $this->waiver->setApp($this->mockedApp);

    }

    public function testUpdateWaiverCollectionSingle()
    {

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->will($this->returnCallback(array(
                $this,
                'queryfirstrow_assocRecordMock'
            )));

        // $this->dbh->expects($this->once())->method('perform')
        //   ->with($this->callback(array($this, 'performWithQuery')),
        //       $this->callback(array($this, 'performWithId')),
        //       $this->callback(array($this, 'performWithTermCode')),
        //       $this->callback(array($this, 'performWithEmptyValue')),
        //       $this->callback(array($this, 'performWithEligible')))
        //   ->will($this->returnCallback(array($this, 'performMock')));

        $this->mockedApp->expects($this->once())->method('callResource')
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $testCollection = array(
            array(
                'id' => 'DAVISD',
                'termCode' => '201610',
                'waiverStatus' => 'WA',
                'bannerId' => '+11120000',
            ),
        );

        $request->expects($this->once())->method('getData')->willReturn($testCollection);

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($testCollection), count($payload));
        $this->assertTrue(array_key_exists($testCollection[0]['id'], $payload));
        $this->assertEquals(App::API_OK,
            $payload[$testCollection[0]['id']]['code']);

    }

    public function testUpdateWaiverCollectionMultiple()
    {

        $this->dbh->expects($this->exactly(3))->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->exactly(3))->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->will($this->returnCallback(array(
                $this,
                'queryfirstrow_assocRecordMock'
            )));

        // $this->dbh->expects($this->once())->method('perform')
        //   ->with($this->callback(array($this, 'performWithQuery')),
        //       $this->callback(array($this, 'performWithId')),
        //       $this->callback(array($this, 'performWithTermCode')),
        //       $this->callback(array($this, 'performWithEmptyValue')),
        //       $this->callback(array($this, 'performWithEligible')))
        //   ->will($this->returnCallback(array($this, 'performMock')));

        $this->mockedApp->expects($this->exactly(3))->method('callResource')
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $testCollection = array(
            array(
                'id' => 'DAVISD',
                'termCode' => '201610',
                'waiverStatus' => 'WA',
                'bannerId' => '+11120000',
            ),
            array(
                'id' => 'HOWARDJ',
                'termCode' => '201610',
                'waiverStatus' => 'WA',
                'bannerId' => '+11120001',
            ),
            array(
                'id' => 'JONESJ',
                'termCode' => '201610',
                'waiverStatus' => 'WA',
                'bannerId' => '+11120002',
            ),
        );

        $request->expects($this->once())->method('getData')->willReturn($testCollection);

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($testCollection), count($payload));
        for ($i = 0; $i < count($testCollection); $i++) {
            $this->assertTrue(array_key_exists($testCollection[$i]['id'], $payload));
            $this->assertEquals(App::API_OK,
                $payload[$testCollection[$i]['id']]['code']);
        }

    }

    public function testUpdateWaiverCollectionPartialFail()
    {

        $this->dbh->expects($this->exactly(3))->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->exactly(2))->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->will($this->returnCallback(array(
                $this,
                'queryfirstrow_assocRecordMock'
            )));

        // $this->dbh->expects($this->once())->method('perform')
        //   ->with($this->callback(array($this, 'performWithQuery')),
        //       $this->callback(array($this, 'performWithId')),
        //       $this->callback(array($this, 'performWithTermCode')),
        //       $this->callback(array($this, 'performWithEmptyValue')),
        //       $this->callback(array($this, 'performWithEligible')))
        //   ->will($this->returnCallback(array($this, 'performMock')));

        $this->mockedApp->expects($this->exactly(2))->method('callResource')
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $testCollection = array(
            array(
                'id' => 'DAVISD',
                'termCode' => '201610',
                'waiverStatus' => 'WA',
                'bannerId' => '+11120000',
            ),
            array(
                'id' => 'HOWARDX',
                'termCode' => '201610',
                'waiverStatus' => 'WA',
                'bannerId' => '+11120001',
            ),
            array(
                'id' => 'JONESJ',
                'termCode' => '201610',
                'waiverStatus' => 'WA',
                'bannerId' => '+11120002',
            ),
        );

        $request->expects($this->once())->method('getData')->willReturn($testCollection);

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(count($testCollection), count($payload));
        for ($i = 0; $i < count($testCollection); $i++) {
            $this->assertTrue(array_key_exists($testCollection[$i]['id'], $payload));
            if ($testCollection[$i]['id'] === 'HOWARDX') {
                $this->assertEquals(App::API_FAILED,
                    $payload[$testCollection[$i]['id']]['code']);
            } else {
                $this->assertEquals(App::API_OK,
                    $payload[$testCollection[$i]['id']]['code']);
            }
        }

    }


    public function testUpdateWaiverCollectionError()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Data for MiamiOH\ProjectsInsurancewaiver\Services\Waiver::updateWaiverCollection must be an array');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')->willReturn('');

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_FAILURE, $resp->getStatus());

    }

    public function getConfigMock()
    {
        return $this->config;
    }

    public function callResourceMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();
        $response->setStatus(App::API_OK);

        return $response;
    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryfirstcolumnWithId($subject)
    {
        $this->queryId = $subject;

        return true;
    }

    public function queryfirstcolumnMock()
    {
        if (isset($this->sampleStudentIds[$this->queryId])) {
            return $this->sampleStudentIds[$this->queryId]['bannerId'];
        }

        return null;
    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryfirstrow_assocWithId($subject)
    {
        $this->queryId = $subject;

        return true;
    }

    public function queryfirstrow_assocWithTermCode($subject)
    {
        $this->queryTermCode = $subject;

        return true;
    }

    public function queryfirstrow_assocRecordMock()
    {
        if (!isset($this->sampleStudentIds[$this->queryId])) {
            return \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET;
        }

        $result = array(
            'stuins_pidm' => $this->sampleStudentIds[$this->queryId]['pidm'],
            'stuins_termcode' => $this->sampleStudentIds[$this->queryId]['termCode'],
            'stuins_status' => $this->sampleStudentIds[$this->queryId]['status'],
            'stuins_eligible' => $this->sampleStudentIds[$this->queryId]['eligible'],
            'stuins_activity_date' => '07-MAY-15'
        );

        return $result;
    }

}
