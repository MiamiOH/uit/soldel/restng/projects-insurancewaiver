<?php

namespace MiamiOH\ProjectsInsurancewaiver\Tests\Unit;

use MiamiOH\RESTng\App;

class WaiverTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $population;

    private $mockedApp;
    private $configObj;
    private $http;

    private $config = array();

    private $querySql = '';
    private $queryParams = array();
    private $queryId = '';
    private $queryPidm = '';
    private $queryTermCode = '';
    private $queryWaiverStatus = '';
    private $queryEligible = '';
    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();
    private $insert = false;
    private $update = false;
    private $hasData = false;

    protected function setUp(): void
    {
        $this->config = array(
            'feeDetailCodeFall' => "3956",
            'feeDetailCodeSpring' => "395S",
            'notifyFrom' => 'test@miamioh.edu',
            'notifySubject' => 'test subject',
            'notifyBody' => 'test body',
            'notificationOn' => 'on',
            'wasClientUpdateUrl' => 'http://example.com/update?ss=bob&uid=',
            'feewaiverReverseSwitch' => '1',
        );

        $this->querySql = '';
        $this->queryParams = array();
        $this->resourceBeingCalledName = '';
        $this->resourceBeingCalledArgs = array();

        $this->mockedApp = $this->createMock(App::class);

        $this->mockedApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->mockedApp->method('locationFor')
            ->with($this->callback(array($this, 'locationForNameWith')),
                $this->callback(array($this, 'locationForParamsWith')))
            ->will($this->returnCallback(array($this, 'locationForMock')));

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array(
                'queryall_array',
                'queryfirstcolumn',
                'queryfirstrow_assoc',
                'perform'
            ))
            ->getMock();

        $this->dbh->error_string = '';

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->configObj = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Config')
            ->setMethods(array('getConfig', 'getDefaultTermCode'))
            ->getMock();

        $this->configObj->method('getConfig')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->http = $this->getMockBuilder('\MiamiOH\ProjectsInsurancewaiver\Services\Http')
            ->setMethods(array('get'))
            ->getMock();

        $this->population = $this->getMockBuilder('MiamiOH\ProjectsInsurancewaiver\Services\Population')
            ->setMethods(array('getEligibilityQuery'))
            ->getMock();

        $this->waiver = new \MiamiOH\ProjectsInsurancewaiver\Services\Waiver();

        $this->waiver->setConfigObj($this->configObj);
        $this->waiver->setDatabase($db);
        $this->waiver->setHttp($this->http);
        $this->waiver->setApp($this->mockedApp);
        $this->waiver->setPopulation($this->population);

    }

    public function testGetWaiver()
    {

        $this->queryTermCode = '201610';
        $this->queryId = 'smitht2';

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')->willReturn(array('termCode' => $this->queryTermCode));

        $request->expects($this->once())->method('getResourceParam')->with($this->equalTo('id'))->willReturn($this->queryId);

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayModelMock')));

        $this->waiver->setRequest($request);

        $resp = $this->waiver->getWaiver();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertEquals(5, count(array_keys($payload)),
            'Assert 5 elements in the array');
        $this->assertTrue(array_key_exists('id', $payload), 'Assert id exists');
        $this->assertTrue(array_key_exists('termCode', $payload),
            'Assert termCode exists');
        $this->assertTrue(array_key_exists('waiverStatus', $payload),
            'Assert waiverStatus exists');
        $this->assertTrue(array_key_exists('activityDate', $payload),
            'Assert activityDate exists');
        $this->assertTrue(array_key_exists('eligible', $payload),
            'Assert eligible exists');

    }

    public function testGetWaiverRecordNotFound()
    {

        $this->queryTermCode = '201610';
        $this->queryId = 'smitht2';

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')->willReturn(array('termCode' => $this->queryTermCode));

        $request->expects($this->once())->method('getResourceParam')->with($this->equalTo('id'))->willReturn($this->queryId);

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayEmptyMock')));

        $this->waiver->setRequest($request);

        $resp = $this->waiver->getWaiver();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_NOTFOUND, $resp->getStatus());

    }


    public function testGetWaiverTooManyRecords()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Too many records found for unique ID');

        $this->queryTermCode = '201610';
        $this->queryId = 'smitht2';

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions');

        $request->expects($this->once())->method('getResourceParam')->with($this->equalTo('id'))->willReturn($this->queryId);

        $this->configObj->expects($this->once())->method('getDefaultTermCode')
            ->willReturn($this->queryTermCode);

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array(
                $this,
                'queryall_arrayCollectionMock'
            )));

        $this->waiver->setRequest($request);

        $resp = $this->waiver->getWaiver();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_FAILED, $resp->getStatus());

    }

    public function testGetWaiverList()
    {

        $this->queryTermCode = '201610';

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array(
                $this,
                'queryall_arrayCollectionMock'
            )));


        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions');

        $this->configObj->expects($this->once())->method('getDefaultTermCode')
            ->willReturn($this->queryTermCode);

        $this->waiver->setRequest($request);

        $resp = $this->waiver->getWaiverList();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertGreaterThan(1, count($payload));

        $first = $payload[0];
        $this->assertEquals(5, count(array_keys($first)),
            'Assert 5 elements in the array');
        $this->assertTrue(array_key_exists('id', $first), 'Assert pidm exists');
        $this->assertTrue(array_key_exists('termCode', $first),
            'Assert termCode exists');
        $this->assertTrue(array_key_exists('waiverStatus', $first),
            'Assert waiverStatus exists');
        $this->assertTrue(array_key_exists('activityDate', $first),
            'Assert activityDate exists');
        $this->assertTrue(array_key_exists('eligible', $first),
            'Assert eligible exists');

    }

    public function testGetWaiverListWithTermCode()
    {

        $this->queryTermCode = '201610';

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array(
                $this,
                'queryall_arrayCollectionMock'
            )));


        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')->willReturn(array('termCode' => $this->queryTermCode));

        $this->waiver->setRequest($request);

        $resp = $this->waiver->getWaiverList();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertGreaterThan(1, count($payload));

        $first = $payload[0];
        $this->assertEquals(5, count(array_keys($first)),
            'Assert 5 elements in the array');
        $this->assertTrue(array_key_exists('id', $first), 'Assert pidm exists');
        $this->assertTrue(array_key_exists('termCode', $first),
            'Assert termCode exists');
        $this->assertTrue(array_key_exists('waiverStatus', $first),
            'Assert waiverStatus exists');
        $this->assertTrue(array_key_exists('activityDate', $first),
            'Assert activityDate exists');
        $this->assertTrue(array_key_exists('eligible', $first),
            'Assert eligible exists');

    }


    public function testupdateWaiverModelMissingID()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('MiamiOH\ProjectsInsurancewaiver\Services\Waiver::updateWaiverModel requires id in data body');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'waiverStatus' => 'WA',
                'termCode' => '201610',
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

    }


    public function testupdateWaiverModelMissingTermCode()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('MiamiOH\ProjectsInsurancewaiver\Services\Waiver::updateWaiverModel requires termCode in data body');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'waiverStatus' => 'WA',
                'id' => 'smithj',
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

    }


    public function testupdateWaiverModelMissingWaiverStatusAndEligible()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('MiamiOH\ProjectsInsurancewaiver\Services\Waiver::updateWaiverModel requires either waiverStatus or eligible in data body');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->onlyMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'termCode' => '201610',
                'id' => 'smithj',
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

    }


    public function testUpdateWaiverIdMismatch()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('ID in path does not match ID in body');


        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = 'WA';

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn('abcdef');

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'id' => $this->queryId,
                'termCode' => $this->queryTermCode,
                'waiverStatus' => $this->queryWaiverStatus,
                'bannerId' => $this->queryBannerId
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

    }


    public function testUpdateWaiverBannerIdNotFoundError()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Error retrieving BannerId');

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = 'WA';

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnEmptyMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'id' => $this->queryId,
                'termCode' => $this->queryTermCode,
                'waiverStatus' => $this->queryWaiverStatus,
                'bannerId' => $this->queryBannerId
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

    }


    public function testUpdateWaiverBannerIdMismatchError()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('BannerId does not match record of given uniqueId');

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = 'WA';

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->willReturn('+11110000');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'id' => $this->queryId,
                'termCode' => $this->queryTermCode,
                'waiverStatus' => $this->queryWaiverStatus,
                'bannerId' => $this->queryBannerId
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

    }

    public function testUpdateWaiverInsertNewRecordStatus()
    {

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = 'WA';
        $this->queryEligible = '1';

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->will($this->returnCallback(array(
                $this,
                'queryfirstrow_assocEmptyRecordMock'
            )));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithId')),
                $this->callback(array($this, 'performWithTermCode')),
                $this->callback(array($this, 'performWithWaiverStatus')),
                $this->callback(array($this, 'performWithEmptyValue')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->mockedApp->expects($this->once())->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->http->expects($this->once())->method('get');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'termCode' => $this->queryTermCode,
                'waiverStatus' => $this->queryWaiverStatus,
                'id' => $this->queryId,
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function testUpdateWaiverInsertNewRecordEligible()
    {

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = '';
        $this->queryEligible = '1';

        $this->dbh->expects($this->any())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->any())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->will($this->returnCallback(array(
                $this,
                'queryfirstrow_assocRecordMock'
            )));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithId')),
                $this->callback(array($this, 'performWithTermCode')),
                $this->callback(array($this, 'performWithEmptyValue')),
                $this->callback(array($this, 'performWithEligible')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->mockedApp->expects($this->once())->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceCreateMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'termCode' => $this->queryTermCode,
                'eligible' => $this->queryEligible,
                'id' => $this->queryId,
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    /*  public function testUpdateWaiverInsertNewRecordEligibleNoEmail() {

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = '';
        $this->queryEligible = '1';

        $this->dbh->expects($this->any())->method('queryfirstcolumn')
          ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
            $this->callback(array($this, 'queryfirstcolumnWithId')))
          ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->any())->method('queryfirstrow_assoc')
          ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
              $this->callback(array($this, 'queryfirstrow_assocWithId')),
              $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
          ->will($this->returnCallback(array($this, 'queryfirstrow_assocRecordMock')));

        $this->dbh->expects($this->once())->method('perform')
          ->with($this->callback(array($this, 'performWithQuery')),
              $this->callback(array($this, 'performWithId')),
              $this->callback(array($this, 'performWithTermCode')),
              $this->callback(array($this, 'performWithEmptyValue')),
              $this->callback(array($this, 'performWithEligible')))
          ->will($this->returnCallback(array($this, 'performMock')));

        $this->api->expects($this->never())->method('callResource');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
                ->setMethods(array('getResourceParam','getData'))
                ->getMock();

        $request->expects($this->once())->method('getResourceParam')
                ->with($this->equalTo('id'))
                ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
              'termCode' => $this->queryTermCode,
              'eligible' => $this->queryEligible,
              'id' => $this->queryId,
              )
          );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

      }*/

    public function testUpdateWaiverModelAlreadyExistNoChange()
    {

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryPidm = '11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = 'WA';
        $this->queryEligible = '1';
        $this->hasData = true;

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->will($this->returnCallback(array(
                $this,
                'queryfirstrow_assocRecordMock'
            )));

        $this->dbh->expects($this->never())->method('perform');

        $this->mockedApp->expects($this->once())->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $this->http->expects($this->never())->method('get');

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'termCode' => $this->queryTermCode,
                'waiverStatus' => $this->queryWaiverStatus,
                'id' => $this->queryId,
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function testUpdateWaiverModelAlreadyExistWithChangeStatus()
    {

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryPidm = '11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = 'E';
        $this->queryEligible = '1';
        $this->hasData = true;

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->willReturn(array(
                'stuins_status' => 'WA',
                'stuins_eligible' => $this->queryEligible,
                'stuins_pidm' => $this->queryPidm,
                'stuins_termcode' => $this->queryTermCode
            ));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithWaiverStatus')),
                $this->callback(array($this, 'performWithEligible')),
                $this->callback(array($this, 'performWithPidm')),
                $this->callback(array($this, 'performWithTermCode')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $this->http->expects($this->once())->method('get');

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'termCode' => $this->queryTermCode,
                'waiverStatus' => $this->queryWaiverStatus,
                'id' => $this->queryId,
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function testUpdateWaiverModelAlreadyExistWithChangeEligible()
    {

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11112345';
        $this->queryPidm = '11112345';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = 'E';
        $this->queryEligible = '1';
        $this->hasData = true;

        $this->dbh->expects($this->any())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->any())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->willReturn(array(
                'stuins_status' => $this->queryWaiverStatus,
                'stuins_eligible' => '0',
                'stuins_pidm' => $this->queryPidm,
                'stuins_termcode' => $this->queryTermCode
            ));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithWaiverStatus')),
                $this->callback(array($this, 'performWithEligible')),
                $this->callback(array($this, 'performWithPidm')),
                $this->callback(array($this, 'performWithTermCode')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->mockedApp->expects($this->never())->method('callResource');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $this->http->expects($this->once())->method('get');

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'termCode' => $this->queryTermCode,
                'eligible' => $this->queryEligible,
                'id' => $this->queryId,
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function testUpdateWaiverInsertNewRecordWaiveReverseError()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Failed to update fee waiver');

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11119999';
        $this->queryPidm = '11119999';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = 'WA';

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->will($this->returnCallback(array(
                $this,
                'queryfirstrow_assocEmptyRecordMock'
            )));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithId')),
                $this->callback(array($this, 'performWithTermCode')),
                $this->callback(array($this, 'performWithWaiverStatus')),
                $this->callback(array($this, 'performWithEligible')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->mockedApp->expects($this->once())->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceFailureMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'id' => $this->queryId,
                'termCode' => $this->queryTermCode,
                'waiverStatus' => $this->queryWaiverStatus,
                'bannerId' => $this->queryBannerId
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_FAILED, $resp->getStatus());
    }


    public function testUpdateWaiverInsertNewRecordEligibleReverseError()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Notification request failed: 500');

        $this->queryId = 'smithj';
        $this->queryBannerId = '+11119999';
        $this->queryPidm = '11119999';
        $this->queryTermCode = '201610';
        $this->queryWaiverStatus = '';
        $this->queryEligible = '1';
        //  $this->feeReversalStartDate = '20160801';

        //  $this->config['feeReversalStartDate']->setConfigObj($this->configObj);


        $this->dbh->expects($this->any())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithId')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->any())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithId')),
                $this->callback(array($this, 'queryfirstrow_assocWithTermCode')))
            ->will($this->returnCallback(array(
                $this,
                'queryfirstrow_assocRecordMock'
            )));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithId')),
                $this->callback(array($this, 'performWithTermCode')),
                $this->callback(array($this, 'performWithWaiverStatus')),
                $this->callback(array($this, 'performWithEligible')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->mockedApp->expects($this->once())->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceFailureMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'id' => $this->queryId,
                'termCode' => $this->queryTermCode,
                'eligible' => $this->queryEligible,
                'bannerId' => $this->queryBannerId
            )
        );

        $this->waiver->setRequest($request);

        $resp = $this->waiver->updateWaiverModel();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_FAILURE, $resp->getStatus());
    }


    public function getConfigMock()
    {
        return $this->config;
    }

    public function locationForNameWith($subject)
    {
        $this->locationForNameBeingCalled = $subject;

        return true;
    }

    public function locationForParamsWith($subject)
    {
        $this->locationForParamsBeingCalled = $subject;

        return true;
    }

    public function locationForMock()
    {
        $path = str_replace('.', '/', $this->locationForNameBeingCalled);
        $params = $this->locationForParamsBeingCalled;
        $qsFunction = function ($key) use ($params) {
            return $key . '=' . $params[$key];
        };
        $queryString = implode('&',
            array_map($qsFunction, array_keys($this->locationForParamsBeingCalled)));

        return 'http://example.com/' . $path . ($queryString ? '?' . $queryString : '');
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryall_arrayWithQueryFullTimeEligible($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayWithTermCode($subject)
    {
        return $subject === $this->queryTermCode;
    }

    public function queryall_arrayCollectionMock()
    {
        $records = array(
            array(
                'id' => 'smithr',
                'termCode' => '201510',
                'waiverStatus' => 'WA',
                'activityDate' => '07-MAY-15',
                'eligible' => '1',
            ),
            array(
                'id' => 'smith2',
                'termCode' => '201510',
                'waiverStatus' => 'E',
                'activityDate' => '07-MAY-15',
                'eligible' => '0',
            ),
        );

        return $records;
    }

    public function queryall_arrayModelMock()
    {
        $records = array(
            array(
                'id' => $this->queryId,
                'termCode' => $this->queryTermCode,
                'waiverStatus' => 'WA',
                'activityDate' => '07-MAY-15',
                'eligible' => '1',
            ),
        );

        return $records;
    }

    public function queryall_arrayEmptyMock()
    {
        $records = array();

        return $records;
    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryfirstcolumnWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryfirstcolumnWithId($subject)
    {
        return $subject === $this->queryId;
    }

    public function queryfirstcolumnEmptyMock()
    {
        return \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET;
    }

    public function queryfirstcolumnMock()
    {
        if (strpos($this->querySql, 'sgbstdn')) {
            return $this->queryId;
        } else {
            if (strpos($this->querySql, 'gzkemail')) {
                return $this->queryId . '@miamioh.edu';
            } else {
                return $this->queryBannerId;
            }
        }
    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->querySql = $subject;

        return true;
    }

    public function queryfirstrow_assocWithId($subject)
    {
        return $subject === $this->queryId;
    }

    public function queryfirstrow_assocWithTermCode($subject)
    {
        return $subject === $this->queryTermCode;
    }

    public function queryfirstrow_assocEmptyRecordMock()
    {
        return \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET;
    }

    public function queryfirstrow_assocRecordMock()
    {
        if ($this->insert || $this->update || $this->hasData) {
            $result = array(
                'stuins_pidm' => $this->queryPidm,
                'stuins_termcode' => $this->queryTermCode,
                'stuins_status' => $this->queryWaiverStatus,
                'stuins_eligible' => $this->queryEligible,
                'stuins_activity_date' => '07-MAY-15'
            );

            return $result;
        } else {
            return \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET;
        }
    }

    public function performWithQuery($subject)
    {
        if (strpos($subject, 'insert')) {
            $this->update = false;
            $this->hasData = true;
        } else {
            if (strpos($subject, 'update')) {
                $this->insert = true;
                $this->hasData = true;
            }
        }
        $this->querySql = $subject;

        return true;
    }

    public function performWithId($subject)
    {
        return $subject === $this->queryId;
    }

    public function performWithPidm($subject)
    {
        return $subject === $this->queryPidm;
    }

    public function performWithTermCode($subject)
    {
        return $subject === $this->queryTermCode;
    }

    public function performWithWaiverStatus($subject)
    {
        return $subject === $this->queryWaiverStatus;
    }

    public function performWithEligible($subject)
    {
        return $subject === $this->queryEligible;
    }

    public function performWithEmptyValue($subject)
    {
        return $subject === '';
    }

    public function performMock()
    {
        return true;
    }

    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName = $subject;

        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs = $subject;

        return true;
    }

    public function callResourceMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();
        $response->setStatus(App::API_OK);

        return $response;
    }

    public function callResourceCreateMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();
        $response->setStatus(App::API_CREATED);

        return $response;
    }

    public function callResourceFailureMock()
    {
        $response = new \MiamiOH\RESTng\Util\Response();
        $response->setStatus(App::API_FAILED);
        $response->setPayload(array('data' => array('message', '')));

        return $response;
    }


}
