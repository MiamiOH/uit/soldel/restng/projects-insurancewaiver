#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/a collection of waiver updates in the payload/, sub {
    
    S->{'object'} = [
        {
            'id' => 'DAVISD',
            'termCode' => '201610',
            'waiverStatus' => 'WA',
        },
        {
            'id' => 'HOWARDJ',
            'termCode' => '201610',
            'waiverStatus' => 'E',
        },
        {
            'id' => 'JONESJ',
            'termCode' => '201610',
            'waiverStatus' => 'WD',
        },
    ]
    
};

