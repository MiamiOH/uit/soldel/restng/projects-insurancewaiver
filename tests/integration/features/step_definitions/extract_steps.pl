#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use LWP::UserAgent;
use JSON;
use Data::Dumper;

use lib 'lib';
use StepConfig;

When qr/an extract file is now located at (\S+) called (\S+)/, sub{
    my $fileLocation = $1;
    my $fileName = $2;
    S->{'fullFileName'} = '../'.$1.'/'.$2;
};

When qr/I run the extract called (\S+) located at (\S+)/, sub{
    my $fileName = $1;
    my $fileLocation = $2;
    my $results = system('../'.$fileLocation.'/'.$fileName);
    ok($results eq 0,"Could not execute extract script");
};

Then qr/I delete the extract file located at (\S+) called (\S+)/, sub{
    is(system('rm ../'. $1 .'/' . $2),0);
};

Then qr/I delete the created extract file/, sub{
    is(system('rm ' .S->{'fullFileName'}),0);
};

Then qr/the response (\S+) element count equals the number of records in file (\S+) located at (\S+)/, sub {
    my $body = from_json(${S->{'response'}->content_ref()});

    my $element = $1;
    my $fileName = $2;
    my $fileLocation = $3;
    my $lines=0;
    my $responseCount = $#{$body->{$element}}+1;
    my $results = open (FILE, '../' . $fileLocation .'/'. $fileName);
      while (<FILE>) { $lines++ if  !/^\s+?$/;}
    close FILE;
    ok($results eq 1,"Could not open file to read");
    ok($lines eq $responseCount,"Record Count in File $fileName is $lines and Records returned with a ". S->{'method'} . " request for " . S->{'path'} . " are $responseCount");
};

Then qr/the value of the (\S+) field from the file is equal to value of the response (\S+) element entry (\S+) key/, sub {
    my $field = $1;
    my $element = $2;
    my $key = $3;
    my @fields = [];

    my $index = 0 if ($field eq 'StudentId');
# print Dumper($index);
    my $results = open (FILE, S->{'fullFileName'});
    while(<FILE>)
    {
      # get rid of the pesky newline character
      chomp;
      # read the fields in the current line into an array
      @fields = split('\|', $_);
      my $StudentId = $fields[$index];
      C->run(When => "I make a GET request for /health/insurance/student/v1/profile/${StudentId}");;

      print "$fields[$index]\n";
    }
    close FILE;
    };
