#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the WAS test data is ready/, sub {
    
    $dbh->do(q{
        delete from stuins_test_update
        });
    
    $dbh->do(q{
        delete from student_insurance_status
        });
    
};

Then qr/the WAS update was called with (\S+) "(\S+)" for (\S+)/, sub {
    my $attribute = $1;
    my $value = $2;
    my $uid = $3;

    my $column  = '';
    $column = 'stuins_eligible' if ($attribute eq 'eligible');

    my($exists) = $dbh->selectrow_array(qq{ 
        select count(*)
            from stuins_test_update
            where stuins_uid = lower(?)
                and $column = '$value'
        }, undef, $uid);

    ok($exists, "WAS attribute $attribute was set to $value for $uid");

};

Then qr/the WAS update was not called for (\S+)/, sub {
    my $uid = $1;

    my($exists) = $dbh->selectrow_array(qq{ 
        select count(*)
            from stuins_test_update
            where stuins_uid = lower(?)
        }, undef, $uid);

    ok(!$exists, "WAS update was not called for $uid");

};

Then qr/the WAS update was called with completed today for (\S+)/, sub {
    my $uid = $1;

    my($exists) = $dbh->selectrow_array(qq{ 
        select count(*)
            from stuins_test_update
            where stuins_uid = lower(?)
                and stuins_completed_date = to_char(sysdate, 'YYYYMMDD')
        }, undef, $uid);

    ok($exists, "WAS data was marked complete on current date for $uid");

};

