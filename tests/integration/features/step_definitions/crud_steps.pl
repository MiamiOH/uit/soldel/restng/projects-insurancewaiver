#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use lib 'lib';
use StepConfig;

Given qr/an empty (\S+) table/, sub { 
	$dbh->do(qq{ delete from $1 });
};

Given qr/an empty external (\S+) table/, sub { 
	$dbhExternal->do(qq{ delete from $1 });
};

# a book with the title "My Book"
Given qr/a (\S+) record with the (\S+) "([^"]+)"/, sub {
	S->{'recordType'} = $1;
	S->{'record'}{$2} = $3;
};

# that the book has been added
Given qr/that the (\S+) record has been added/, sub {
	ok($1 eq S->{'recordType'}, "Create a new $1");

	if ($1 eq S->{'recordType'}) {
		my $fields = join(', ', keys %{S->{'record'}});
		my $values = join(', ', map { "'" . $_ . "'"} values %{S->{'record'}});
		my $query = "insert into " . S->{'recordType'} . '(' . $fields . ') values (' . $values . ')';

		$dbh->do($query);
	}
};

Given qr/a (\S+) record with (\S+) (\S+) where (\S+) equals (\S+) (.*)/, sub {
	my $table = $1;
	my $field = $2;
	my $value = $3;
	my $withField = $4;
	my $withValue = $5;
	my $spec = $6;
	my $status = $6 eq 'does exist' ? 1 : 0;

	my($realStatus) = $dbh->selectrow_array(qq{ 
		select count(*)
			from $table
			where $field = ?
			  and $withField = ?
		}, undef, $value, $withValue);

	ok($realStatus == $status, "A $table with $field of $value $spec where $withField equals $withValue");

};

Given qr/that a (\S+) record with (\S+) (\S+) (.*)/, sub {
	my $table = $1;
	my $field = $2;
	my $value = $3;
	my $spec = $4;
	my $status = $4 eq 'does exist' ? 1 : 0;

	my($realStatus) = $dbh->selectrow_array(qq{ 
		select count(*)
			from $table
			where $field = ?
		}, undef, $value);

	ok($realStatus == $status, "A $table with $field of $value $spec");

};

When qr/I have a (\S+) with the (\S+) "([^"]+)"/, sub {
	S->{'objectType'} = $1;
	S->{'object'}{$2} = $3;
};

Then qr/a (\S+) record with (\S+) (\S+) where (\S+) equals (\S+) (.*)/, sub {
	my $table = $1;
	my $field = $2;
	my $value = $3;
	my $withField = $4;
	my $withValue = $5;
	my $spec = $6;
	my $status = $6 eq 'does exist' ? 1 : 0;

	my($realStatus) = $dbh->selectrow_array(qq{ 
		select count(*)
			from $table
			where $field = ?
			  and $withField = ?
		}, undef, $value, $withValue);

	ok($realStatus == $status, "A $table with $field of $value $spec where $withField equals $withValue");

};

Then qr/a (\S+) record with (\S+) (\S+) (.*)/, sub {
	my $table = $1;
	my $field = $2;
	my $value = $3;
	my $spec = $4;
	my $status = $4 eq 'does exist' ? 1 : 0;

	my($realStatus) = $dbh->selectrow_array(qq{ 
		select count(*)
			from $table
			where $field = ?
		}, undef, $value);

	ok($realStatus == $status, "A $table with $field of $value $spec");

};

Then qr/a external (\S+) record with (\S+) (\S+) (.*)/, sub {
	my $table = $1;
	my $field = $2;
	my $value = $3;
	my $spec = $4;
	my $status = $4 eq 'does exist' ? 1 : 0;

	my($realStatus) = $dbhExternal->selectrow_array(qq{ 
		select count(*)
			from $table
			where $field = ?
		}, undef, $value);

	ok($realStatus == $status, "A $table with $field of $value $spec");

};