# Test usage of the Student Insurance population service
Feature: Get the population of Miami students to be provided to the insurance portal vendor
 As a consumer of the Student Insurance population service
 I want to get a list of the correct population
 In order to use the list of people to get profile information

Background:
  Given the test data is ready
  And the configuration data is ready

Scenario: Require authentication to get the population list
  Given a REST client
  When I make a GET request for /health/insurance/student/v1/population
  Then the HTTP status code is 401

Scenario: Get a population list using proper credentials
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/population
  Then the HTTP status code is 200
  And the response data element contains 6 entries

Scenario: Get a population list for a specific term using proper credentials
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/population?termCode=201710
  Then the HTTP status code is 200
  And the response data element contains 6 entries

Scenario: Get a population list of non-actors
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/population?nonActors=true
  Then the HTTP status code is 200
  And the response data element contains 1 entries

Scenario: Get a population list containing only health insurance eligible students
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/population?fullTimeEligible=true
  Then the HTTP status code is 200
  And the response data element contains 4 entries

Scenario: Get a population list of non-actors with email addresses
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/population?nonActors=true&attributes=email
  Then the HTTP status code is 200
  And the response data element contains 1 entries
  And the response data element first entry contains a emailAddress key matching ".*"

Scenario: Get a population list containing only health insurance enrolled students
  Given a REST client
  And the configuration item nonActorEnrollmentStartDate has the value "20160801"
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/population?enrolled=true
  Then the HTTP status code is 200
  And the response data element contains 3 entries

Scenario: Get a population list containing only late charge students
  Given a REST client
  And the late charge start date is today
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/population?lateCharge=true
  Then the HTTP status code is 200
  And the response data element contains 1 entries

