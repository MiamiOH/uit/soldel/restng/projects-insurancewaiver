# Test usage of the Student Insurance term service
Feature: Get current term for operations
 As a consumer of the Student Insurance service
 I want to get the current term
 In order to call other services with a specific term value
 
Scenario: Require authentication to get the term
  Given a REST client
  When I make a GET request for /health/insurance/student/v1/term/default
  Then the HTTP status code is 401
  
Scenario: Get waiver collection using proper credentials
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/term/default
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  
