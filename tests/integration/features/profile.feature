# Test usage of the Student Insurance population service
Feature: Get the profile of Miami students to be provided to the insurance portal vendor
 As a consumer of the Student Insurance profile service
 I want to get
 In order to

Background:
  Given the test data is ready

Scenario: Require authentication to get the profile list
  Given a REST client
  When I make a GET request for /health/insurance/student/v1/profile
  Then the HTTP status code is 401

Scenario: Get profile collection using proper credentials
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a id key equal to "doej"
  And the response data element first entry contains a firstName key equal to "John"
  And the response data element first entry contains a lastName key equal to "Doe"
  And the response data element first entry contains a mi key equal to "G"
  And the response data element first entry contains a bannerId key equal to "+11118888"
  And the response data element first entry contains a emailAddress key equal to "doej@miamioh.edu"
  And the response data element first entry contains a birthDate key equal to "09121999"
  And the response data element first entry contains a sex key equal to "M"
  And the response data element first entry contains a address1 key equal to "123 Main Street"
  And the response data element first entry contains a address2 key that is empty
  And the response data element first entry contains a city key equal to "Hometown"
  And the response data element first entry contains a state key equal to "OH"
  And the response data element first entry contains a zip key equal to "450001234"
  And the response data element first entry contains a country key equal to "USA"
  And the response data element first entry contains a termCode key equal to "201710"
  And the response data element first entry contains a studentType key equal to "D"

Scenario: Get profile collection passing an id list as a filter
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile?id=doej,smithj
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a id key equal to "doej"
  And the response data element first entry contains a firstName key equal to "John"
  And the response data element first entry contains a lastName key equal to "Doe"
  And the response data element first entry contains a mi key equal to "G"
  And the response data element first entry contains a bannerId key equal to "+11118888"
  And the response data element first entry contains a emailAddress key equal to "doej@miamioh.edu"
  And the response data element first entry contains a birthDate key equal to "09121999"
  And the response data element first entry contains a sex key equal to "M"
  And the response data element first entry contains a address1 key equal to "123 Main Street"
  And the response data element first entry contains a address2 key that is empty
  And the response data element first entry contains a city key equal to "Hometown"
  And the response data element first entry contains a state key equal to "OH"
  And the response data element first entry contains a zip key equal to "450001234"
  And the response data element first entry contains a country key equal to "USA"
  And the response data element first entry contains a termCode key equal to "201710"
  And the response data element first entry contains a studentType key equal to "D"
#  And the response data element first entry contains a waiverStatus key equal to ""

Scenario: Get profile collection passing an bannerId list as a filter
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile?bannerId=%2B11118888
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a id key equal to "doej"
  And the response data element first entry contains a firstName key equal to "John"
  And the response data element first entry contains a lastName key equal to "Doe"
  And the response data element first entry contains a mi key equal to "G"
  And the response data element first entry contains a bannerId key equal to "+11118888"
  And the response data element first entry contains a emailAddress key equal to "doej@miamioh.edu"
  And the response data element first entry contains a birthDate key equal to "09121999"
  And the response data element first entry contains a sex key equal to "M"
  And the response data element first entry contains a address1 key equal to "123 Main Street"
  And the response data element first entry contains a address2 key that is empty
  And the response data element first entry contains a city key equal to "Hometown"
  And the response data element first entry contains a state key equal to "OH"
  And the response data element first entry contains a zip key equal to "450001234"
  And the response data element first entry contains a country key equal to "USA"
  And the response data element first entry contains a termCode key equal to "201710"
  And the response data element first entry contains a studentType key equal to "D"
#  And the response data element first entry contains a waiverStatus key equal to ""

Scenario: Get profile model using proper credentials
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile/doej
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element contains a id key equal to "doej"
  And the response data element contains a firstName key equal to "John"
  And the response data element contains a lastName key equal to "Doe"
  And the response data element contains a mi key equal to "G"
  And the response data element contains a bannerId key equal to "+11118888"
  And the response data element contains a emailAddress key equal to "doej@miamioh.edu"
  And the response data element contains a birthDate key equal to "09121999"
  And the response data element contains a sex key equal to "M"
  And the response data element contains a address1 key equal to "123 Main Street"
  And the response data element contains a address2 key that is empty
  And the response data element contains a city key equal to "Hometown"
  And the response data element contains a state key equal to "OH"
  And the response data element contains a zip key equal to "450001234"
  And the response data element contains a country key equal to "USA"
  And the response data element contains a termCode key equal to "201710"
  And the response data element contains a studentType key equal to "D"
#  And the response data element first entry contains a waiverStatus key equal to ""

Scenario: Get profile 404 for a user that does not exist
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile/foo
  Then the HTTP status code is 404
  And the HTTP Content-Type header is "application/json"

Scenario: Get profile model using proper credentials and passing a termCode
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile/doej?termCode=201710
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element contains a id key equal to "doej"
  And the response data element contains a firstName key equal to "John"
  And the response data element contains a lastName key equal to "Doe"
  And the response data element contains a mi key equal to "G"
  And the response data element contains a bannerId key equal to "+11118888"
  And the response data element contains a emailAddress key equal to "doej@miamioh.edu"
  And the response data element contains a birthDate key equal to "09121999"
  And the response data element contains a sex key equal to "M"
  And the response data element contains a address1 key equal to "123 Main Street"
  And the response data element contains a city key equal to "Hometown"
  And the response data element contains a state key equal to "OH"
  And the response data element contains a zip key equal to "450001234"
  And the response data element contains a country key equal to "USA"
  And the response data element contains a termCode key equal to "201710"
  And the response data element contains a studentType key equal to "D"

Scenario: Get profile model with a specific birthdate format
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile/doej?birthDateFormat=YYYYMMDD
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element contains a id key equal to "doej"
  And the response data element contains a birthDate key equal to "19990912"

Scenario: Get profile collection with a specific birthdate format
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile?birthDateFormat=YYYYMMDD
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a id key equal to "doej"
  And the response data element first entry contains a birthDate key equal to "19990912"

Scenario: Get profile model with a specific birthdate format does not allow injection
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile/doej?birthDateFormat=YYYY'MMDD
  Then the HTTP status code is 500

Scenario: Get profile collection with a specific birthdate format does not allow injection
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/profile?birthDateFormat=YYYY'MMDD
  Then the HTTP status code is 500
