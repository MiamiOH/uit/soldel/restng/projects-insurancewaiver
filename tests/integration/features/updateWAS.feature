# Test usage of the Student Insurance waiver service
Feature: Update WAS when various data points change
 As waiver processing service
 I want to notify WAS of data changes
 In order to have current status available for notification
 
Background:
  Given the test data is ready
  And the WAS test data is ready

Scenario: Update waiver for records that do not already exist with no waiver
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And that a student_insurance_status record with stuins_pidm <pidm> does not exist
  When I have a profile with the waiverStatus "<status>"
  And I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  And the WAS update was called with eligible "<eligible>" for <uniqueId>
  And the WAS update was called with completed today for <uniqueId>
  Examples:
      | pidm     | uniqueId | bannerId  | status | termCode | eligible |
      | 11120001 | HOWARDJ  | +11120001 | WD     | 201710   | 1        |
      | 11120002 | JONESJ   | +11120002 | E      | 201710   | 0        |
      
Scenario: Do not update WAS if there are no changes
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_termcode "<termCode>"
  And a student_insurance_status record with the stuins_status "<status>"
  And a student_insurance_status record with the stuins_eligible "<eligible>"
  And a student_insurance_status record with the stuins_activity_date "27-APR-15"
  And that the student_insurance_status record has been added
  And that a student_insurance_status record with stuins_pidm <pidm> does exist
  When I have a profile with the waiverStatus "<status>"
  And I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  And the WAS update was not called for <uniqueId>
  Examples:
      | pidm     | uniqueId | bannerId  | status | termCode | eligible |
      | 11120003 | JONESJ2  | +11120003 | E      | 201610   | 1        |
      | 11120004 | SMITHQ   | +11120004 | WD     | 201610   | 1        |
