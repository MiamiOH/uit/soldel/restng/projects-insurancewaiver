# Test usage of the Student Insurance waiver service
Feature: Get details regarding health insurance waiver status
 As a consumer of the Student Insurance waiver service
 I want to get a list of the population of students in the waiver status table for a given term
 In order to ensure eligibility status is properly updated

Background:
  Given the test data is ready
  And the configuration data is ready

Scenario: Require authentication to get the waiver status of a single user
  Given a REST client
  When I make a GET request for /health/insurance/student/v1/waiver/doej?term=201710
  Then the HTTP status code is 401

Scenario: Get waiver status for a single record using proper credentials
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/waiver/smitht2?termCode=201610
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element contains a id key equal to "smitht2"
  And the response data element contains a termCode key equal to "201610"
  And the response data element contains a waiverStatus key equal to "E"
  And the response data element contains a activityDate key equal to "07-MAY-15"
  And the response data element contains a eligible key equal to "1"

Scenario: Request waiver status for a record that doesn't exist
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/waiver/doej?termCode=201610
  Then the HTTP status code is 404
  And the HTTP Content-Type header is "application/json"

Scenario: Require authentication to get the waiver collection
  Given a REST client
  When I make a GET request for /health/insurance/student/v1/waiver
  Then the HTTP status code is 401

Scenario: Get waiver collection using proper credentials
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/waiver?termCode=201610
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a id key equal to "smithq"
  And the response data element first entry contains a termCode key equal to "201610"
  And the response data element first entry contains a waiverStatus key equal to "WA"
  And the response data element first entry contains a activityDate key equal to "07-MAY-15"
  And the response data element first entry contains a eligible key equal to "1"
  And the response data element second entry contains a id key equal to "smithr"
  And the response data element second entry contains a termCode key equal to "201610"
  And the response data element second entry contains a waiverStatus key equal to "WA"
  And the response data element second entry contains a activityDate key equal to "07-MAY-15"
  And the response data element second entry contains a eligible key equal to "0"
  And the response data element third entry contains a id key equal to "smitht2"
  And the response data element third entry contains a termCode key equal to "201610"
  And the response data element third entry contains a waiverStatus key equal to "E"
  And the response data element third entry contains a activityDate key equal to "07-MAY-15"
  And the response data element third entry contains a eligible key equal to "1"

Scenario: Require authentication to update waiver
  Given a REST client
  When I make a PUT request for /health/insurance/student/v1/waiver/smith2
  Then the HTTP status code is 401

Scenario: Update waiver for records that do not already exist with no waiver
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And that a student_insurance_status record with stuins_pidm <pidm> does not exist
  When I have a profile with the waiverStatus "<status>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | status | termCode |
      | 11120001 | HOWARDJ  | +11120001 | WD     | 201710   |
      | 11120002 | JONESJ   | +11120002 | E      | 201710   |

Scenario: Update waiver for records that do not already exist with waiver
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And that a student_insurance_status record with stuins_pidm <pidm> does not exist
  When I have a profile with the waiverStatus "<status>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does exist
  Examples:
      | pidm     | uniqueId | bannerId  | status | termCode |
      | 11120000 | DAVISD   | +11120000 | WA     | 201710   |
      | 11120001 | HOWARDJ  | +11120001 | WA     | 201710   |

Scenario: Update waiver for records that do not already exist with eligibility
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And that a student_insurance_status record with stuins_pidm <pidm> does not exist
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | eligible | termCode |
      | 11120000 | DAVISD   | +11120000 | 1        | 201710   |
      | 11120001 | HOWARDJ  | +11120001 | 0        | 201710   |

Scenario: Update waiver for records that do not already exist with eligibility and no banner ID
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And that a student_insurance_status record with stuins_pidm <pidm> does not exist
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | eligible | termCode |
      | 11120000 | DAVISD   | +11120000 | 1        | 201710   |
      | 11120001 | HOWARDJ  | +11120001 | 0        | 201710   |

Scenario: Update waiver for records that do already exist with no waiver
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_termcode "<termCode>"
  And a student_insurance_status record with the stuins_status " "
  And a student_insurance_status record with the stuins_activity_date "27-APR-15"
  And that the student_insurance_status record has been added
  And that a student_insurance_status record with stuins_pidm <pidm> does exist
  When I have a profile with the waiverStatus "<status>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | status | termCode |
      | 11120001 | HOWARDJ  | +11120001 | WD     | 201710   |
      | 11120002 | JONESJ   | +11120002 | E      | 201710   |

Scenario: Update waiver for records that do already exist with waiver
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_termcode "<termCode>"
  And a student_insurance_status record with the stuins_status " "
  And a student_insurance_status record with the stuins_activity_date "27-APR-15"
  And that the student_insurance_status record has been added
  And that a student_insurance_status record with stuins_pidm <pidm> does exist
  When I have a profile with the waiverStatus "<status>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does exist
  Examples:
      | pidm     | uniqueId | bannerId  | status | termCode |
      | 11120000 | DAVISD   | +11120000 | WA     | 201710   |
      | 11120001 | HOWARDJ  | +11120001 | WA     | 201710   |

Scenario: Update waiver for records that do already exist with eligibility
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_termcode "<termCode>"
  And a student_insurance_status record with the stuins_status " "
  And a student_insurance_status record with the stuins_activity_date "27-APR-15"
  And that the student_insurance_status record has been added
  And that a student_insurance_status record with stuins_pidm <pidm> does exist
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | eligible | termCode |
      | 11120001 | HOWARDJ  | +11120001 | 1        | 201710   |
      | 11120002 | JONESJ   | +11120002 | 0        | 201710   |

Scenario: Update waiver for records that do already exist with eligibility with no Banner ID
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_termcode "<termCode>"
  And a student_insurance_status record with the stuins_status " "
  And a student_insurance_status record with the stuins_activity_date "27-APR-15"
  And that the student_insurance_status record has been added
  And that a student_insurance_status record with stuins_pidm <pidm> does exist
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | eligible | termCode |
      | 11120001 | HOWARDJ  | +11120001 | 1        | 201710   |
      | 11120002 | JONESJ   | +11120002 | 0        | 201710   |

Scenario: Update waiver for records that do already exist with waiver and eligibility
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_termcode "<termCode>"
  And a student_insurance_status record with the stuins_status " "
  And a student_insurance_status record with the stuins_activity_date "27-APR-15"
  And that the student_insurance_status record has been added
  And that a student_insurance_status record with stuins_pidm <pidm> does exist
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the waiverStatus "<status>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | status | eligible | termCode |
      | 11120001 | HOWARDJ  | +11120001 | WD     | 1        | 201710   |
      | 11120002 | JONESJ   | +11120002 | E      | 0        | 201710   |

Scenario: Update waiver for records that do not already exist and are eligible and ensure notifications are properly sent
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And that a student_insurance_status record with stuins_pidm <pidm> does not exist
  And an empty external notsrv_email table
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  And a external notsrv_email record with to_addr <email> does exist
  Examples:
      | pidm     | uniqueId | bannerId  | eligible | termCode | email               |
      | 11120000 | DAVISD   | +11120000 | 1        | 201710   | davisd@miamioh.edu  |
      | 11120001 | HOWARDJ  | +11120001 | 1        | 201710   | howardj@miamioh.edu |

Scenario: Update waiver for records that do not already exist and aren't eligible and ensure no notifications are sent
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And that a student_insurance_status record with stuins_pidm <pidm> does not exist
  And an empty external notsrv_email table
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  And a external notsrv_email record with to_addr <email> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | eligible | termCode | email               |
      | 11120000 | DAVISD   | +11120000 | 0        | 201710   | davisd@miamioh.edu  |
      | 11120001 | HOWARDJ  | +11120001 | 0        | 201710   | howardj@miamioh.edu |

Scenario: Update waiver for records that are eligible with no status and ensure notifications sent properly
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_termcode "<termCode>"
  And a student_insurance_status record with the stuins_eligible "0"
  And a student_insurance_status record with the stuins_activity_date "27-APR-15"
  And that the student_insurance_status record has been added
  And that a student_insurance_status record with stuins_pidm <pidm> does exist
  And an empty external notsrv_email table
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  And a external notsrv_email record with to_addr <email> does exist
  Examples:
      | pidm     | uniqueId | bannerId  | eligible | termCode | email               |
      | 11120001 | HOWARDJ  | +11120001 | 1        | 201710   | howardj@miamioh.edu |
      | 11120002 | JONESJ   | +11120002 | 1        | 201710   | jonesj@miamioh.edu  |

Scenario: Update waiver for records that should not get eligibility emails and ensure no notifications are sent
  Given a REST client
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_termcode "<termCode>"
  And a student_insurance_status record with the stuins_status "<status>"
  And a student_insurance_status record with the stuins_activity_date "27-APR-15"
  And that the student_insurance_status record has been added
  And that a student_insurance_status record with stuins_pidm <pidm> does exist
  And an empty external notsrv_email table
  When I have a profile with the eligible "<eligible>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the waiverStatus "<status>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  And a external notsrv_email record with to_addr <email> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | status | eligible | termCode | email               |
      | 11120001 | HOWARDJ  | +11120001 | WD     | 1        | 201710   | howardj@miamioh.edu |
      | 11120002 | JONESJ   | +11120002 | E      | 1        | 201710   | jonesj@miamioh.edu  |
      | 11120001 | HOWARDJ  | +11120001 | WD     | 0        | 201710   | howardj@miamioh.edu |
      | 11120002 | JONESJ   | +11120002 | E      | 0        | 201710   | jonesj@miamioh.edu  |

Scenario: Require authentication / authorization to update a collection of waiver records
  Given a REST client
  When I make a PUT request for /health/insurance/student/v1/waiver
  Then the HTTP status code is 401

#Scenario: Update a collection of waiver records
#  Given a REST client
#  And a valid token for an authorized user
#  When I make a PUT request for /health/insurance/student/v1/waiver
#  Then the HTTP status code is 200
#  And the HTTP Content-Type header is "application/json"


Scenario: Update waiver for records that have fee reversal turned on
  Given a REST client
  And the configuration item feewaiverReverseSwitch has the value "1"
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_status "<status>"
  When I have a profile with the waiverStatus "<status>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does exist
  Examples:
      | pidm     | uniqueId | bannerId  | status | termCode |
      | 11120000 | DAVISD   | +11120000 | WA     | 201710   |
      | 11120001 | HOWARDJ  | +11120001 | WA     | 201710   |


Scenario: Update waiver for records that have fee reversal turned off
  Given a REST client
  And the configuration item feewaiverReverseSwitch has the value "0"
  And a valid token for an authorized user
  And that a szbuniq record with szbuniq_unique_id <uniqueId> does exist
  And a student_insurance_status record with the stuins_pidm "<pidm>"
  And a student_insurance_status record with the stuins_status "<status>"
  When I have a profile with the waiverStatus "<status>"
  And I have a profile with the termCode "<termCode>"
  And I have a profile with the bannerId "<bannerId>"
  And I have a profile with the id "<uniqueId>"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver/<uniqueId>
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm <pidm> does exist
  And a sfrefee record with sfrefee_pidm <pidm> does not exist
  Examples:
      | pidm     | uniqueId | bannerId  | status | termCode |
      | 11120000 | DAVISD   | +11120000 | WA     | 201710   |
      | 11120001 | HOWARDJ  | +11120001 | WA     | 201710   |
