# Test usage of the Student Insurance waiver service
Feature: Validate student eligibility for student health insurance
 As a administrator of the Student Insurance waiver process
 I want to determine a student's eligibility for health insurance
 In order to ensure eligibility status is properly updated in relevant systems
 
Background:
  Given the test data is ready
  
Scenario: Require authentication / authorization to validate eligibility of a single record
  Given a REST client
  When I make a PUT request for /health/insurance/student/v1/validateEligibility/smith2
  Then the HTTP status code is 401
  
Scenario: Validate eligibility of a single record - add new record
  Given a REST client
  And a valid token for an authorized user
  And that a student_insurance_status record with stuins_pidm 11120007 does not exist
  When I make a PUT request for /health/insurance/student/v1/validateEligibility/fishb?termCode=201710
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And a student_insurance_status record with stuins_pidm 11120007 where stuins_eligible equals 1 does exist
  
Scenario: Validate eligibility of a single record - no change
  Given a REST client
  And a valid token for an authorized user
  And that a student_insurance_status record with stuins_pidm 11120005 where stuins_eligible equals 0 does exist
  When I make a PUT request for /health/insurance/student/v1/validateEligibility/smithr?termCode=201610
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And a student_insurance_status record with stuins_pidm 11120005 where stuins_eligible equals 0 does exist

Scenario: Validate eligibility of a single record - no change
  Given a REST client
  And a valid token for an authorized user
  And that a student_insurance_status record with stuins_pidm 11120006 where stuins_eligible equals 1 does exist
  When I make a PUT request for /health/insurance/student/v1/validateEligibility/smitht2?termCode=201610
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And a student_insurance_status record with stuins_pidm 11120006 where stuins_eligible equals 1 does exist

Scenario: Validate eligibility of a single record - update record
  Given a REST client
  And a valid token for an authorized user
  And that a student_insurance_status record with stuins_pidm 11119999 where stuins_eligible equals 1 does exist
  When I make a PUT request for /health/insurance/student/v1/validateEligibility/smithj?termCode=201620
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And a student_insurance_status record with stuins_pidm 11119999 where stuins_eligible equals 0 does exist

Scenario: Require authentication / authorization to validate eligibility of a collection of records
  Given a REST client
  When I make a PUT request for /health/insurance/student/v1/validateEligibility
  Then the HTTP status code is 401

Scenario: Validate eligibility of a collection of records
  Given a REST client
  And a valid token for an authorized user
  And that a student_insurance_status record with stuins_pidm 11120001 does not exist
  And that a student_insurance_status record with stuins_pidm 11120005 where stuins_eligible equals 0 does exist
  When I make a PUT request for /health/insurance/student/v1/validateEligibility?id=howardj,smithr&termCode=201610
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And a student_insurance_status record with stuins_pidm 11120001 where stuins_eligible equals 1 does exist
  And a student_insurance_status record with stuins_pidm 11120005 where stuins_eligible equals 0 does exist

Scenario: Validate eligibility of a collection of records
  Given a REST client
  And a valid token for an authorized user
  And that a student_insurance_status record with stuins_pidm 11120002 does not exist
  And that a student_insurance_status record with stuins_pidm 11119999 where stuins_eligible equals 1 does exist
  When I make a PUT request for /health/insurance/student/v1/validateEligibility?id=jonesj,smithj&termCode=201620
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And a student_insurance_status record with stuins_pidm 11120002 does not exist
  And a student_insurance_status record with stuins_pidm 11119999 where stuins_eligible equals 0 does exist