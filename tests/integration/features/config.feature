# Test usage of the Student Insurance configuration service
Feature: Get the Aetna plan configuration
 As a consumer of the Student Insurance configuration service
 I want to get the current Aetna plan configuration
 In order to create a properly populated eligible extract file

Scenario: Require authentication to get the config data
  Given a REST client
  When I make a GET request for /health/insurance/student/v1/config/plan
  Then the HTTP status code is 401

Scenario: Get the config using proper credentials
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /health/insurance/student/v1/config/plan
  Then the HTTP status code is 200
  And the response data element contains a aetnaCoverageOption key equal to "E"
  And the response data element contains a aetnaPlanCodeDomestic key equal to "000101"
  And the response data element contains a aetnaPlanCodeInternational key equal to "000103"
  And the response data element contains a aetnaEffectiveDate key equal to "20150801"
  And the response data element contains a aetnaTermDate key equal to "20160731"
  And the response data element contains a aetnaTermCode key equal to "36"
  And the response data element contains a aetnaGroupNumber key equal to "0000867929"
  And the response data element contains a aetnaLocation key that is empty
