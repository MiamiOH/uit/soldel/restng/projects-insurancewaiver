# Test usage of the Student Insurance waiver service
Feature: Update waiver status for a collection of students
 As a consumer of the Student Insurance waiver service
 I want to update a collection of students in a single request
 In order to process updates more efficiently
 
Background:
  Given the test data is ready
  And the configuration data is ready

Scenario: Require authentication to get the waiver collection
  Given a REST client
  And a valid token for an authorized user
  And a collection of waiver updates in the payload
  When I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /health/insurance/student/v1/waiver
  Then the HTTP status code is 200
  And a student_insurance_status record with stuins_pidm 11120000 does exist
  And a sfrefee record with sfrefee_pidm 11120000 does exist
  And a student_insurance_status record with stuins_pidm 11120001 does exist
  And a student_insurance_status record with stuins_pidm 11120002 does exist
