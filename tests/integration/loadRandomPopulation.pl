#!/usr/bin/perl

use strict;

use DBI;
use Data::Dumper;

my $dbh = DBI->connect("DBI:Oracle:XE", "restng", "Hello123") || die DBI->errstr;

my @words;
open WORDS, '/usr/share/dict/words' or die "Couldn't open /usr/share/dict/words: $!";
while (<WORDS>) {
    chomp;
    push(@words, $_);
}
close WORDS;

my $id = 20000000;
my %uidsUsed;
for (my $i = 0; $i < 20000; $i++) {
    my $firstName = getRandomName(\@words);
    my $lastName = getRandomName(\@words);

    my $candidateUid = substr( $lastName, 0, 6 );
    $candidateUid .= (split('', $firstName))[0];

    my $uid = $candidateUid;
    my $count = 1;
    while (defined($uidsUsed{$candidateUid})) {
        $candidateUid = $uid . $count;
        $count++;
    }

    $uid = $candidateUid;

    my $status = int(rand(100)) % 2 == 0 ? 'true' : 'false';
    $dbh->do(q{
        insert into szbuniq (szbuniq_banner_id, szbuniq_unique_id, szbuniq_pidm)
            values (?, ?, ?)
        }, undef, '+' . $id, uc $uid, $id);

    $dbh->do(q{
        insert into spriden (spriden_pidm, spriden_id, spriden_first_name, spriden_last_name)
            values (?, ?, ?, ?)
        }, undef, $id, '+' . $id, $firstName, $lastName);
    
    $id++;
}

sub getRandomName {
    my $words = shift;

    my $totalWords = scalar(@{$words});

    my $word = '';
    until ($word) {
        my $index = int(rand($totalWords));
        
        $word = $words->[$index] unless ($words->[$index] =~ /[^a-z]/);

    }

    return $word;

}