#!/usr/bin/perl

use strict;

use DBI;
use Data::Dumper;
use LWP::UserAgent;
use JSON;
use Getopt::Long;

my $refreshFromSpreadSheet = 0;

my $result = GetOptions('refresh' => \$refreshFromSpreadSheet);

if ($refreshFromSpreadSheet) {
    # https://blogs.it.ox.ac.uk/acit-rs-team/2014/10/08/how-to-get-data-out-of-a-google-spreadsheet-using-jsonp/
    # follow those instructions to get the sheet id and plug it into the map below.
    my $ssBaseUrl = 'https://spreadsheets.google.com/feeds/cells/1q5Nol36-4wz7m9Y-4AYkoKiivPLixkm65x1hKNnBFKI';

    my %sources = (
            'od6' => 'students',
            'oa99ie' => 'waivers',
            'o5lky87' => 'addresses',
            'o80ha3h' => 'fees',
            'oitk20m' => 'feeCodes',
            'ot5kcgk' => 'terms',
            'orq2rar' => 'personIds',
            'o5civl1' => 'visas',
        );

    foreach my $sheetId (keys %sources) {
        my $url = $ssBaseUrl . '/' . $sheetId . '/public/values?alt=json';

        my ($fieldNames, $data) = getWorksheetData($url);

        open OUT, ">sampleData/$sources{$sheetId}.txt" or die "Couldn't open sampleData/$sources{$sheetId}.txt: $!";
        print OUT join("\t", @{$fieldNames}) . "\n";
        foreach my $record (@{$data}) {
            my @values;
            foreach my $field (@{$fieldNames}) {
                push(@values, $record->{$field});
            }
            print OUT join("\t", @values) . "\n";
        }
        close OUT,
    }

}

my $studentData = loadDataFromFile('students');
my $waiverData = loadDataFromFile('waivers');
my $addressData = loadDataFromFile('addresses');
my $feeData = loadDataFromFile('fees');
my $feeCodeData = loadDataFromFile('feeCodes');
my $termData = loadDataFromFile('terms');
my $personData = loadDataFromFile('personIds');
my $visaData = loadDataFromFile('visas');

my $dbh = DBI->connect("DBI:Oracle:XE", "restng", "Hello123") || die DBI->errstr;

foreach my $table (qw(szbuniq spriden spraddr spbpers sgbstdn gorvisa sfrefee stvterm student_insurance_status )) {
    $dbh->do(qq{
            delete from $table
        });
}

foreach my $student (@{$studentData}) {

    $dbh->do(q{
        insert into szbuniq (szbuniq_banner_id, szbuniq_unique_id, szbuniq_pidm)
            values (?, upper(?), ?)
        }, undef, $student->{'bannerId'}, uc $student->{'uniqueId'}, $student->{'pidm'});

    $dbh->do(q{
        insert into spbpers (spbpers_pidm, spbpers_birth_date, spbpers_sex, spbpers_activity_date, spbpers_armed_serv_med_vet_ind)
            values (?, to_date(?, 'MM/DD/YYYY'), ?, sysdate, 'N')
        }, undef, $student->{'pidm'}, $student->{'birthDate'}, $student->{'sex'});

    $dbh->do(q{
        insert into sgbstdn (sgbstdn_pidm, sgbstdn_camp_code, sgbstdn_term_code_eff, sgbstdn_stst_code,
                sgbstdn_styp_code, sgbstdn_levl_code, sgbstdn_activity_date,sgbstdn_full_part_ind)
            values (?, ?, ?, ?, ?, ?, sysdate,?)
        }, undef, $student->{'pidm'}, $student->{'campusCode'}, $student->{'termCode'}, $student->{'studentCode'},
            $student->{'studentType'},$student->{'level'},$student->{'fullPartTimeIndicator'});

}

foreach my $waiver (@{$waiverData}) {

    $dbh->do(q{
        insert into student_insurance_status (stuins_pidm, stuins_termcode, stuins_eligible, stuins_status, stuins_activity_date)
            values (?, ?, ?, ?, sysdate)
        }, undef, $waiver->{'pidm'}, $waiver->{'termCode'}, $waiver->{'eligible'}, $waiver->{'status'});

}

foreach my $person (@{$personData}) {

    $dbh->do(q{
        insert into spriden (spriden_pidm, spriden_id, spriden_first_name, spriden_last_name, spriden_mi, spriden_change_ind)
            values (?, ?, ?, ?, ?, ?)
        }, undef, $person->{'pidm'}, $person->{'id'}, $person->{'firstName'}, $person->{'lastName'}, $person->{'mi'}, $person->{'changeInd'});

}

foreach my $visa (@{$visaData}) {

    $dbh->do(q{
        insert into gorvisa (gorvisa_pidm, gorvisa_seq_no, gorvisa_vtyp_code, gorvisa_entry_ind,
                gorvisa_user_id, gorvisa_activity_date)
            values (?, ?, ?, ?, ?, sysdate)
        }, undef, $visa->{'pidm'}, $visa->{'seqNo'}, $visa->{'visaType'}, 'Y', 'DOEJ');

}

foreach my $address (@{$addressData}) {

    $dbh->do(q{
        insert into spraddr (spraddr_pidm, spraddr_atyp_code, spraddr_street_line1, spraddr_street_line2, spraddr_city,
                spraddr_stat_code, spraddr_zip, spraddr_natn_code, spraddr_seqno, spraddr_activity_date,
                spraddr_from_date, spraddr_to_date, spraddr_status_ind)
            values (?, ?, ?, ?, ?, ?, ?, ?, nvl((select max(spraddr_seqno) from spraddr where spraddr_pidm = ?), 0) + 1,
                sysdate, to_date(?, 'mm/dd/yyyy'), to_date(?, 'mm/dd/yyyy'), ?)
        }, undef, $address->{'pidm'}, $address->{'aType'}, $address->{'address1'}, $address->{'address2'},
            $address->{'city'}, $address->{'state'}, $address->{'zip'}, $address->{'country'}, $address->{'pidm'},
            $address->{'from'}, $address->{'to'}, $address->{'status'});

}

foreach my $fee (@{$feeData}) {

        $dbh->do(q{
            insert into tbraccd (tbraccd_pidm, tbraccd_term_code, tbraccd_detail_code, tbraccd_amount)
                values (?, ?, ?, ?)
            }, undef, $fee->{'pidm'}, $fee->{'termCode'}, $fee->{'feeDetailCode'}, $fee->{'amount'});

}

foreach my $feeCode (@{$feeCodeData}) {

        $dbh->do(q{
            insert into sfrefee (sfrefee_pidm, sfrefee_term_code, sfrefee_detl_code, sfrefee_activity_date)
                values (?, ?, ?, sysdate)
            }, undef, $feeCode->{'pidm'}, $feeCode->{'termCode'}, $feeCode->{'feeDetailCode'});

}

foreach my $term (@{$termData}) {

        $dbh->do(q{
            insert into stvterm (stvterm_code, stvterm_desc, stvterm_start_date, stvterm_end_date, stvterm_activity_date)
                values (?, ?, to_date(?, 'MM/DD/YYYY'), to_date(?, 'MM/DD/YYYY'), sysdate)
            }, undef, $term->{'termCode'}, $term->{'description'}, $term->{'startDate'}, $term->{'endDate'});

}

sub getWorksheetData {
    my $dataUrl = shift;

    my $ua = LWP::UserAgent->new();
    my $request = HTTP::Request->new('GET', $dataUrl);

    my $response = $ua->simple_request($request);

    my $data = from_json(${$response->content_ref()});

    my $headings = {};
    my @fieldNames;
    my $records = [];

    foreach my $entry (@{$data->{'feed'}{'entry'}}) {
        if ($entry->{'gs$cell'}{'row'} == 1) {
            $headings->{$entry->{'gs$cell'}{'col'}} = $entry->{'gs$cell'}{'$t'};
            push(@fieldNames, $entry->{'gs$cell'}{'$t'});
        } else {
            # Adjust row number to 0 based array index
            my $recordNum = $entry->{'gs$cell'}{'row'} - 2;

            if (!defined($records->[$recordNum])) {
                $records->[$recordNum] = {};
            }

            $records->[$recordNum]{$headings->{$entry->{'gs$cell'}{'col'}}} = $entry->{'gs$cell'}{'$t'};
        }
    }

    return \@fieldNames, $records;

}

sub loadDataFromFile {
    my $fileName = shift;

    my $data = [];

    open FILE, "sampleData/$fileName.txt" or die "Couldn't open sampleData/$fileName.txt: $!";

    my $names = <FILE>;
    chomp $names;

    my @fieldNames = split("\t", $names);

    while (<FILE>) {
        chomp;
        my @values = split("\t");

        my $record = {};
        for (my $i = 0; $i < scalar(@fieldNames); $i++) {
            $record->{$fieldNames[$i]} = $values[$i];
        }
        push(@{$data}, $record);
    }

    close FILE;

    return $data;
}
