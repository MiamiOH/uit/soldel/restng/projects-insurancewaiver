<?php

namespace MiamiOH\ProjectsInsurancewaiver\Services;

class Waiver extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'STUINS_DB';
    private $dbh;

    private $configObj;
    private $config = array();
    private $http;

    private $termCode;


    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function setConfigObj($configObj)
    {
        $this->configObj = $configObj;
        $this->config = $this->configObj->getConfig();
    }

    public function setHttp($http)
    {
        $this->http = $http;
    }

    public function setPopulation($population)
    {
        $this->population = $population;
    }

    public function getWaiver()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('id');
        $options = $request->getOptions();

        $idArray = array();
        $idArray[] = $uniqueId;
        $options['id'] = $idArray;

        $waiver = $this->waiverQuery($options);

        if (count($waiver) == 1) {
            $waiver = $this->camelCaseKeys($waiver[0]);
            $response->setPayload($waiver);
        } else {
            if (count($waiver) > 1) {
                throw new \Exception('Too many records found for unique ID');
            } else {
                if (count($waiver) < 1) {
                    $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                }
            }
        }

        return $response;
    }

    public function getWaiverList()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        $waiverList = $this->waiverQuery($options);

        for ($i = 0; $i < count($waiverList); $i++) {
            $waiverList[$i] = $this->camelCaseKeys($waiverList[$i]);
        }

        $response->setPayload($waiverList);

        return $response;
    }

    public function waiverQuery($options = null)
    {
        $values = array();
        $conditions = array();

        $this->setTermCode($options);

        $values[] = $this->termCode;

        if (isset($options['id']) && is_array($options['id'])) {
            $placeHolders = array();
            foreach ($options['id'] as $uniqueId) {
                $placeHolders[] = 'upper(?)';
                $values[] = $uniqueId;
            }
            $conditions[] = 'szbuniq_unique_id in (' . join(', ',
                    $placeHolders) . ')';
        }

        if (isset($options['bannerId']) && is_array($options['bannerId'])) {
            $placeHolders = array();
            foreach ($options['bannerId'] as $bannerId) {
                $placeHolders[] = '?';
                $values[] = $bannerId;
            }
            $conditions[] = 'szbuniq_banner_id in (' . join(', ',
                    $placeHolders) . ')';
        }

        $queryConditionString = '';
        if ($conditions) {
            $queryConditionString = ' and ' . join(' and ', $conditions);
        }

        $waiverQuery =
            'select lower(szbuniq_unique_id) as id,
                stuins_termcode as term_code,
                stuins_status as waiver_status,
                stuins_activity_date as activity_date,
                stuins_eligible as eligible
            from student_insurance_status
              inner join szbuniq
                on stuins_pidm = szbuniq_pidm
            where stuins_termcode = ?';

        $waiverQuery .= $queryConditionString;

        $waiver = $this->dbh->queryall_array($waiverQuery, $values);

        return $waiver;
    }

    public function updateWaiverModel()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('id');
        $data = $request->getData();

        if (!(isset($data['id']) && $data['id'])) {
            throw new \Exception(__CLASS__ . '::updateWaiverModel requires id in data body');
        }

        if (!(isset($data['termCode']) && $data['termCode'])) {
            throw new \Exception(__CLASS__ . '::updateWaiverModel requires termCode in data body');
        }

        if (!(isset($data['waiverStatus']) && $data['waiverStatus']) && !(isset($data['eligible']))) {
            throw new \Exception(__CLASS__ . '::updateWaiverModel requires either waiverStatus or eligible in data body');
        }

        if ($uniqueId !== $data['id']) {
            throw new \Exception('ID in path does not match ID in body');
        }

        $this->updateWaiver($data);

        return $response;
    }

    public function updateWaiverCollection()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $data = $request->getData();

        if (!is_array($data)) {
            throw new \Exception('Data for ' . __CLASS__ . '::updateWaiverCollection must be an array');
        }

        $results = array();
        for ($i = 0; $i < count($data); $i++) {
            try {
                $this->updateWaiver($data[$i]);
                $results[$data[$i]['id']] = array(
                    'code' => \MiamiOH\RESTng\App::API_OK,
                    'message' => ''
                );
            } catch (\Exception $e) {
                $results[$data[$i]['id']] = array(
                    'code' => \MiamiOH\RESTng\App::API_FAILED,
                    'message' => $e->getMessage()
                );
            }
        }

        $response->setPayload($results);

        return $response;
    }

    public function validateEligibilityModel()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('id');
        $options = $request->getOptions();

        if (!(isset($options['termCode']) && $options['termCode'])) {
            throw new \Exception(__CLASS__ . '::validateEligibilityModel requires termCode');
        }

        $termCode = $options['termCode'];

        $this->validateEligibility($uniqueId, $termCode);

        return $response;
    }

    public function validateEligibilityCollection()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        if (!(isset($options['termCode']) && $options['termCode'])) {
            throw new \Exception(__CLASS__ . '::validateEligibilityCollection requires termCode');
        }

        if (!(isset($options['id']) && $options['id'])) {
            throw new \Exception(__CLASS__ . '::validateEligibilityCollection requires id');
        }

        $idList = $options['id'];
        $termCode = $options['termCode'];

        for ($i = 0; $i < count($idList); $i++) {
            $this->validateEligibility($idList[$i], $termCode);
        }

        return $response;
    }


    private function setTermCode($options)
    {
        if (isset($options['termCode']) && $options['termCode']) {
            $this->termCode = $options['termCode'];
        } else {
            $this->termCode = $this->configObj->getDefaultTermCode();
        }
    }

    private function updateWaiver($data)
    {
        $bannerId = $this->getBannerId($data);

        if ((isset($data['bannerId']) && $data['bannerId'])) {
            if ($bannerId !== $data['bannerId']) {
                throw new \Exception('BannerId does not match record of given uniqueId');
            }
        } else {
            $data['bannerId'] = $bannerId;
        }

        $waiverExists = $this->waiverExists($data);

        $wasData = array();
        $eligible = false;

        if ($waiverExists === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            if (!(isset($data['waiverStatus']) && $data['waiverStatus'])) {
                $data['waiverStatus'] = '';
            } else {
                $wasData['completed'] = date('Ymd');
            }

            if (!(isset($data['eligible']))) {
                $data['eligible'] = '';
            } else {
                $wasData['eligible'] = $data['eligible'];
                if ($data['eligible'] === '1') {
                    $eligible = true;
                }
            }

            $this->insertWaiver($data);

        } else {
            $update = false;

            if ((isset($data['waiverStatus']) && $data['waiverStatus'])) {
                if ($waiverExists['stuins_status'] !== $data['waiverStatus']) {
                    $waiverExists['stuins_status'] = $data['waiverStatus'];
                    $wasData['completed'] = date('Ymd');
                    $update = true;
                }
            }

            if ((isset($data['eligible']))) {
                if ($waiverExists['stuins_eligible'] !== $data['eligible']) {
                    $waiverExists['stuins_eligible'] = $data['eligible'];
                    $wasData['eligible'] = $data['eligible'];
                    $update = true;
                    if ($data['eligible'] === '1') {
                        $eligible = true;
                    }
                }
            }

            if ($update) {
                $this->updateWaiverRecord($waiverExists);
            }
        }

        if (isset($data['waiverStatus']) && $data['waiverStatus'] && $data['waiverStatus'] === 'WA') {
            $this->feeReversal($data);
        }

        //only update WAS and send notification for the fall term record processing, to avoid duplicated actions
        if ($this->isFallTerm($data['termCode'])) {
            if ($wasData) {
                $wasData['id'] = $data['id'];
                $this->updateWas($wasData);
            }

            if ($eligible) {
                $waiverExistsNow = $this->waiverExists($data);
                if ($waiverExistsNow['stuins_status'] === '' || is_null($waiverExistsNow['stuins_status'])) {
                    //student is eligible and does not have a recorded status, so notify them they need to act
                    $this->sendNotification($data);
                }
            }
        }
    }

    public function updateWas($options)
    {

        if (!is_array($options)) {
            throw new \Exception(__CLASS__ . '::updateWas parameter must be an array');
        }

        if (!(isset($this->config['wasClientUpdateUrl']) && $this->config['wasClientUpdateUrl'])) {
            $this->log->info('Skipping WAS call because WAS client update url is not set');

            return null;
        }

        if (!(isset($options['id']) && $options['id'])) {
            throw new \Exception('Missing id for updateWas');
        }

        // The URL will already have the start of a query string, so don't add another '?'
        $url = $this->config['wasClientUpdateUrl'] . '&uid=' . $options['id'];

        if (isset($options['eligible'])) {
            $url .= '&eligible=' . $options['eligible'];
        }

        if (isset($options['completed'])) {
            $url .= '&completed=' . $options['completed'];
            $url .= '&complete=1';
        }

        $this->http->get($url);
    }

    private function getBannerId($data)
    {
        $bannerId = $this->dbh->queryfirstcolumn('
            select szbuniq_banner_id as bannerId
               from szbuniq
               where szbuniq_unique_id = upper(?)
            ', $data['id']);

        if ($bannerId === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            throw new \Exception('Error retrieving BannerId');
        }

        return $bannerId;
    }

    private function waiverExists($data)
    {
        $waiverExists = $this->dbh->queryfirstrow_assoc('
                select *
                  from student_insurance_status
                    inner join szbuniq
                      on student_insurance_status.stuins_pidm = szbuniq_pidm
                      and szbuniq_unique_id = upper(?)
                  where stuins_termcode = ?
              ', $data['id'], $data['termCode']);

        return $waiverExists;
    }

    private function insertWaiver($data)
    {
        $this->dbh->perform('
            insert into student_insurance_status (stuins_pidm, stuins_termcode, stuins_status, stuins_eligible, stuins_activity_date)
              values ((
                       select szbuniq_pidm
                       from szbuniq
                       where szbuniq_unique_id = upper(?)
                      ),
              ?, ?, ?, sysdate)
             ', $data['id'], $data['termCode'], $data['waiverStatus'],
            $data['eligible']);
    }

    private function updateWaiverRecord($waiver)
    {
        $this->dbh->perform('
              update student_insurance_status
              set stuins_status = ?, stuins_eligible = ?, stuins_activity_date = sysdate
              where stuins_pidm = ?
                and stuins_termcode = ?
             ', $waiver['stuins_status'], $waiver['stuins_eligible'],
            $waiver['stuins_pidm'], $waiver['stuins_termcode']);
    }

    public function feeReversal($data)
    {
        if ((int)$this->config['feewaiverReverseSwitch'] === 1) {

            if ($this->isFallTerm($data['termCode'])) {
                $data['feeDetailCode'] = $this->config['feeDetailCodeFall'];
            } else if ($this->isSpringTerm($data['termCode'])) {
                $data['feeDetailCode'] = $this->config['feeDetailCodeSpring'];
            } else {
                throw new \Exception('Invalid term code.');
            }

            $feeWaiverResponse = $this->callResource('finance.accountsReceivable.student.v1.fee.reverse',
                array(
                    'data' => $data
                ));

            // Check that the call was successful.
            if ($feeWaiverResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
                throw new \Exception('Failed to update fee waiver: ' . print_r($feeWaiverResponse->getPayload(),
                        true));
            }
        }
    }


    private function sendNotification($data)
    {
        if ($this->config['notificationOn'] === 'on') {
            $toEmail = $this->dbh->queryfirstcolumn("
              select gzkemail.f_getemail_address(szbuniq_pidm,'MU')
                 from szbuniq
                 where szbuniq_unique_id = upper(?)
              ", $data['id']);

            $notifyData = array();
            $notifyData['toAddr'] = $toEmail;
            $notifyData['fromAddr'] = $this->config['notifyFrom'];
            $notifyData['subject'] = $this->config['notifySubject'];
            $notifyData['body'] = $this->config['notifyBody'];
            $fullPayload = array();
            $fullPayload['dataType'] = 'model';
            $fullPayload['data'] = $notifyData;
            $notificationResponse = $this->callResource('notification.v1.email.message.create',
                array(
                    'data' => $fullPayload
                ));

            // Check that the call was successful.
            if ($notificationResponse->getStatus() !== \MiamiOH\RESTng\App::API_CREATED) {
                throw new \Exception('Notification request failed: ' . print_r($notificationResponse->getStatus(),
                        true));
            }
        }
    }

    private function validateEligibility($uniqueId, $termCode)
    {
        $data['id'] = $uniqueId;
        $data['termCode'] = $termCode;

        $rtElig = $this->getRealTimeEligibility($data);

        $waiverExists = $this->waiverExists($data);

        if ($waiverExists === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $stElig = '0';
        } else {
            $stElig = $waiverExists['stuins_eligible'];
        }

        if ($rtElig !== $stElig) {
            $data['eligible'] = $rtElig;
            $this->updateWaiver($data);
        }
    }

    private function getRealTimeEligibility($data)
    {
        $rtElig = '0';

        $fetchQueryPieces = $this->population->getEligibilityQuery($data['termCode']);
        $buildQuery = $this->population->buildQuery($fetchQueryPieces['select'],
            $fetchQueryPieces['from'], $fetchQueryPieces['conditions']);

        $eligQuery = $buildQuery . ' and szbuniq_unique_id = upper(?)';
        $eligQuery .= $fetchQueryPieces['groupBy'];

        $values = $fetchQueryPieces['values'];
        $values[] = $data['id'];

        $getEligibility = $this->dbh->queryfirstcolumn($eligQuery, $values);

        if ($getEligibility === $data['id']) {
            $rtElig = '1';
        }

        return $rtElig;
    }

    private function isFallTerm(string $termCode): bool
    {
        return substr(trim($termCode), -2) == '10';

    }

    private function isSpringTerm(string $termCode)
    {
        return substr(trim($termCode), -2) == '20';
    }

}
