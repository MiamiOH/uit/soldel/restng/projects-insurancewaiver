<?php

namespace MiamiOH\ProjectsInsurancewaiver\Services;

class Term extends \MiamiOH\RESTng\Service
{

    private $configObj;
    private $config = array();

    public function setConfigObj($configObj)
    {
        $this->configObj = $configObj;
        $this->config = $this->configObj->getConfig();
    }

    public function getDefaultTerm()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $term = array();

        $termCode = $this->configObj->getDefaultTermCode();

        $term['termCode'] = $termCode;

        $response->setPayload($term);

        return $response;
    }

}
