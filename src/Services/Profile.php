<?php

namespace MiamiOH\ProjectsInsurancewaiver\Services;

class Profile extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'STUINS_DB';
    private $dbh;

    private $configObj;
    private $config = array();

    private $termCode;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function setConfigObj($configObj)
    {
        $this->configObj = $configObj;
        $this->config = $this->configObj->getConfig();
    }

    public function setWaiver($waiver)
    {
        $this->waiver = $waiver;
    }

    public function getProfile()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('id');
        $options = $request->getOptions();
        $this->setTermCode($options);

        if ($uniqueId) {

            $idArray = array();
            $idArray[] = $uniqueId;
            $options['id'] = $idArray;

            $profile = $this->buildProfile($options);

            if (count($profile) == 1) {
                $profile = $this->camelCaseKeys($profile[0]);
                $response->setPayload($profile);
            } else {
                if (count($profile) > 1) {
                    throw new \Exception('Too many records found for unique ID');
                } else {
                    if (count($profile) < 1) {
                        $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                    }
                }
            }

        } else {

            $profile = $this->buildProfile($options);

            for ($i = 0; $i < count($profile); $i++) {
                $profile[$i] = $this->camelCaseKeys($profile[$i]);
            }

            $response->setPayload($profile);

        }

        return $response;
    }

    private function buildProfile($options = null, $atyp = 'LO')
    {
        $values = array();
        $conditions = array();

        if (isset($options['id']) && is_array($options['id'])) {
            $placeHolders = array();
            foreach ($options['id'] as $uniqueId) {
                $placeHolders[] = 'upper(?)';
                $values[] = $uniqueId;
            }
            $conditions[] = 'szbuniq_unique_id in (' . join(', ',
                    $placeHolders) . ')';
        }

        if (isset($options['bannerId']) && is_array($options['bannerId'])) {
            $placeHolders = array();
            foreach ($options['bannerId'] as $bannerId) {
                $placeHolders[] = '?';
                $values[] = $bannerId;
            }
            $conditions[] = 'szbuniq_banner_id in (' . join(', ',
                    $placeHolders) . ')';
        }

        $profile = $this->profileQuery($conditions, $values, $options);

        for ($i = 0; $i < count($profile); $i++) {
            $options['termCode'] = $this->termCode;
            $waiverStatus = $this->waiver->waiverQuery($options);
            $profile[$i]['waiver_status'] = $waiverStatus === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET ? '' : $waiverStatus[0]['waiver_status'] ?? null;
            $profile[$i]['termCode'] = $this->termCode;

            // Sanitize ZIP to remove hyphen from US ZIP+4 format
            if (preg_match('/(\d{5})-(\d{4})/', $profile[$i]['zip'], $matches)) {
                $profile[$i]['zip'] = $matches[1] . $matches[2];
            }
        }

        return $profile;
    }

    private function profileQuery($conditions, $values, $options = array())
    {
        if (!is_array($options)) {
            throw new \Exception('Options argument for profileQuery must be an array');
        }

        if (!isset($options['birthDateFormat'])) {
            $options['birthDateFormat'] = 'MMDDYYYY';
        }

        // Verify that only allowed characters are in the format in order to prevent injection
        if (preg_match('/[^YMD]/', $options['birthDateFormat'])) {
            throw new \Exception('Invalid character in birthDateFormat');
        }

        $queryConditionString = '';
        if ($conditions) {
            $queryConditionString = ' and ' . join(' and ', $conditions);
        }

        $profileQuery =
            "select lower(szbuniq_unique_id) as id, spriden_first_name as first_name, spriden_last_name as last_name,
            		substr(spriden_mi, 1, 1) as mi, szbuniq_banner_id as banner_id, gzkemail.f_getemail_address(szbuniq_pidm,'MU') as email_address,
            		to_char(spbpers_birth_date, '" . $options['birthDateFormat'] . "') as birth_date,
                spbpers_sex as sex, 
                spraddr_street_line1 as address_1, spraddr_street_line2 as address_2, 
                spraddr_city as city, spraddr_stat_code as state,
            		spraddr_zip as zip, spraddr_natn_code as country,
                fz_getIntlOrDomStuTypForWaiver(szbuniq_pidm) as student_type,
                sgbstdn_camp_code as campus_code,
                sgbstdn_levl_code as study_level,
                spraddr_atyp_code as address_type,
                shrdgmr_degc_code as degree
                from szbuniq
                left join sgbstdn on sgbstdn_pidm = szbuniq_pidm and sgbstdn_term_code_eff = ?
                	inner join spriden
                		on szbuniq_pidm = spriden_pidm
                		and spriden_change_ind is null
                  	inner join spbpers
                  		on szbuniq_pidm = spbpers_pidm
                  	left outer join spraddr
                  		on szbuniq_pidm = spraddr_pidm
                  		and spraddr_status_ind is null
                  		and (spraddr_from_date <= sysdate and (spraddr_to_date is null or spraddr_to_date >= sysdate))
                  		and spraddr_atyp_code in ('MA')
                    left outer join shrdgmr on shrdgmr_pidm = szbuniq_pidm
                        and shrdgmr_term_code_grad is null
                where 1 = 1";
        array_unshift($values, $this->termCode);
        $profileQuery .= $queryConditionString;

        $profile = $this->dbh->queryall_array($profileQuery, $values);

        $payload = [];

        foreach ($profile as $row) {
            if (!isset($payload[$row['id']])) {
                $payload[$row['id']] = $row;
            } else {
                if ($row['address_type'] === 'RH'
                    || ($row['address_type'] === 'LO' && $payload[$row['id']]['address_type'] === 'MA')
                ) {
                    $payload[$row['id']]['address_1'] = $row['address_1'];
                    $payload[$row['id']]['address_2'] = $row['address_2'];
                    $payload[$row['id']]['city'] = $row['city'];
                    $payload[$row['id']]['state'] = $row['state'];
                    $payload[$row['id']]['zip'] = $row['zip'];
                    $payload[$row['id']]['address_type'] = $row['address_type'];
                }
            }
        }

        return array_values($payload);
    }

    private function setTermCode($options)
    {
        if (isset($options['termCode']) && $options['termCode']) {
            $this->termCode = $options['termCode'];
        } else {
            $this->termCode = $this->configObj->getDefaultTermCode();
        }
    }

}
