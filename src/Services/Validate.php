<?php

// @codeCoverageIgnoreStart

namespace MiamiOH\ProjectsInsurancewaiver\Services;

/*
  Enable validation in dev/test only

  Create validate module in AuthMan
  Create datasource for PWAS access
*/

class Validate extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'STUINS_DB';
    private $dbh;

    private $configObj;
    private $config = array();

    private $termCode;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
        $this->wasDbh = $database->getHandle('STUINS_WAS');
    }

    public function setConfigObj($configObj)
    {
        $this->configObj = $configObj;
        $this->config = $this->configObj->getConfig();
    }

    public function getProfileValidation()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('id');
        $options = $request->getOptions();
        $this->setTermCode($options);

        $profileResponse = $this->callResource('health.insurance.student.v1.profile.id',
            array(
                'params' => array(
                    'id' => $uniqueId,
                ),
                'options' => array(
                    'termCode' => isset($options['termCode']) ? $options['termCode'] : '',
                )
            ));

        $validation = array();
        $validation['profile'] = $profileResponse->getPayload();

        $pidm = $this->dbh->queryfirstcolumn('
        select szbuniq_pidm 
          from szbuniq 
          where szbuniq_banner_id = ?
      ', $validation['profile']['bannerId']);

        if ($pidm !== \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $waiverStatus = $this->dbh->queryfirstcolumn('
          select stuins_status as waiver_status
            from student_insurance_status
            where stuins_pidm = ?
        ', $pidm);

            $validation['waiverStatus'] = $waiverStatus === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET ? '' : $waiverStatus;

            $validation['feeReversalPresent'] = $this->dbh->queryfirstcolumn('
          select count(*)
            from sfrefee
            where sfrefee_pidm = ?
              and sfrefee_term_code = ?
              and sfrefee_detl_code = ?
        ', $pidm, $this->termCode, $this->config['feeDetailCode']);

            $wasStatus = $this->wasDbh->queryfirstcolumn('
          select user_data
            from was_extapp_log
            where extapp_id = (select extapp_id from was_extapp where name = ?)
              and client_id = (select client_id from was_client where uniqueid = lower(?))
        ', 'StudentInsuranceWaiver', $validation['profile']['id']);

            $validation['wasStatus'] = $wasStatus === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET ? '' : $wasStatus;
        }

        $response->setPayload($validation);

        return $response;
    }

    private function setTermCode($options)
    {
        if (isset($options['termCode']) && $options['termCode']) {
            $this->termCode = $options['termCode'];
        } else {
            $this->termCode = $this->configObj->getDefaultTermCode();
        }
    }

}

// @codeCoverageIgnoreEnd
