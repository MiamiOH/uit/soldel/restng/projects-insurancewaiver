<?php

namespace MiamiOH\ProjectsInsurancewaiver\Services;

use MiamiOH\RESTng\Service;

class Config extends Service
{
    private $dbDataSourceName = 'STUINS_DB';
    private $dbh;

    private $configLoaded = false;
    private $config = array();

    private $planConfig = array();

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function getConfig()
    {
        $this->loadConfig();

        return $this->config;
    }

    public function getDefaultTermCode()
    {
        $this->loadConfig();

        $defaultTermCode = $this->config['defaultTermCode'];

        if ($defaultTermCode == '') {
            //Look up Term if we don't have one
            $defaultTermCode = $this->dbh->queryfirstcolumn('
            select min(stvterm_code) as termCode
               From stvterm
              where stvterm_end_date >= sysdate
                and substr(stvterm_code,5,2) = 10
            ');
        }

        return $defaultTermCode;
    }

    private function throwExceptionsWhenMissingConfigValues($config)
    {
        // Verify that we get the expected config values
        if (!isset($config['campusCodeList'])) {
            throw new \Exception('Missing config value for campusCodeList');
        }


        if (!isset($config['feeDetailCodeFall'])) {
            throw new \Exception('Missing config value for feeDetailCodeFall');
        }

        if (!isset($config['feeDetailCodeSpring'])) {
            throw new \Exception('Missing config value for feeDetailCodeSpring');
        }

        if (!isset($config['notifyFrom'])) {
            throw new \Exception('Missing config value for notifyFrom');
        }

        if (!isset($config['notifySubject'])) {
            throw new \Exception('Missing config value for notifySubject');
        }

        if (!isset($config['notifyBody'])) {
            throw new \Exception('Missing config value for notifyBody');
        }

        if (!isset($config['notificationOn'])) {
            throw new \Exception('Missing config value for notificationOn');
        }

        if (!isset($config['studentStatusCodeList'])) {
            throw new \Exception('Missing config value for studentStatusCodeList');
        }

        if (!isset($config['currentStudentTypeCodeList'])) {
            throw new \Exception('Missing config value for currentStudentTypeCodeList');
        }

        if (!isset($config['newStudentTypeCodeList'])) {
            throw new \Exception('Missing config value for newStudentTypeCodeList');
        }

        if (!isset($config['courseRegistrationStatusCodes'])) {
            throw new \Exception('Missing config value for courseRegistrationStatusCodes');
        }
    }

    private function loadConfig()
    {
        $logger = \Logger::getLogger(__CLASS__);

        if ($this->configLoaded) {
            $logger->debug('Configuration already loaded');

            return true;
        }

        $logger->debug('Getting new configuration from config service');

        $response = $this->callResource('config.v1', array(
            'params' => array(
                'application' => 'StudentInsWaiver'
            ),
            'options' => array(
                'category' => 'Waiver Processing',
                'format' => 'list'
            )
        ));

        if ($response->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
            throw new \Exception('Failed to load config: ' . print_r($response->getPayload(),
                    true));
        }

        $config = $response->getPayload();

        $this->throwExceptionsWhenMissingConfigValues($config);

        $this->config = array(
            'campusCodeList' => explode(',', $config['campusCodeList']),
            'feeDetailCodeFall' => $config['feeDetailCodeFall'],
            'feeDetailCodeSpring' => $config['feeDetailCodeSpring'],
            'notifyFrom' => $config['notifyFrom'],
            'notifySubject' => $config['notifySubject'],
            'notifyBody' => $config['notifyBody'],
            'notificationOn' => $config['notificationOn'],
            'studentStatusCodeList' => explode(',',
                $config['studentStatusCodeList']),
            'currentStudentTypeCodeList' => explode(',',
                $config['currentStudentTypeCodeList']),
            'newStudentTypeCodeList' => explode(',',
                $config['newStudentTypeCodeList']),
            'courseRegistrationStatusCodes' => explode(',', $config['courseRegistrationStatusCodes']),
            'defaultTermCode' => $config['defaultTermCode'],
            'wasClientUpdateUrl' => isset($config['wasClientUpdateUrl']) ? $config['wasClientUpdateUrl'] : '',
            'nonActorEnrollmentStartDate' => isset($config['nonActorEnrollmentStartDate']) ? $config['nonActorEnrollmentStartDate'] : '',
            'lateChargeStartDate' => isset($config['lateChargeStartDate']) ? $config['lateChargeStartDate'] : '',
            'feewaiverReverseSwitch' => isset($config['feewaiverReverseSwitch']) ? $config['feewaiverReverseSwitch'] : '0',
        );

        $this->planConfig = array();

        foreach (array_keys($config) as $configKey) {
            if (strpos($configKey, 'UH') === 0) {
                $this->planConfig[$configKey] = isset($config[$configKey]) ? $config[$configKey] : '';
            }
        }

        $this->configLoaded = true;
    }

    public function getPlanConfig()
    {
        $this->loadConfig();

        return $this->planConfig;
    }

}
