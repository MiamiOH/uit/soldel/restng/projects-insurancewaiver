<?php

// @codeCoverageIgnoreStart

namespace MiamiOH\ProjectsInsurancewaiver\Services;

class Http
{

    public function get($url)
    {
        return file_get_contents($url);
    }

}

// @codeCoverageIgnoreEnd
