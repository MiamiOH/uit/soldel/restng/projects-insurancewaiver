<?php

namespace MiamiOH\ProjectsInsurancewaiver\Services;

class Population extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'STUINS_DB';
    private $dbh;

    private $configObj;
    private $config = array();
    private $termCode = '';

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function setConfigObj($configObj)
    {
        $this->configObj = $configObj;
        $this->config = $this->configObj->getConfig();
    }


    public function getBasePopulationClause($termCode)
    {
        $values = array();
        $select = 'lower(szbuniq_unique_id) as id ';
        $from = 'sgbstdn, stvterm thisterm, stvterm lastspring, stvterm lastsummer, szbuniq ';
        $conditions = array();

        $conditions[] = 'thisterm.stvterm_code = ?';
        $values[] = $termCode;
        $conditions[] = "lastspring.stvterm_code = (select max(stvterm_code) from stvterm where stvterm_code < thisterm.stvterm_code and stvterm_code like '%20')";
        $conditions[] = "lastsummer.stvterm_code = (select max(stvterm_code) from stvterm where stvterm_code < thisterm.stvterm_code and stvterm_code like '%30')";
        $conditions[] = 'sgbstdn_term_code_eff = (select max(sgbstdn_term_code_eff) from sgbstdn where sgbstdn_pidm = szbuniq_pidm and sgbstdn_term_code_eff <= thisterm.stvterm_code)';
        $conditions[] = 'sgbstdn_pidm = szbuniq_pidm';
        if (is_array($this->config['campusCodeList'])) {
            $placeHolders = array();
            foreach ($this->config['campusCodeList'] as $id) {
                $placeHolders[] = 'upper(?)';
                $values[] = $id;
            }
            $tempCondition = '(sgbstdn_camp_code IN ('.join(', ',
                    $placeHolders).')';
        }
        if (is_array($this->config['campusCodeList'])) {
            $placeHolders = array();
            foreach ($this->config['campusCodeList'] as $id) {
                $placeHolders[] = 'upper(?)';
                $values[] = $id;
            }
            $tempCondition .= ' or exists(select * from sgbstdn where sgbstdn_pidm = szbuniq_pidm and sgbstdn_term_code_eff IN(lastsummer.stvterm_code,lastspring.stvterm_code) and sgbstdn_camp_code IN ('.join(', ',
                    $placeHolders).'))'
                .')';
        }
        $conditions[] = $tempCondition;

        if (is_array($this->config['studentStatusCodeList'])) {
            $placeHolders = array();
            foreach ($this->config['studentStatusCodeList'] as $id) {
                $placeHolders[] = 'upper(?)';
                $values[] = $id;
            }
            $conditions[] = 'sgbstdn_stst_code in ('.join(', ',
                    $placeHolders).')';
        }
        if (is_array($this->config['currentStudentTypeCodeList']) or (is_array($this->config['newStudentTypeCodeList']))) {
            $placeHolders = array();
            if (is_array($this->config['currentStudentTypeCodeList'])) {
                foreach ($this->config['currentStudentTypeCodeList'] as $id) {
                    $placeHolders[] = 'upper(?)';
                    $values[] = $id;
                }
            }
            if (is_array($this->config['newStudentTypeCodeList'])) {
                foreach ($this->config['newStudentTypeCodeList'] as $id) {
                    $placeHolders[] = 'upper(?)';
                    $values[] = $id;
                }
            }
            $conditions[] = 'sgbstdn_styp_code in ('.join(', ',
                    $placeHolders).')';
        }
        $tempCondition = "(fz_enrolled_this_term(sgbstdn_pidm,thisterm.stvterm_code) = 'Y' ";
        if (is_array($this->config['newStudentTypeCodeList'])) {
            $placeHolders = array();
            foreach ($this->config['newStudentTypeCodeList'] as $id) {
                $placeHolders[] = 'upper(?)';
                $values[] = $id;
            }
            $tempCondition .= 'and sgbstdn_styp_code in ('.join(', ',
                    $placeHolders).')';
        }
        $tempCondition .= "or (fz_enrolled_this_term(sgbstdn_pidm,lastsummer.stvterm_code) = 'Y' ";
        $tempCondition .= ' and not exists(select * from shrdgmr where shrdgmr_pidm = sgbstdn_pidm '
            .' and shrdgmr_term_code_grad = lastsummer.stvterm_code '
            .' and shrdgmr_degs_code in(?,?))';

        $values[] = 'AW';
        $values[] = 'DC';
        $tempCondition .= ')'
            ." or (fz_enrolled_this_term(sgbstdn_pidm,lastspring.stvterm_code) = 'Y' "
            .' and not exists(select * from shrdgmr where shrdgmr_pidm = sgbstdn_pidm '
            .' and shrdgmr_term_code_grad = lastspring.stvterm_code '
            .' and shrdgmr_degs_code in(?,?))';
        $values[] = 'AW';
        $values[] = 'DC';

        $tempCondition .= ')'
            .')';
        $conditions[] = $tempCondition;
        $query = $this->buildQuery($select, $from, $conditions);
        $returnArray = array(
            'query' => $query,
            'values' => $values,
            'select' => $select,
            'from' => $from,
            'conditions' => $conditions
        );

        return $returnArray;
    }

    public function buildQuery($select, $from, $conditions)
    {
        $query =
            'select '.$select
            .' from '.$from
            .' where '.join(' and ', $conditions);

        return $query;

    }

    // August 2021 - TD Ticket 18306148
    // changes due to Aetna Production issue
    public function getUpdatedBasePopulationClause($termCode)
    {
        $basePopulation = $this->getBasePopulationClause($termCode);
        $basePopulationQuery = $basePopulation['query'];
        $basePopulationValues = $basePopulation['values'];

        // Students without AS status (SGBSTDN) with at least one or
        // more credit registered hours for the current term
        $registeredInactiveStudents = $this->getInactiveStudentsWithCourseRegistration($termCode);
        $registeredInactiveStudentsQuery = $registeredInactiveStudents['query'];
        $registeredInactiveStudentsValues = $registeredInactiveStudents['values'];

        // Students with health insurance charges in TBRACCD (Detail Code of 3956)
        $studentsWithHealthInsuranceCharges = $this->getEligibilityQuery($termCode);
        $studentsWithHealthInsuranceChargesQuery = $studentsWithHealthInsuranceCharges['query'];
        $studentsWithHealthInsuranceChargesValues = $studentsWithHealthInsuranceCharges['values'];

        $additionalStudents = $registeredInactiveStudentsQuery.' UNION '.$studentsWithHealthInsuranceChargesQuery;

        // updated population list
        $populationListQuery = $basePopulationQuery.' UNION ('.$additionalStudents.')';

        $returnArray = array(
            'query' => $populationListQuery,
            'values' => array_merge($basePopulationValues, $registeredInactiveStudentsValues,
                $studentsWithHealthInsuranceChargesValues),
        );

        return $returnArray;
    }


    // Non-Active Students (SGBSTDN_STYP_CODE) that have 1 or
    // more registered credit hours (SFRSTCR) in the current term
    public function getInactiveStudentsWithCourseRegistration($termCode)
    {
        $values = array();
        $conditions = array();

        $values[] = $termCode;

        $select = 'distinct lower(szbuniq_unique_id) as id ';
        $from = 'szbuniq inner join sfrstcr on szbuniq_pidm = sfrstcr_pidm';
        $from .= ' inner join sgbstdn on szbuniq_pidm = sgbstdn_pidm';

        $conditions[] = 'sgbstdn_term_code_eff = ?';
        $conditions[] = 'sfrstcr_term_code = sgbstdn_term_code_eff';
        $conditions[] = 'sfrstcr_bill_hr >= 1';

        $courseRegistrationStatusCodes = array();
        if (is_array($this->config['courseRegistrationStatusCodes'])) {
            foreach ($this->config['courseRegistrationStatusCodes'] as $registrationStatusCode) {
                $courseRegistrationStatusCodes[] = 'upper(?)';
                $values[] = $registrationStatusCode;
            }
        }

        $conditions[] = 'sfrstcr_rsts_code in ('.join(', ',
                $courseRegistrationStatusCodes).')';

        $studentStatusCodes = array();
        if (is_array($this->config['studentStatusCodeList'])) {
            foreach ($this->config['studentStatusCodeList'] as $studentStatusCode) {
                $studentStatusCodes[] = 'upper(?)';
                $values[] = $studentStatusCode;
            }
        }

        $conditions[] = 'sgbstdn_stst_code not in ('.join(', ', $studentStatusCodes).')';

        $query = $this->buildQuery($select, $from, $conditions);
        $returnArray = array(
            'query' => $query,
            'values' => $values,
            'select' => $select,
            'from' => $from,
            'conditions' => $conditions
        );

        return $returnArray;
    }

    public function getSpringPopulationClause(string $termCode)
    {
        $select = "lower(szbuniq_unique_id) as id ";
        $from = "sgbstdn a, szbuniq ";

        $conditions = [];
        $values = [];
        $conditions[] = "sgbstdn_term_code_eff = (select max(sgbstdn_term_code_eff) from sgbstdn b where sgbstdn_term_code_eff <= ?)";
        $values[] = $termCode;
        $conditions[] = "sgbstdn_pidm = szbuniq_pidm";
        $conditions[] = "sgbstdn_styp_code in ('N','T')";
        $conditions[] = "sgbstdn_stst_code = 'AS'";


        $query = $this->buildQuery($select, $from, $conditions);
        return [
            'query' => $query,
            'values' => $values
        ];
    }

    public function getSpringTransferPopulationClause(string $termCode)
    {
        $select = "lower(szbuniq_unique_id) as id ";
        $from = "sgbstdn a, szbuniq ";

        $conditions = [];
        $values = [];
        $conditions[] = "sgbstdn_term_code_eff = (select max(sgbstdn_term_code_eff) from sgbstdn b where sgbstdn_term_code_eff <= ?)";
        $values[] = $termCode;
        $conditions[] = "sgbstdn_pidm = szbuniq_pidm";
        $conditions[] = "sgbstdn_styp_code in ('J')";
        $conditions[] = "sgbstdn_camp_code in ('O', 'L')";
        $conditions[] = "sgbstdn_stst_code = 'AS'";
        $conditions[] = "sgbstdn_pidm not in (select sgbstdn_pidm from sgbstdn where sgbstdn_term_code_eff = ? AND sgbstdn_camp_code in ('O', 'L'))";
        $values[] = (int) $termCode - 10;

        $query = $this->buildQuery($select, $from, $conditions);
        return [
            'query' => $query,
            'values' => $values
        ];
    }

    public function combinedSpringFile(string $termCode)
    {
        $baseSpring = $this->getSpringPopulationClause($termCode);
        $transferSpring = $this->getSpringTransferPopulationClause($termCode);

        $baseSpringQuery = $baseSpring['query'];
        $transferSpringQuery = $transferSpring['query'];
        $baseSpringValues = $baseSpring['values'];
        $transferSpringValues = $transferSpring['values'];

        $combinedQuery = $baseSpringQuery.' UNION '.$transferSpringQuery;

        $returnArray = array(
            'query' => $combinedQuery,
            'values' => array_merge($baseSpringValues, $transferSpringValues),
        );

        return $returnArray;
    }


    public function getEligibilityQuery($termCode)
    {
        $values = array();
        $conditions = array();

        $select = 'lower(szbuniq_unique_id) as id';
        $from = 'tbraccd inner join szbuniq on tbraccd_pidm = szbuniq_pidm';

        $conditions[] = 'tbraccd_detail_code = ?';
        $conditions[] = 'tbraccd_term_code = ?';

        if (substr($termCode, -2) == '10') {
            $values[] = $this->config['feeDetailCodeFall'];
        } else {
            $values[] = $this->config['feeDetailCodeSpring'];
        }

        $values[] = $termCode;

        $groupBy = ' group by szbuniq_unique_id having sum (tbraccd_amount) > 0';

        $query = $this->buildQuery($select, $from, $conditions);
        $query .= $groupBy;

        $returnArray = array(
            'query' => $query,
            'values' => $values,
            'conditions' => $conditions,
            'select' => $select,
            'from' => $from,
            'groupBy' => $groupBy
        );

        return $returnArray;
    }

    public function getPopulationList()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        //Note that the placeholder for this termCode is built into the $populationQuery
        if (isset($options['termCode']) && $options['termCode']) {
            $this->termCode = $options['termCode'];
        } else {
            $this->termCode = $this->configObj->getDefaultTermCode();
        }

        if (isset($options['type']) && $options['type'] == 'enrolled') {

            $returnedValues = $this->getEligibilityQuery($this->termCode);
            $enrollmentPopulation = $this->dbh->queryall_list($returnedValues['query'],
                $returnedValues['values']);

            $population = [];
            foreach ($enrollmentPopulation as $value) {
                array_push($population, array("id" => $value));
            }

        } else {
            //return the big eligibility population
            if (substr($this->termCode, -2) == '20') {
                $returnedValues = $this->combinedSpringFile($this->termCode);

            } else {
                $returnedValues = $this->getUpdatedBasePopulationClause($this->termCode);
            }

            $population = $this->dbh->queryall_array($returnedValues['query'],
                $returnedValues['values']);

        }

        for ($i = 0; $i < count($population); $i++) {
            $population[$i] = $this->camelCaseKeys($population[$i]);
            $population[$i]['locationFor'] = $this->locationFor('health.insurance.student.v1.profile.id',
                array('id' => $population[$i]['id']));
        }

        $response->setPayload($population);

        return $response;
    }

    public function getPlanConfig()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $response->setPayload($this->configObj->getPlanConfig());

        return $response;
    }

    public function getEnrollment(array $a, array $b, array $dates)
    {
        $population_list = array();
        $today_test = $dates[0] - $dates[1];
        if ($today_test >= 0) {
            $population_list = $a;
        } else {
            $population_list = array_diff($a, $b);
        }

        return $population_list;

    }

}
