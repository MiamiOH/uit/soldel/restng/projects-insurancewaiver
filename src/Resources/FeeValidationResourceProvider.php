<?php

namespace MiamiOH\ProjectsInsurancewaiver\Resources;

use MiamiOH\RESTng\Util\ResourceProvider;

class FeeValidationResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Health\Insurance\StudentProfileValidate',
            'class' => 'MiamiOH\ProjectsInsurancewaiver\Services\Validate',
            'description' => 'Provide profile validation services.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configObj' => array(
                    'type' => 'service',
                    'name' => 'StuInsConfigObj'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'health.insurance.student.v1.validate.id',
            'description' => 'Get a student profile validation entry.',
            'pattern' => '/health/insurance/student/v1/validate/:id',
            'service' => 'Health\Insurance\StudentProfileValidate',
            'method' => 'getProfileValidation',
            'returnType' => 'model',
            'params' => array('id' => array('description' => 'UniqueID for student')),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Validate',
                    'key' => 'view'
                ),
            ),
            'options' => array(
                'termCode' => array('description' => 'Term code for which to select the population'),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}