<?php

namespace MiamiOH\ProjectsInsurancewaiver\Resources;

use MiamiOH\RESTng\Util\ResourceProvider;

class StudentInsuranceResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'HealthInsurance.Waiver',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'string',
                ),
                'termCode' => array(
                    'type' => 'string',
                ),
                'waiverStatus' => array(
                    'type' => 'string',
                ),
                'eligible' => array(
                    'type' => 'string',
                )
            ),
        ));
        $this->addDefinition(array(
            'name' => 'HealthInsurance.Waiver.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/HealthInsurance.Waiver'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StuInsConfigObj',
            'class' => 'MiamiOH\ProjectsInsurancewaiver\Services\Config',
            'description' => 'Provide common configuration services.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'app' => array('type' => 'service', 'name' => 'App'),
            ),
        ));

        $this->addService(array(
            'name' => 'StuInsHttp',
            'class' => 'MiamiOH\ProjectsInsurancewaiver\Services\Http',
            'description' => 'Provide basic functions for the Insurance Waiver service.',
        ));

        $this->addService(array(
            'name' => 'Health\Insurance\Term',
            'class' => 'MiamiOH\ProjectsInsurancewaiver\Services\Term',
            'description' => 'Provide term services.',
            'set' => array(
                'configObj' => array(
                    'type' => 'service',
                    'name' => 'StuInsConfigObj'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'Health\Insurance\StudentPopulation',
            'class' => 'MiamiOH\ProjectsInsurancewaiver\Services\Population',
            'description' => 'Provide population list services.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configObj' => array(
                    'type' => 'service',
                    'name' => 'StuInsConfigObj'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'Health\Insurance\StudentProfile',
            'class' => 'MiamiOH\ProjectsInsurancewaiver\Services\Profile',
            'description' => 'Provide profile services.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configObj' => array(
                    'type' => 'service',
                    'name' => 'StuInsConfigObj'
                ),
                'waiver' => array(
                    'type' => 'service',
                    'name' => 'Health\Insurance\StudentWaiver'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'Health\Insurance\StudentWaiver',
            'class' => 'MiamiOH\ProjectsInsurancewaiver\Services\Waiver',
            'description' => 'Provide waiver services.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configObj' => array(
                    'type' => 'service',
                    'name' => 'StuInsConfigObj'
                ),
                'http' => array('type' => 'service', 'name' => 'StuInsHttp'),
                'population' => array(
                    'type' => 'service',
                    'name' => 'Health\Insurance\StudentPopulation'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'health.insurance.student.v1.population',
            'description' => 'Get a population list.',
            'pattern' => '/health/insurance/student/v1/population',
            'service' => 'Health\Insurance\StudentPopulation',
            'method' => 'getPopulationList',
            'returnType' => 'collection',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Population',
                    'key' => 'view'
                ),
            ),
            'options' => array(
                'termCode' => array('description' => 'Term code for which to select the population'),
                'type' => array(
                    'enum' => [
                        'all',
                        'enrolled'
                    ],
                    'default' => 'all',
                    'description' => 'The type of population to fetch'
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'health.insurance.student.v1.profile',
            'description' => 'Get a collection of student profiles.',
            'pattern' => '/health/insurance/student/v1/profile',
            'service' => 'Health\Insurance\StudentProfile',
            'method' => 'getProfile',
            'returnType' => 'collection',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Profile',
                    'key' => 'view'
                ),
            ),
            'options' => array(
                'id' => array(
                    'type' => 'list',
                    'description' => 'List of UniqueIDs'
                ),
                'bannerId' => array(
                    'type' => 'list',
                    'description' => 'List of Banner IDs'
                ),
                'termCode' => array('description' => 'Term code for which to select the population'),
                'birthDateFormat' => array('description' => 'Date format for returned birthdate value (Oracle TO_CHAR syntax)'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'health.insurance.student.v1.profile.id',
            'description' => 'Get a student profile.',
            'pattern' => '/health/insurance/student/v1/profile/:id',
            'service' => 'Health\Insurance\StudentProfile',
            'method' => 'getProfile',
            'returnType' => 'model',
            'params' => array('id' => array('description' => 'UniqueID for student')),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Profile',
                    'key' => 'view'
                ),
            ),
            'options' => array(
                'termCode' => array('description' => 'Term code for which to select the population'),
                'birthDateFormat' => array('description' => 'Date format for returned birthdate value (Oracle TO_CHAR syntax)'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'health.insurance.student.v1.waiver.id',
            'description' => 'Get student waiver status.',
            'pattern' => '/health/insurance/student/v1/waiver/:id',
            'service' => 'Health\Insurance\StudentWaiver',
            'method' => 'getWaiver',
            'returnType' => 'model',
            'params' => array('id' => array('description' => 'UniqueID for student')),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Waiver',
                    'key' => 'view'
                ),
            ),
            'options' => array(
                'termCode' => array('description' => 'Term code for which to select the population'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'health.insurance.student.v1.waiver',
            'description' => 'Get a collection of student waiver records.',
            'pattern' => '/health/insurance/student/v1/waiver',
            'service' => 'Health\Insurance\StudentWaiver',
            'method' => 'getWaiverList',
            'returnType' => 'collection',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Waiver',
                    'key' => 'view'
                ),
            ),
            'options' => array(
                'termCode' => array('description' => 'Term code for which to select the population'),
            ),
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'health.insurance.student.v1.waiver.id.update',
            'description' => 'Update a student waiver record.',
            'pattern' => '/health/insurance/student/v1/waiver/:id',
            'service' => 'Health\Insurance\StudentWaiver',
            'method' => 'updateWaiverModel',
            'returnType' => 'model',
            'params' => array('id' => array('description' => 'UniqueID for student')),
            'body' => array(
                'description' => 'Waiver data model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/HealthInsurance.Waiver'
                )
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Waiver',
                    'key' => 'update'
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'health.insurance.student.v1.waiver.update',
            'description' => 'Update student waiver records.',
            'pattern' => '/health/insurance/student/v1/waiver',
            'service' => 'Health\Insurance\StudentWaiver',
            'method' => 'updateWaiverCollection',
            'body' => array(
                'description' => 'Collection of waiver data model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/HealthInsurance.Waiver.Collection'
                )
            ),
            'returnType' => 'model',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Waiver',
                    'key' => 'update'
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'health.insurance.student.v1.waiver.id.validate',
            'description' => 'Validate eligibility for a student waiver record.',
            'pattern' => '/health/insurance/student/v1/validateEligibility/:id',
            'service' => 'Health\Insurance\StudentWaiver',
            'method' => 'validateEligibilityModel',
            'returnType' => 'model',
            'params' => array('id' => array('description' => 'UniqueID for student')),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Waiver',
                    'key' => 'update'
                ),
            ),
            'options' => array(
                'termCode' => array('description' => 'Term code for which to select the population'),
            ),
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'health.insurance.student.v1.validate',
            'description' => 'Validate eligibility for student waiver records.',
            'pattern' => '/health/insurance/student/v1/validateEligibility',
            'service' => 'Health\Insurance\StudentWaiver',
            'method' => 'validateEligibilityCollection',
            'returnType' => 'model',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Waiver',
                    'key' => 'update'
                ),
            ),
            'options' => array(
                'id' => array(
                    'type' => 'list',
                    'description' => 'List of UniqueIDs'
                ),
                'termCode' => array('description' => 'Term code for which to select the population'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'health.insurance.student.v1.term.default',
            'description' => 'Get the default term.',
            'pattern' => '/health/insurance/student/v1/term/default',
            'service' => 'Health\Insurance\Term',
            'method' => 'getDefaultTerm',
            'returnType' => 'model',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Profile',
                    'key' => 'view'
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'health.insurance.student.v1.config.plan',
            'description' => 'Get the Aetna plan configuration.',
            'pattern' => '/health/insurance/student/v1/config/plan',
            'service' => 'Health\Insurance\StudentPopulation',
            'method' => 'getPlanConfig',
            'returnType' => 'model',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'Student Insurance Web Services',
                    'module' => 'Population',
                    'key' => 'view'
                ),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}