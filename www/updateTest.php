<?php

$dbh = oci_new_connect('restng', 'Hello123', 'XE');

$uid = strtolower($_REQUEST['uid']);

$result = oci_parse($dbh, "select count(*) from stuins_test_update where stuins_uid = '$uid'");
catchError($dbh);
oci_execute($result, OCI_COMMIT_ON_SUCCESS);
catchError($result);
$resultRow = oci_fetch_array($result, OCI_NUM+OCI_RETURN_NULLS);
catchError($result);

if ($resultRow[0] == 0) {
    $result = oci_parse($dbh, 
        "insert into stuins_test_update (stuins_uid)
            values ('$uid')");
    catchError($dbh);
    oci_execute($result, OCI_COMMIT_ON_SUCCESS);
    catchError($result);
}

if (isset($_REQUEST['eligible'])) {
    $eligible = $_REQUEST['eligible'];

    $result = oci_parse($dbh, 
        "update stuins_test_update set stuins_eligible = '$eligible'
            where stuins_uid = '$uid'");
    catchError($dbh);
    oci_execute($result, OCI_COMMIT_ON_SUCCESS);
    catchError($result);
}

if (isset($_REQUEST['completed'])) {
    $completed = $_REQUEST['completed'];
    $result = oci_parse($dbh, 
        "update stuins_test_update set stuins_completed_date = '$completed'
            where stuins_uid = '$uid'");
    catchError($dbh);
    oci_execute($result, OCI_COMMIT_ON_SUCCESS);
    catchError($result);
}

function catchError($oci) {
    $ociError = oci_error($oci);
    if (is_array($ociError)) {
        print_r($ociError);
        exit;
    }
}