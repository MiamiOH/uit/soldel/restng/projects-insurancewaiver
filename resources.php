<?php

return [
    'resources' => [
        'health' => [
            MiamiOH\ProjectsInsurancewaiver\Resources\FeeValidationResourceProvider::class,
            MiamiOH\ProjectsInsurancewaiver\Resources\StudentInsuranceResourceProvider::class,
        ],

    ]
];