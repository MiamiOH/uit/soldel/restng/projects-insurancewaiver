create or replace FUNCTION fz_getIntlorDomStuTyp(pidm spriden.spriden_pidm%TYPE) RETURN VARCHAR2 AS
  studentType  VARCHAR2(3);
  BEGIN

    if (mod(pidm,4) = 3) then
    studentType := 'I';
      else
      studentType := 'D';
    end if;

  RETURN studentType;

  END fz_getIntlorDomStuTyp;

/
