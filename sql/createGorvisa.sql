BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE GORVISA';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE GORVISA
   (	GORVISA_PIDM NUMBER(8,0) NOT NULL ENABLE,
	GORVISA_SEQ_NO NUMBER(3,0) NOT NULL ENABLE,
	GORVISA_VTYP_CODE VARCHAR2(2 CHAR) NOT NULL ENABLE,
	GORVISA_VISA_NUMBER VARCHAR2(18 CHAR),
	GORVISA_NATN_CODE_ISSUE VARCHAR2(5 CHAR),
	GORVISA_VISS_CODE VARCHAR2(6 CHAR),
	GORVISA_VISA_START_DATE DATE,
	GORVISA_VISA_EXPIRE_DATE DATE,
	GORVISA_ENTRY_IND VARCHAR2(1 CHAR) NOT NULL ENABLE,
	GORVISA_USER_ID VARCHAR2(30 CHAR) NOT NULL ENABLE,
	GORVISA_ACTIVITY_DATE DATE NOT NULL ENABLE,
	GORVISA_VISA_REQ_DATE DATE,
	GORVISA_VISA_ISSUE_DATE DATE,
	GORVISA_PENT_CODE VARCHAR2(3 CHAR),
	GORVISA_NO_ENTRIES VARCHAR2(2 CHAR),
	GORVISA_DATA_ORIGIN VARCHAR2(30 CHAR),
	GORVISA_SURROGATE_ID NUMBER(19,0),
	GORVISA_VERSION NUMBER(19,0),
	GORVISA_VPDI_CODE VARCHAR2(6 CHAR));

   COMMENT ON COLUMN GORVISA.GORVISA_PIDM IS 'PIDM:  Internal Identification number.';
   COMMENT ON COLUMN GORVISA.GORVISA_SEQ_NO IS 'SEQUENCE NUMBER: A Record Sequence Number.';
   COMMENT ON COLUMN GORVISA.GORVISA_VTYP_CODE IS 'VISA TYPE CODE: The Type of Visa Issued.';
   COMMENT ON COLUMN GORVISA.GORVISA_VISA_NUMBER IS 'VISA NUMBER: A unique identification number assigned to a Visa.';
   COMMENT ON COLUMN GORVISA.GORVISA_NATN_CODE_ISSUE IS 'ISSUING NATION CODE: The Country Code of the nation issuing the visa.';
   COMMENT ON COLUMN GORVISA.GORVISA_VISS_CODE IS 'ISSUE AUTHORITY: The Agency authorizing the visa.';
   COMMENT ON COLUMN GORVISA.GORVISA_VISA_START_DATE IS 'VISA START DATE: The date the visa begins.';
   COMMENT ON COLUMN GORVISA.GORVISA_VISA_EXPIRE_DATE IS 'VISA EXPIRE DATE: The date the visa expires.';
   COMMENT ON COLUMN GORVISA.GORVISA_ENTRY_IND IS 'ENTRY INDICATOR: This field indicates whether the visa is for entry into the country.';
   COMMENT ON COLUMN GORVISA.GORVISA_USER_ID IS 'USER ID: The Oracle ID of the user who changed the record.';
   COMMENT ON COLUMN GORVISA.GORVISA_ACTIVITY_DATE IS 'ACTIVITY DATE:  Date of last activity (insert or update) on the record.';
   COMMENT ON COLUMN GORVISA.GORVISA_VISA_REQ_DATE IS 'VISA REQUEST DATE: The date the Visa was requested.';
   COMMENT ON COLUMN GORVISA.GORVISA_VISA_ISSUE_DATE IS 'VISA ISSUE DATE: The date the Visa was Issued.';
   COMMENT ON COLUMN GORVISA.GORVISA_PENT_CODE IS 'PORT OF ENTRY: The Port of Entry into the country.';
   COMMENT ON COLUMN GORVISA.GORVISA_NO_ENTRIES IS 'NUMBER OF ENTRIES: The Number of Entries into the country.  Valid values:  M, S and number 1-99.';
   COMMENT ON COLUMN GORVISA.GORVISA_DATA_ORIGIN IS 'DATA ORIGIN: Source system that created or updated the row.';
   COMMENT ON COLUMN GORVISA.GORVISA_SURROGATE_ID IS 'SURROGATE ID: Immutable unique key';
   COMMENT ON COLUMN GORVISA.GORVISA_VERSION IS 'VERSION: Optimistic lock token.';
   COMMENT ON COLUMN GORVISA.GORVISA_VPDI_CODE IS 'VPDI CODE: Multi-entity processing code.';
   COMMENT ON TABLE GORVISA  IS 'Visa Information Table';


   ALTER TABLE GORVISA ADD CONSTRAINT PK_GORVISA PRIMARY KEY (GORVISA_PIDM, GORVISA_SEQ_NO, GORVISA_VTYP_CODE) ENABLE;
