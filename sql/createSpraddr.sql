BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SPRADDR';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
--------------------------------------------------------
--  DDL for Table SPRADDR
--------------------------------------------------------

  CREATE TABLE SPRADDR 
   (  SPRADDR_PIDM NUMBER(8,0), 
  SPRADDR_ATYP_CODE VARCHAR2(2 CHAR), 
  SPRADDR_SEQNO NUMBER(2,0), 
  SPRADDR_FROM_DATE DATE, 
  SPRADDR_TO_DATE DATE, 
  SPRADDR_STREET_LINE1 VARCHAR2(75 BYTE), 
  SPRADDR_STREET_LINE2 VARCHAR2(75 BYTE), 
  SPRADDR_STREET_LINE3 VARCHAR2(75 BYTE), 
  SPRADDR_CITY VARCHAR2(50 BYTE), 
  SPRADDR_STAT_CODE VARCHAR2(3 CHAR), 
  SPRADDR_ZIP VARCHAR2(30 BYTE), 
  SPRADDR_CNTY_CODE VARCHAR2(5 CHAR), 
  SPRADDR_NATN_CODE VARCHAR2(5 CHAR), 
  SPRADDR_PHONE_AREA VARCHAR2(6 BYTE), 
  SPRADDR_PHONE_NUMBER VARCHAR2(12 BYTE), 
  SPRADDR_PHONE_EXT VARCHAR2(10 BYTE), 
  SPRADDR_STATUS_IND VARCHAR2(1 CHAR), 
  SPRADDR_ACTIVITY_DATE DATE, 
  SPRADDR_USER VARCHAR2(30 CHAR), 
  SPRADDR_ASRC_CODE VARCHAR2(4 CHAR), 
  SPRADDR_DELIVERY_POINT NUMBER(2,0), 
  SPRADDR_CORRECTION_DIGIT NUMBER(1,0), 
  SPRADDR_CARRIER_ROUTE VARCHAR2(4 CHAR), 
  SPRADDR_GST_TAX_ID VARCHAR2(15 CHAR), 
  SPRADDR_REVIEWED_IND VARCHAR2(1 CHAR), 
  SPRADDR_REVIEWED_USER VARCHAR2(30 CHAR), 
  SPRADDR_DATA_ORIGIN VARCHAR2(30 CHAR), 
  SPRADDR_CTRY_CODE_PHONE VARCHAR2(4 BYTE), 
  SPRADDR_HOUSE_NUMBER VARCHAR2(10 BYTE), 
  SPRADDR_STREET_LINE4 VARCHAR2(75 BYTE), 
  SPRADDR_SURROGATE_ID NUMBER(19,0), 
  SPRADDR_VERSION NUMBER(19,0), 
  SPRADDR_USER_ID VARCHAR2(30 CHAR), 
  SPRADDR_VPDI_CODE VARCHAR2(6 CHAR)
   )  ENABLE ROW MOVEMENT ;

   COMMENT ON COLUMN SPRADDR.SPRADDR_PIDM IS 'Internal identification number of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_ATYP_CODE IS 'This field identifies the type of address associated with person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_SEQNO IS 'This field assigns an internal sequence number to each address type associated with person. This field does not display on screen.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_FROM_DATE IS 'This field maintains the effective start date of address associated with person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_TO_DATE IS 'This field maintains the effective end date of address associated with person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_STREET_LINE1 IS 'This field maintains the first line of the address associated with person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_STREET_LINE2 IS 'This field maintains the second line of the address associated with person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_STREET_LINE3 IS 'This field maintains the third line of the address associated with person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_CITY IS 'This field maintains the city associated with the address of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_STAT_CODE IS 'This field maintains the state associated with the address of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_ZIP IS 'This field maintains the zip code associated with the address of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_CNTY_CODE IS 'This field maintains the county associated with the address of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_NATN_CODE IS 'This field maintains the nation/country associated with the address of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_PHONE_AREA IS 'This field maintains the area code of the phone number associated with address of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_PHONE_NUMBER IS 'This field maintains the phone number associated with address of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_PHONE_EXT IS 'This field maintains the extension of the phone number associated with address of person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_STATUS_IND IS 'This field identifies if address information is inactive. Valid value is I - Inactive.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_ACTIVITY_DATE IS 'This field defines the most current date a record is added or changed.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_USER IS 'The Id for the User who create or update the record.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_ASRC_CODE IS 'Address source code.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_DELIVERY_POINT IS 'DELIVERY POINT: This field is used to designate the delivery point for mail as established by the Postal Service.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_CORRECTION_DIGIT IS 'CORRECTION DIGIT: The Correction Digit field is defined by the Postal Service and is used to determine the digits used for delivery related to the ZIP code.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_CARRIER_ROUTE IS 'CARRIER ROUTE: The addresses to which a carrier delivers mail. In common usage, carrier route includes city routes, rural routes, highway contract routes, post office box sections, and general delivery u nits.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_GST_TAX_ID IS 'GST TAX IDENTIFICATION NUMBER: Goods and Services Tax Identification of vendor at this address';
   COMMENT ON COLUMN SPRADDR.SPRADDR_REVIEWED_IND IS 'Reviewed Indicator, Y indicates the address has been reviewed';
   COMMENT ON COLUMN SPRADDR.SPRADDR_REVIEWED_USER IS 'Reviewed User, indicates the user who reviewed the address.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_DATA_ORIGIN IS 'DATA SOURCE: Source system that generated the data';
   COMMENT ON COLUMN SPRADDR.SPRADDR_CTRY_CODE_PHONE IS 'COUNTRY CODE: Telephone code that designates the region and country.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_HOUSE_NUMBER IS 'HOUSE NUMBER: Building or lot number on a street or in an area.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_STREET_LINE4 IS 'STREET LINE 4: This field maintains the fourth line of the address associated with person.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_SURROGATE_ID IS 'SURROGATE ID: Immutable unique key';
   COMMENT ON COLUMN SPRADDR.SPRADDR_VERSION IS 'VERSION: Optimistic lock token.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_USER_ID IS 'USER ID: The user ID of the person who inserted or last updated this record.';
   COMMENT ON COLUMN SPRADDR.SPRADDR_VPDI_CODE IS 'VPDI CODE: Multi-entity processing code.';
   COMMENT ON TABLE SPRADDR  IS 'Address Repeating Table';
--------------------------------------------------------
--  DDL for Index SPRADDR_KEY_INDEX
--------------------------------------------------------

  CREATE UNIQUE INDEX SPRADDR_KEY_INDEX ON SPRADDR (SPRADDR_PIDM, SPRADDR_ATYP_CODE, SPRADDR_SEQNO, SPRADDR_STATUS_IND) 
  ;
--------------------------------------------------------
--  DDL for Index SPRADDR_PIDM_ATYP_IND
--------------------------------------------------------

  CREATE INDEX SPRADDR_PIDM_ATYP_IND ON SPRADDR (SPRADDR_PIDM, SPRADDR_ATYP_CODE) 
  ;
--------------------------------------------------------
--  DDL for Index SPRADDR_KEY_INDEX3
--------------------------------------------------------

  CREATE INDEX SPRADDR_KEY_INDEX3 ON SPRADDR (SPRADDR_USER, SPRADDR_ASRC_CODE, SPRADDR_ACTIVITY_DATE) 
  ;
  ALTER TABLE SPRADDR MODIFY (SPRADDR_PIDM NOT NULL ENABLE);
  ALTER TABLE SPRADDR MODIFY (SPRADDR_ATYP_CODE NOT NULL ENABLE);
  ALTER TABLE SPRADDR MODIFY (SPRADDR_SEQNO NOT NULL ENABLE);
  ALTER TABLE SPRADDR MODIFY (SPRADDR_CITY NOT NULL ENABLE);
  ALTER TABLE SPRADDR MODIFY (SPRADDR_ACTIVITY_DATE NOT NULL ENABLE);
