GRANT SELECT on burmgr.student_insurance_status to mu_MM4P;
grant SELECT on saturn.spriden to burmgr;
grant SELECT on saturn.sgbstdn to burmgr;
grant SELECT on saturn.spbpers to burmgr;
grant SELECT on saturn.stvterm to burmgr;
grant SELECT on saturn.spraddr to burmgr;
grant SELECT on saturn.szbuniq to burmgr;
grant SELECT on saturn.shrdgmr to burmgr;
grant SELECT on taismgr.tbraccd to burmgr;
grant EXECUTE on F_CALC_REGISTRATION_HOURS to burmgr;
grant EXECUTE on GZKEMAIL to burmgr;
grant EXECUTE on fz_getIntlorDomStuTyp to burmgr;
grant EXECUTE on fz_getIntlOrDomStuTypForWaiver to PUBLIC;

CREATE or replace PUBLIC SYNONYM fz_getIntlorDomStuTyp FOR BANINST1.FZ_GETINTLORDOMSTUTYP;
CREATE or replace PUBLIC SYNONYM fz_getIntlOrDomStuTypForWaiver FOR BANINST1.FZ_GETINTLORDOMSTUTYPFORWAIVER;
