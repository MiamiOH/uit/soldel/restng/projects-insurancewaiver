BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE STVTERM';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
--------------------------------------------------------
--  DDL for Table STVTERM
--------------------------------------------------------

  CREATE TABLE STVTERM 
   (    STVTERM_CODE VARCHAR2(6 CHAR), 
    STVTERM_DESC VARCHAR2(30 CHAR), 
    STVTERM_START_DATE DATE, 
    STVTERM_END_DATE DATE, 
    STVTERM_FA_PROC_YR VARCHAR2(4 CHAR), 
    STVTERM_ACTIVITY_DATE DATE, 
    STVTERM_FA_TERM VARCHAR2(1 CHAR), 
    STVTERM_FA_PERIOD NUMBER(2,0), 
    STVTERM_FA_END_PERIOD NUMBER(2,0), 
    STVTERM_ACYR_CODE VARCHAR2(4 CHAR), 
    STVTERM_HOUSING_START_DATE DATE, 
    STVTERM_HOUSING_END_DATE DATE, 
    STVTERM_SYSTEM_REQ_IND VARCHAR2(1 CHAR), 
    STVTERM_TRMT_CODE VARCHAR2(1 CHAR), 
    STVTERM_FA_SUMMER_IND VARCHAR2(1 BYTE), 
    STVTERM_SURROGATE_ID NUMBER(19,0), 
    STVTERM_VERSION NUMBER(19,0), 
    STVTERM_USER_ID VARCHAR2(30 CHAR), 
    STVTERM_DATA_ORIGIN VARCHAR2(30 CHAR), 
    STVTERM_VPDI_CODE VARCHAR2(6 CHAR)
   ) ;

   COMMENT ON COLUMN STVTERM.STVTERM_CODE IS 'This field identifies the term code referenced in the Catalog, Recruiting, Admissions, Gen. Student, Registration, Student Billing and Acad. Hist. Modules. Reqd. value: 999999 - End of Time.';
   COMMENT ON COLUMN STVTERM.STVTERM_DESC IS 'This field specifies the term associated with the term code. The term is identified by the academic year and term number and is formatted YYYYTT.';
   COMMENT ON COLUMN STVTERM.STVTERM_START_DATE IS 'This field identifies the term start date and is formatted DD-MON-YY.';
   COMMENT ON COLUMN STVTERM.STVTERM_END_DATE IS 'This field identifies the term end date and is fomatted DD-MON-YY.';
   COMMENT ON COLUMN STVTERM.STVTERM_FA_PROC_YR IS 'This field identifies the financial aid processing start and end years (e.g. The financial aid processing year 1988 - 1989 is formatted 8889.).';
   COMMENT ON COLUMN STVTERM.STVTERM_ACTIVITY_DATE IS 'This field identifies the most recent date a record was created or updated.';
   COMMENT ON COLUMN STVTERM.STVTERM_FA_TERM IS 'This field identifies the financial aid award term.';
   COMMENT ON COLUMN STVTERM.STVTERM_FA_PERIOD IS 'This field identifies the financial aid award beginning period.';
   COMMENT ON COLUMN STVTERM.STVTERM_FA_END_PERIOD IS 'This field identifies the financial aid award ending period.';
   COMMENT ON COLUMN STVTERM.STVTERM_ACYR_CODE IS 'This field is not currently in use.';
   COMMENT ON COLUMN STVTERM.STVTERM_HOUSING_START_DATE IS 'Housing Start Date.';
   COMMENT ON COLUMN STVTERM.STVTERM_HOUSING_END_DATE IS 'Housing End Date.';
   COMMENT ON COLUMN STVTERM.STVTERM_SYSTEM_REQ_IND IS 'System Required Indicator';
   COMMENT ON COLUMN STVTERM.STVTERM_TRMT_CODE IS 'Term type for this term. Will default from SHBCGPA_TRMT_CODE.';
   COMMENT ON COLUMN STVTERM.STVTERM_FA_SUMMER_IND IS 'SUMMER INDICATOR: Indicates a summer term to financial aid.';
   COMMENT ON COLUMN STVTERM.STVTERM_SURROGATE_ID IS 'SURROGATE ID: Immutable unique key';
   COMMENT ON COLUMN STVTERM.STVTERM_VERSION IS 'VERSION: Optimistic lock token.';
   COMMENT ON COLUMN STVTERM.STVTERM_USER_ID IS 'USER ID: The user ID of the person who inserted or last updated this record.';
   COMMENT ON COLUMN STVTERM.STVTERM_DATA_ORIGIN IS 'DATA ORIGIN: Source system that created or updated the data.';
   COMMENT ON COLUMN STVTERM.STVTERM_VPDI_CODE IS 'VPDI CODE: Multi-entity processing code.';
   COMMENT ON TABLE STVTERM  IS 'Term Code Validation Table';
--------------------------------------------------------
--  DDL for Index STVTERM_AIDY_INDEX
--------------------------------------------------------

  CREATE INDEX STVTERM_AIDY_INDEX ON STVTERM (STVTERM_FA_PROC_YR, STVTERM_CODE) 
  ;
--------------------------------------------------------
--  DDL for Index STVTERM_ACYR_INDEX
--------------------------------------------------------

  CREATE UNIQUE INDEX STVTERM_ACYR_INDEX ON STVTERM (STVTERM_ACYR_CODE, STVTERM_CODE) 
  ;
--------------------------------------------------------
--  DDL for Index PK_STVTERM
--------------------------------------------------------

  CREATE UNIQUE INDEX PK_STVTERM ON STVTERM (STVTERM_CODE) 
  ;
--------------------------------------------------------
--  DDL for Index STVTERM_START_DATE_INDEX
--------------------------------------------------------

  CREATE INDEX STVTERM_START_DATE_INDEX ON STVTERM (STVTERM_START_DATE) 
  ;
--------------------------------------------------------
--  DDL for Index STVTERM_END_DATE_INDEX
--------------------------------------------------------

  CREATE INDEX STVTERM_END_DATE_INDEX ON STVTERM (STVTERM_END_DATE) 
  ;
--------------------------------------------------------
--  Constraints for Table STVTERM
--------------------------------------------------------

  ALTER TABLE STVTERM ADD CONSTRAINT PK_STVTERM PRIMARY KEY (STVTERM_CODE) ENABLE;
  ALTER TABLE STVTERM MODIFY (STVTERM_CODE NOT NULL ENABLE);
  ALTER TABLE STVTERM MODIFY (STVTERM_DESC NOT NULL ENABLE);
  ALTER TABLE STVTERM MODIFY (STVTERM_START_DATE NOT NULL ENABLE);
  ALTER TABLE STVTERM MODIFY (STVTERM_END_DATE NOT NULL ENABLE);
  ALTER TABLE STVTERM MODIFY (STVTERM_ACTIVITY_DATE NOT NULL ENABLE);
