set define off;

delete from cm_config where config_application = 'StudentInsWaiver';

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'feeDetailCode',
        'text', 'scalar', 'Waiver Processing',
        'Fee Detail Code used to reverse charges during a fee waiver', '3956',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'campusCodeList',
        'text', 'scalar', 'Waiver Processing',
        'List of campus codes for selecting student population', 'L,O',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'studentStatusCodeList',
        'text', 'scalar', 'Waiver Processing',
        'List of student status codes for selecting student population', 'AS',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'currentStudentTypeCodeList',
        'text', 'scalar', 'Waiver Processing',
        'List of current student type codes for selecting student population', 'C,D,R,E',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'newStudentTypeCodeList',
        'text', 'scalar', 'Waiver Processing',
        'List of new student type codes for selecting student population', 'N,T,J',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'defaultTermCode',
        'text', 'scalar', 'Waiver Processing',
        'termCodes for selecting student population', '',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'wasClientUpdateUrl',
        'text', 'scalar', 'Waiver Processing',
        'Base URL for updating insurance status in WAS for messaging',
        'http://ws/stuins/updateTest.php?extapp=StudentInsuranceWaiver&ss=bob',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'notifyFrom',
        'text', 'scalar', 'Waiver Processing',
        'email address for automatic notifications', 'studentHealth@MiamiOH.edu',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'notifySubject',
        'text', 'scalar', 'Waiver Processing',
        'subject for automatic notification emails', 'You Are Eligible for Student Health Insurance',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'notifyBody',
        'text', 'scalar', 'Waiver Processing',
        'content for automatic notification emails', 'You are eligible for student health insurance. Please take a moment to either confirm your enrollment or to submit a waiver. If you do not take action by a date, you will be automatically enrolled and charged some money.',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'notificationOn',
        'text', 'scalar', 'Waiver Processing',
        'indicator to turn notifications on and off', 'on',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'nonactorFrom',
        'text', 'scalar', 'Waiver Processing',
        'email address for nonactor notifications', 'studentHealth@MiamiOH.edu',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'nonactorSubject',
        'text', 'scalar', 'Waiver Processing',
        'subject for nonactor notification emails', 'You Must Act Now!',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'nonactorBody',
        'text', 'scalar', 'Waiver Processing',
        'content for automatic notification emails', 'Please take a moment to either confirm your enrollment or to submit a waiver. If you do not take action by a date, you will be automatically enrolled and charged some money.',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaGroupNumber',
        'text', 'scalar', 'Waiver Processing',
        'group number for Aetna eligible extract file', '0000867929',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaEffectiveDate',
        'text', 'scalar', 'Waiver Processing',
        'effective date for Aetna eligible extract file', '20150801',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaTermDate',
        'text', 'scalar', 'Waiver Processing',
        'term date for Aetna eligible extract file', '20160731',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaTermCode',
        'text', 'scalar', 'Waiver Processing',
        'term code for Aetna eligible extract file', '36',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaPlanCodeDomestic',
        'text', 'scalar', 'Waiver Processing',
        'plan code for domestic (D) for Aetna eligible extract file', '000101',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaPlanCodeInternational',
        'text', 'scalar', 'Waiver Processing',
        'plan code for international (I) for Aetna eligible extract file', '000103',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaCoverageOption',
        'text', 'scalar', 'Waiver Processing',
        'coverage option for Aetna eligible extract file', 'E',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaLocation',
        'text', 'scalar', 'Waiver Processing',
        'location for Aetna eligible extract file', '',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'aetnaPlanYear',
        'text', 'scalar', 'Waiver Processing',
        'plan year for Aetna coverage', '1617',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'feewaiverReverseSwitch',
        'text', 'scalar', 'Waiver Processing',
        'switch to turn fee reversal for health waiver', '0',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'lateChargeStartDate',
        'text', 'scalar', 'Waiver Processing',
        'Date after which charge is considered to have been applied late', '28-MAR-16',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('StudentInsWaiver', 'nonActorEnrollmentStartDate',
        'text', 'scalar', 'Waiver Processing',
        'Date which at which non-actors are considered enrolled for coverage', '28-MAR-16',
        sysdate, 'doej');
