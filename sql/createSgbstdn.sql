BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SGBSTDN';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
--------------------------------------------------------
--  DDL for Table SGBSTDN
--------------------------------------------------------

  CREATE TABLE "SGBSTDN" 
   (    "SGBSTDN_PIDM" NUMBER(8,0), 
    "SGBSTDN_TERM_CODE_EFF" VARCHAR2(6 CHAR), 
    "SGBSTDN_STST_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_LEVL_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_STYP_CODE" VARCHAR2(1 CHAR), 
    "SGBSTDN_TERM_CODE_MATRIC" VARCHAR2(6 CHAR), 
    "SGBSTDN_TERM_CODE_ADMIT" VARCHAR2(6 CHAR), 
    "SGBSTDN_EXP_GRAD_DATE" DATE, 
    "SGBSTDN_CAMP_CODE" VARCHAR2(3 CHAR), 
    "SGBSTDN_FULL_PART_IND" VARCHAR2(1 CHAR), 
    "SGBSTDN_SESS_CODE" VARCHAR2(1 CHAR), 
    "SGBSTDN_RESD_CODE" VARCHAR2(1 CHAR), 
    "SGBSTDN_COLL_CODE_1" VARCHAR2(2 CHAR), 
    "SGBSTDN_DEGC_CODE_1" VARCHAR2(6 CHAR), 
    "SGBSTDN_MAJR_CODE_1" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_MINR_1" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_MINR_1_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_1" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_1_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_1_3" VARCHAR2(4 CHAR), 
    "SGBSTDN_COLL_CODE_2" VARCHAR2(2 CHAR), 
    "SGBSTDN_DEGC_CODE_2" VARCHAR2(6 CHAR), 
    "SGBSTDN_MAJR_CODE_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_MINR_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_MINR_2_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_2_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_2_3" VARCHAR2(4 CHAR), 
    "SGBSTDN_ORSN_CODE" VARCHAR2(1 CHAR), 
    "SGBSTDN_PRAC_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_ADVR_PIDM" NUMBER(8,0), 
    "SGBSTDN_GRAD_CREDIT_APPR_IND" VARCHAR2(1 CHAR), 
    "SGBSTDN_CAPL_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_LEAV_CODE" VARCHAR2(1 CHAR), 
    "SGBSTDN_LEAV_FROM_DATE" DATE, 
    "SGBSTDN_LEAV_TO_DATE" DATE, 
    "SGBSTDN_ASTD_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_TERM_CODE_ASTD" VARCHAR2(6 CHAR), 
    "SGBSTDN_RATE_CODE" VARCHAR2(5 CHAR), 
    "SGBSTDN_ACTIVITY_DATE" DATE, 
    "SGBSTDN_MAJR_CODE_1_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_2_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_EDLV_CODE" VARCHAR2(3 CHAR), 
    "SGBSTDN_INCM_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_ADMT_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_EMEX_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_APRN_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_TRCN_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_GAIN_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_VOED_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_BLCK_CODE" VARCHAR2(10 CHAR), 
    "SGBSTDN_TERM_CODE_GRAD" VARCHAR2(6 CHAR), 
    "SGBSTDN_ACYR_CODE" VARCHAR2(4 CHAR), 
    "SGBSTDN_DEPT_CODE" VARCHAR2(4 CHAR), 
    "SGBSTDN_SITE_CODE" VARCHAR2(3 CHAR), 
    "SGBSTDN_DEPT_CODE_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_EGOL_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_DEGC_CODE_DUAL" VARCHAR2(6 CHAR), 
    "SGBSTDN_LEVL_CODE_DUAL" VARCHAR2(2 CHAR), 
    "SGBSTDN_DEPT_CODE_DUAL" VARCHAR2(4 CHAR), 
    "SGBSTDN_COLL_CODE_DUAL" VARCHAR2(2 CHAR), 
    "SGBSTDN_MAJR_CODE_DUAL" VARCHAR2(4 CHAR), 
    "SGBSTDN_BSKL_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_PRIM_ROLL_IND" VARCHAR2(1 CHAR), 
    "SGBSTDN_PROGRAM_1" VARCHAR2(12 CHAR), 
    "SGBSTDN_TERM_CODE_CTLG_1" VARCHAR2(6 CHAR), 
    "SGBSTDN_DEPT_CODE_1_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_121" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_122" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_123" VARCHAR2(4 CHAR), 
    "SGBSTDN_SECD_ROLL_IND" VARCHAR2(1 CHAR), 
    "SGBSTDN_TERM_CODE_ADMIT_2" VARCHAR2(6 CHAR), 
    "SGBSTDN_ADMT_CODE_2" VARCHAR2(2 CHAR), 
    "SGBSTDN_PROGRAM_2" VARCHAR2(12 CHAR), 
    "SGBSTDN_TERM_CODE_CTLG_2" VARCHAR2(6 CHAR), 
    "SGBSTDN_LEVL_CODE_2" VARCHAR2(2 CHAR), 
    "SGBSTDN_CAMP_CODE_2" VARCHAR2(3 CHAR), 
    "SGBSTDN_DEPT_CODE_2_2" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_221" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_222" VARCHAR2(4 CHAR), 
    "SGBSTDN_MAJR_CODE_CONC_223" VARCHAR2(4 CHAR), 
    "SGBSTDN_CURR_RULE_1" NUMBER(8,0), 
    "SGBSTDN_CMJR_RULE_1_1" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_11_1" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_11_2" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_11_3" NUMBER(8,0), 
    "SGBSTDN_CMJR_RULE_1_2" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_12_1" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_12_2" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_12_3" NUMBER(8,0), 
    "SGBSTDN_CMNR_RULE_1_1" NUMBER(8,0), 
    "SGBSTDN_CMNR_RULE_1_2" NUMBER(8,0), 
    "SGBSTDN_CURR_RULE_2" NUMBER(8,0), 
    "SGBSTDN_CMJR_RULE_2_1" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_21_1" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_21_2" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_21_3" NUMBER(8,0), 
    "SGBSTDN_CMJR_RULE_2_2" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_22_1" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_22_2" NUMBER(8,0), 
    "SGBSTDN_CCON_RULE_22_3" NUMBER(8,0), 
    "SGBSTDN_CMNR_RULE_2_1" NUMBER(8,0), 
    "SGBSTDN_CMNR_RULE_2_2" NUMBER(8,0), 
    "SGBSTDN_PREV_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_TERM_CODE_PREV" VARCHAR2(6 CHAR), 
    "SGBSTDN_CAST_CODE" VARCHAR2(2 CHAR), 
    "SGBSTDN_TERM_CODE_CAST" VARCHAR2(6 CHAR), 
    "SGBSTDN_DATA_ORIGIN" VARCHAR2(30 CHAR), 
    "SGBSTDN_USER_ID" VARCHAR2(30 CHAR), 
    "SGBSTDN_SCPC_CODE" VARCHAR2(6 CHAR), 
    "SGBSTDN_SURROGATE_ID" NUMBER(19,0), 
    "SGBSTDN_VERSION" NUMBER(19,0), 
    "SGBSTDN_VPDI_CODE" VARCHAR2(6 CHAR)
   ) ;

   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_PIDM" IS 'This field identifies the internal identification number of student.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_EFF" IS 'This field identifies the effective term associated with student record.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_STST_CODE" IS 'This field identifies the students status for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_LEVL_CODE" IS 'This field identifies the level of the student for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_STYP_CODE" IS 'This field identifies the student type for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_MATRIC" IS 'This field identifies the effective term of matriculation.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_ADMIT" IS 'This field identifies the term student was first admitted to institution.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_EXP_GRAD_DATE" IS 'This field identifies expected graduation date.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CAMP_CODE" IS 'This field identifies the campus location associated with the student for the   effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_FULL_PART_IND" IS 'This field identifies whether the student is a full or part-time student.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_SESS_CODE" IS 'This field identifies the session student is attending for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_RESD_CODE" IS 'This field identifies the residency status of the student for the effective     term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_COLL_CODE_1" IS 'This field identifies the college associated with the primary curriculum for    the effective term..';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_DEGC_CODE_1" IS 'This field identifies the degree within the primary curriculum for the          effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_1" IS 'This field identifies the primary major within the primary curriculum for the   effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_MINR_1" IS 'This field identifies the primary minor within the primary curriculum for the   effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_MINR_1_2" IS 'This field identifies the secondary minor of the student for the effective      term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_1" IS 'This field identifies the primary concentration within the primary curriculum   for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_1_2" IS 'This field identifies the secondary concentration within the primary            curriculum for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_1_3" IS 'This field identifies the third concentration within the primary curriculum     for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_COLL_CODE_2" IS 'This field identifies the college within the secondary curriculum for the       effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_DEGC_CODE_2" IS 'This field identifies the degree within the secondary curriculum for the        effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_2" IS 'This field identifies the primary major within the secondary curriculum for     the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_MINR_2" IS 'This field identifies the primary minor within the secondary curriculum for     the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_MINR_2_2" IS 'This field identifies the secondary minor within the secondary curriculum for   the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_2" IS 'This field identifies the primary concentration within the secondary            curriculum for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_2_2" IS 'This field identifies the secondary concentration within the secondary          curriculum for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_2_3" IS 'This field identifies the third concentration within the secondary curriculum   for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_ORSN_CODE" IS 'This field identifies the orientation session assigned to the student for the   effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_PRAC_CODE" IS 'This field identifies the practical training experience of the student for the  effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_ADVR_PIDM" IS 'This field identifies the internal identification number for the advisor        assigned to the student for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_GRAD_CREDIT_APPR_IND" IS 'This field identifies eligibility of student to take graduate courses for       credit for the effective term. Valid values are Y or blank only.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CAPL_CODE" IS 'This field identifies career plan of the student for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_LEAV_CODE" IS 'This field identifies reason of leave of absence of student for the effective   term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_LEAV_FROM_DATE" IS 'This field identifies the begin date of leave of absence of student for the     effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_LEAV_TO_DATE" IS 'This field identifies the end date of leave of absence of student for the       effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_ASTD_CODE" IS 'This field identifies the academic standing override for a student for the      effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_ASTD" IS 'This field identifies the term associated with the academic standing override.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_RATE_CODE" IS 'This field identifies a specific assessment rate of the student for the         effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_ACTIVITY_DATE" IS 'This field identifies the most current date record was created or updated.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_1_2" IS 'This field identifies the secondary major within the primary curriculum for     the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_2_2" IS 'This field identifies the secondary major within the secondary curriculum for   the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_EDLV_CODE" IS 'A two position alphanumeric field which indicate the highest level of the education that the student completed';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_INCM_CODE" IS 'A two position alphanumeric field which indicate the income range of the student';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_ADMT_CODE" IS 'Admissions type from the admissions application';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_EMEX_CODE" IS 'General Student Employment Expectation Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_APRN_CODE" IS 'General Student Apprenticeship Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TRCN_CODE" IS 'General Student Transfer Center Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_GAIN_CODE" IS 'This field identifies the employment and training code of the student for the effective term.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_VOED_CODE" IS 'General Student Vocation Eduaction Status Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_BLCK_CODE" IS 'Block Schedule Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_GRAD" IS 'Term student intends to graduate.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_ACYR_CODE" IS 'Year student intends to graduate.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_DEPT_CODE" IS 'Department Code.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_SITE_CODE" IS 'Site Code.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_DEPT_CODE_2" IS 'Department Code for second curriculum.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_EGOL_CODE" IS 'Educational Goal code.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_BSKL_CODE" IS 'Student Basic Skills Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_PRIM_ROLL_IND" IS 'Indicates whether the Primary Curriculum should be Rolled to Academic History';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_PROGRAM_1" IS 'Curriculum 1 Program Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_CTLG_1" IS 'Curriculum 1 Catalog Term Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_DEPT_CODE_1_2" IS 'Curriculum 1 - Department 2';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_121" IS 'Concentration Code 1 on Second Major of First Curriculum';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_122" IS 'Concentration Code 2 on Second Major on First Curriculum';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_123" IS 'Concentration Code 3 on Second Major on First Curriculum';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_SECD_ROLL_IND" IS 'Indicates whether the Secondary Curriculum should be Rolled to Academic History';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_ADMIT_2" IS 'Admission Term Code associated with the Secondary Curriculum';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_ADMT_CODE_2" IS 'Admissions Type Code associated with the Secondary Curriculum';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_PROGRAM_2" IS 'Curriculum 2 Program Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_CTLG_2" IS 'Curriculum 2 Catalog Term Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_LEVL_CODE_2" IS 'Curriculum 2 Level Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CAMP_CODE_2" IS 'Curriculum 2 Campus Code';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_DEPT_CODE_2_2" IS 'Curriculum 2 - Department 2';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_221" IS 'Concentration 1 on Second Major of Second Curriculum';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_222" IS 'Concentration 2 on Second Major of Second Curriculum';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_MAJR_CODE_CONC_223" IS 'Concentration 3 on Second Major of Second Curriculum';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CURR_RULE_1" IS 'Curriculum 1 Rule reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CMJR_RULE_1_1" IS 'Curriculum 1 Major 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_11_1" IS 'Concentration 1, Major 1, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_11_2" IS 'Concentration 2, Major 1, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_11_3" IS 'Concentration 3, Major 1, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CMJR_RULE_1_2" IS 'Major 2, Curriculum Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_12_1" IS 'Concentration 1, Major 2, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_12_2" IS 'Concentration 2, Major 2, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_12_3" IS 'Concentration 3, Major 2, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CMNR_RULE_1_1" IS 'Minor 1, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CMNR_RULE_1_2" IS 'Minor 2, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CURR_RULE_2" IS 'Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CMJR_RULE_2_1" IS 'Major 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_21_1" IS 'Concentration 1, Major 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_21_2" IS 'Concentration 2, Major 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_21_3" IS 'Concentration 3, Major 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CMJR_RULE_2_2" IS 'Major 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_22_1" IS 'Concentration 1, Major 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_22_2" IS 'Concentration 2, Major 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CCON_RULE_22_3" IS 'Concentration 3, Major 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CMNR_RULE_2_1" IS 'Minor 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CMNR_RULE_2_2" IS 'Minor 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_PREV_CODE" IS 'General Student record Progress Evaluation code.  This code overrides the code in SHRTTRM.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_PREV" IS 'General Student record Progress Evaluation term.  This is the term for which the progress evaluation code override becomes effective.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_CAST_CODE" IS 'General Student record Combined Academic Standing code.  This code overrides the code in SHRTTRM.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_TERM_CODE_CAST" IS 'General Student record Combined Academic Standing term.  This is the term for which the combined academic standing code override becomes effective.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_DATA_ORIGIN" IS 'DATA SOURCE: Source system that created or updated the row';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_USER_ID" IS 'USER ID: The most recent user to create or update a record.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_SCPC_CODE" IS 'STUDENT CENTRIC PERIOD CYCLE CODE: Cycle Code for the student centric period.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_SURROGATE_ID" IS 'SURROGATE ID: Immutable unique key';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_VERSION" IS 'VERSION: Optimistic lock token.';
   COMMENT ON COLUMN "SGBSTDN"."SGBSTDN_VPDI_CODE" IS 'VPDI CODE: Multi-entity processing code.';
   COMMENT ON TABLE "SGBSTDN"  IS 'Student Base Table';
--------------------------------------------------------
--  DDL for Index PK_SGBSTDN
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_SGBSTDN" ON "SGBSTDN" ("SGBSTDN_PIDM", "SGBSTDN_TERM_CODE_EFF") 
  ;
--------------------------------------------------------
--  Constraints for Table SGBSTDN
--------------------------------------------------------

  ALTER TABLE "SGBSTDN" ADD CONSTRAINT "PK_SGBSTDN" PRIMARY KEY ("SGBSTDN_PIDM", "SGBSTDN_TERM_CODE_EFF") ENABLE;
  ALTER TABLE "SGBSTDN" MODIFY ("SGBSTDN_PIDM" NOT NULL ENABLE);
  ALTER TABLE "SGBSTDN" MODIFY ("SGBSTDN_TERM_CODE_EFF" NOT NULL ENABLE);
  ALTER TABLE "SGBSTDN" MODIFY ("SGBSTDN_STST_CODE" NOT NULL ENABLE);
  ALTER TABLE "SGBSTDN" MODIFY ("SGBSTDN_STYP_CODE" NOT NULL ENABLE);
  ALTER TABLE "SGBSTDN" MODIFY ("SGBSTDN_ACTIVITY_DATE" NOT NULL ENABLE);
