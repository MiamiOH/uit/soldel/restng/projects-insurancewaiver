BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SPRIDEN';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
--------------------------------------------------------
--  DDL for Table SPRIDEN
--------------------------------------------------------

  CREATE TABLE SPRIDEN 
   (    SPRIDEN_PIDM NUMBER(8,0), 
    SPRIDEN_ID VARCHAR2(9 CHAR), 
    SPRIDEN_LAST_NAME VARCHAR2(60 CHAR), 
    SPRIDEN_FIRST_NAME VARCHAR2(60 BYTE), 
    SPRIDEN_MI VARCHAR2(60 BYTE), 
    SPRIDEN_CHANGE_IND VARCHAR2(1 CHAR), 
    SPRIDEN_ENTITY_IND VARCHAR2(1 CHAR), 
    SPRIDEN_ACTIVITY_DATE DATE, 
    SPRIDEN_USER VARCHAR2(30 CHAR), 
    SPRIDEN_ORIGIN VARCHAR2(30 CHAR), 
    SPRIDEN_SEARCH_LAST_NAME VARCHAR2(60 CHAR), 
    SPRIDEN_SEARCH_FIRST_NAME VARCHAR2(60 BYTE), 
    SPRIDEN_SEARCH_MI VARCHAR2(60 BYTE), 
    SPRIDEN_SOUNDEX_LAST_NAME CHAR(4 CHAR), 
    SPRIDEN_SOUNDEX_FIRST_NAME CHAR(4 CHAR), 
    SPRIDEN_NTYP_CODE VARCHAR2(4 CHAR), 
    SPRIDEN_CREATE_USER VARCHAR2(30 CHAR), 
    SPRIDEN_CREATE_DATE DATE, 
    SPRIDEN_DATA_ORIGIN VARCHAR2(30 CHAR), 
    SPRIDEN_CREATE_FDMN_CODE VARCHAR2(30 CHAR), 
    SPRIDEN_SURNAME_PREFIX VARCHAR2(60 BYTE), 
    SPRIDEN_SURROGATE_ID NUMBER(19,0), 
    SPRIDEN_VERSION NUMBER(19,0), 
    SPRIDEN_USER_ID VARCHAR2(30 CHAR), 
    SPRIDEN_VPDI_CODE VARCHAR2(6 CHAR)
   )  ENABLE ROW MOVEMENT ;

   COMMENT ON COLUMN SPRIDEN.SPRIDEN_PIDM IS 'Internal identification number of the person.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_ID IS 'This field defines the identification number used to access person on-line.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_LAST_NAME IS 'This field defines the last name of person.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_FIRST_NAME IS 'This field identifies the first name of person.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_MI IS 'This field identifies the middle name of person.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_CHANGE_IND IS 'This field identifies whether type of change made to the record was an ID       number change or a name change. Valid values: I - ID change, N - name change.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_ENTITY_IND IS 'This field identifies whether record is person or non-person record.  It does   not display on the form. Valid values:  P - person, C - non-person.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_ACTIVITY_DATE IS 'This field defines the most current date record is created or changed.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_USER IS 'USER: The ID for the user that most recently updated the record.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_ORIGIN IS 'ORIGIN: The name of the Banner Object that was used most recently to update the row in the spriden table.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_SEARCH_LAST_NAME IS 'The Last Name field with all spaces and punctuation removed and all letters capitalized.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_SEARCH_FIRST_NAME IS 'The First Name field with all spaces and punctuation removed and all letters capitalized.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_SEARCH_MI IS 'The MI (Middle Initial) field with all spaces and punctuation removed and all letters capitalized.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_SOUNDEX_LAST_NAME IS 'The Last Name field in SOUNDEX phonetic format.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_SOUNDEX_FIRST_NAME IS 'The First Name field in SOUNDEX phonetic format.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_NTYP_CODE IS 'NAME TYPE CODE: The field is used to store the code that represents the name type associated with a persons name.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_CREATE_USER IS 'Record Create User: This field contains Banner User ID which created new record';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_CREATE_DATE IS 'Record Create Date: This field contains date new record created';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_DATA_ORIGIN IS 'DATA SOURCE: Source system that generated the data';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_CREATE_FDMN_CODE IS 'PII DOMAIN: PII Domain of the user who created the spriden row.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_SURNAME_PREFIX IS 'SURNAME PREFIX: Name tag preceding the last name or surname.  (Van, Von, Mac, etc.)';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_SURROGATE_ID IS 'SURROGATE ID: Immutable unique key';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_VERSION IS 'VERSION: Optimistic lock token.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_USER_ID IS 'USER ID: The user ID of the person who inserted or last updated this record.';
   COMMENT ON COLUMN SPRIDEN.SPRIDEN_VPDI_CODE IS 'VPDI CODE: Multi-entity processing code.';
   COMMENT ON TABLE SPRIDEN  IS 'Person Identification/Name Repeating Table';
--------------------------------------------------------
--  DDL for Index SPRIDEN_CHANGE_IND
--------------------------------------------------------

  CREATE INDEX SPRIDEN_CHANGE_IND ON SPRIDEN (NVL(SPRIDEN_CHANGE_IND,'null')) 
  ;
--------------------------------------------------------
--  DDL for Index SPRIDEN_INDEX_SEARCH
--------------------------------------------------------

  CREATE INDEX SPRIDEN_INDEX_SEARCH ON SPRIDEN (SPRIDEN_SEARCH_LAST_NAME, SPRIDEN_SEARCH_FIRST_NAME, SPRIDEN_SEARCH_MI, SPRIDEN_SURNAME_PREFIX) 
  ;
--------------------------------------------------------
--  DDL for Index SPRIDEN_INDEX_SOUNDEX
--------------------------------------------------------

  CREATE INDEX SPRIDEN_INDEX_SOUNDEX ON SPRIDEN (SPRIDEN_SOUNDEX_LAST_NAME, SPRIDEN_SOUNDEX_FIRST_NAME) 
  ;
--------------------------------------------------------
--  DDL for Index SPRIDEN_INDEX_PERS
--------------------------------------------------------

  CREATE INDEX SPRIDEN_INDEX_PERS ON SPRIDEN (SPRIDEN_LAST_NAME, SPRIDEN_FIRST_NAME, SPRIDEN_MI, SPRIDEN_SURNAME_PREFIX, SPRIDEN_ENTITY_IND, SPRIDEN_CHANGE_IND) 
  ;
--------------------------------------------------------
--  DDL for Index SPRIDEN_INDEX_ID
--------------------------------------------------------

  CREATE INDEX SPRIDEN_INDEX_ID ON SPRIDEN (SPRIDEN_ID, SPRIDEN_ENTITY_IND, SPRIDEN_CHANGE_IND) 
  ;
--------------------------------------------------------
--  DDL for Index SPRIDEN_KEY_INDEX
--------------------------------------------------------

  CREATE UNIQUE INDEX SPRIDEN_KEY_INDEX ON SPRIDEN (SPRIDEN_PIDM, SPRIDEN_ID, SPRIDEN_LAST_NAME, SPRIDEN_FIRST_NAME, SPRIDEN_MI, SPRIDEN_SURNAME_PREFIX, SPRIDEN_CHANGE_IND, SPRIDEN_NTYP_CODE) 
  ;
--------------------------------------------------------
--  DDL for Index SPRIDEN_PIDM_INDEX
--------------------------------------------------------

  CREATE INDEX SPRIDEN_PIDM_INDEX ON SPRIDEN (SPRIDEN_PIDM, SPRIDEN_CHANGE_IND) 
  ;
