create or replace FUNCTION fz_getIntlOrDomStuTypForWaiver(pidm spriden.spriden_pidm%TYPE) RETURN VARCHAR2 IS
 /*******************************************************************************
  *   This function is a replacement of FZ_GETINTLORDOMSTUTYP function (For Insurance Waiver USE ONLY).
  *
  *   The original function (FZ_GETINTLORDOMSTUTYP) has been created to easily 
  *   determine if a student is an international student or a domestic one.  
  *   This function was specifically written for the Student Health Waiver 
  *   process to determine if there was additional verbage that should be 
  *   emphasized for international students verse domestic students.
  *
  *   This function chage the logic to determine indernational/domestic student 
  *   based on the original function. For New students, the SARADAP table will 
  *   take priority when determining status. For Continuing students, 
  *   the GORVISA table will take priority.
  *
  *   Author:  Mingchao Liao
  *   Date:    07/05/2017
  *
  *   AUDIT TRAIL:
  *   DATE		    PRJ-TSK			UNIQUE ID       VERSION
  *   04/30/2018   fix: could not get new admitted       liaom
  *                student's sgbstdn status before 
  *                term starting
  *
  ******************************************************************************/

studentType VARCHAR2(3) := '';
  admtCodeType VARCHAR2(3) := '';
  visaType varchar2(2) := '';
  term sgbstdn.sgbstdn_term_code_eff%type;
  levl sgbstdn.sgbstdn_levl_code%type;
  camp sgbstdn.sgbstdn_camp_code%type;
  styp sgbstdn.sgbstdn_styp_code%type;
  ApplStatus varchar2(100) := '';
  
  BEGIN
    -- ************************************************************************
    -- *  get student type from sgbstdn, and group into                       *
    -- *    'N': new admitted students                                        *
    -- *    'C': continuing students                                          *
    -- ************************************************************************
    begin
      select sgbstdn_term_code_eff, sgbstdn_levl_code, sgbstdn_camp_code, sgbstdn_styp_code into term, levl, camp, styp
        from sgbstdn o
        where sgbstdn_pidm = pidm
        and sgbstdn_term_code_eff =  (select MAX(g2.SGBSTDN_TERM_CODE_EFF)
                                      FROM SGBSTDN g2
                                      WHERE g2.SGBSTDN_PIDM = o.sgbstdn_pidm);

      exception when no_data_found then
        return null;
    end;
    
    if(styp = 'N') OR (styp = 'T') or (styp = 'R')
      then
        styp := 'N';  -- new student
      else
        styp := 'C';  -- continuing student
    end if;

    -- ************************************************************************
    -- * get visa type from GORVISA, and group into                           *
    -- *   'I' : Non-Immigrant Visa Type (e.g. F1, J1)                        *
    -- *   'NI': Immigrant Visa Type (e.g. PR, IM)                            * 
    -- *   'NR : No record in the table                                       *
    -- ************************************************************************
    BEGIN
      SELECT y.gorvisa_vtyp_code into visaType
      FROM gorvisa y
      WHERE gorvisa_pidm = pidm
      and y.gorvisa_seq_no = (
        select max(x.gorvisa_seq_no)
        from gorvisa x
        where x.gorvisa_pidm = pidm
      );
    
      exception when no_data_found then
        visaType := null;
    END;
    if (visaType in ('IM', 'NV', 'PR', 'RA', 'WA', 'XX', 'ZZ')) THEN
        visaType := 'NI';
      ELSIF (visaType is not null) THEN
        visaType := 'I';
      else
        visaType := 'NR';
    END IF;

    -- *************************************************************************
    -- * get admitted code type from SARADAP, and group into                   *
    -- *   'I' :  International admt code (e.g. OI, RI)                        *
    -- *   'NI':  Domistic admt code (e.g. OM, OE, OA)                         *
    -- *   'E' :  Record exist, but saradap_admt_code is 'null'                *
    -- *   'NR :  No record in the table                                       *
    -- *************************************************************************
    
    begin
      select saradap_admt_code into admtCodeType
        from saradap
        where saradap_pidm = pidm
        and saradap_levl_code = levl
        and saradap_camp_code = camp
        and saradap_term_code_entry = term
        and saradap_appl_no = FZ_GET_MAX_SARADAP_APPLICATION(pidm,term);
        
      exception when no_data_found then
        admtCodeType := 'N_R';
    end;
      
    if admtCodeType in ('OI', 'RI') then
      admtCodeType := 'I';
    elsif admtCodeType = 'N_R' then
      admtCodeType := 'NR';
    elsif admtCodeType is null then
      admtCodeType := 'E';
    else
      admtCodeType := 'NI';
    end if;

    -- ************************************************************************
    -- *  apply logic to determine 'I' or 'D', based on documentation:        *
    -- *     https://docs.google.com/a/miamioh.edu/document/d/1hx_5INKYe1sf   *
    -- *         djYwOSu_chH8dMgSlgsLY2q388OZJ74/edit?usp=sharing             *
    -- ************************************************************************
    IF(styp = 'N') -- if is new student
      THEN
        IF (visaType = 'I' AND admtCodeType = 'I')
          OR (visaType = 'NI' AND admtCodeType = 'I')
          OR (visaType = 'NR' AND admtCodeType = 'I')
          THEN
            studentType := 'I';
        else
            studentType := 'D';
        END IF;
      ELSE -- if is contining student
        IF (visaType = 'I')
          THEN
            studentType := 'I';
        else
            studentType := 'D';
        END IF;
    END IF;
  RETURN studentType;
END fz_getIntlOrDomStuTypForWaiver;

/
