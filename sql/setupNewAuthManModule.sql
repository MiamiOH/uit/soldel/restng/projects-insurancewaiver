declare

  v_authman_app_name varchar2(64) := null;
  v_authman_module_name varchar2(64) := null;
  v_authman_entity_name varchar2(64) := null;
  v_authman_grantkey varchar2(64) := null;

begin

  v_authman_app_name := 'Student Insurance Web Services';
  v_authman_module_name := 'Waiver';
  v_authman_entity_name := 'STUINS_WS_USER';
  v_authman_grantkey := 'view';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_grantkey := 'update';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');


end;
/
