create or replace PACKAGE GZKEMAIL AS
/*****************************************************************************/
/* FILE NAME: GZKEMAIL.sql                                                   */
/*Copyright (c) [2000] Miami University, All Rights Reserved.                */
/*                                                                           */
/*Miami University grants you ("Licensee") a non-exclusive, royalty free,    */
/*license to use, modify and redistribute this software in source and        */
/*binary code form, provided that i) this copyright notice and license       */
/*appear on all copies of the software; and ii) Licensee does not utilize    */
/*the software in a manner which is disparaging to Miami University.         */
/*                                                                           */
/*This software is provided "AS IS" and any express or implied warranties,   */
/*including, but not limited to, the implied warranties of merchantability   */
/*and fitness for a particular purpose are disclaimed. It has been tested    */
/*and is believed to work as intended within Miami University's              */
/*environment. Miami University does not warrant this software to work as    */
/*designed in any other environment.                                         */
/*                                                                           */
/*                                                                           */
/*AUTHOR:       Don Kidd                                                     */
/*                                                                           */
/*DESCRIPTION: Package created to get a person's email address based on      */
/*  their UNIQUEID in Banner                                                 */
/*****************************************************************************/

 FUNCTION f_getemail_address(
          p_pidm                spriden.spriden_pidm%TYPE,
          p_emal_code           varchar2)
  RETURN VARCHAR2;


END GZKEMAIL;

/

create or replace PACKAGE BODY GZKEMAIL AS
  /*****************************************************************************/
/* FILE NAME: GZKEMAI1.sql                                                   */
/*Copyright (c) [2000] Miami University, All Rights Reserved.                */
/*                                                                           */
/*Miami University grants you ("Licensee") a non-exclusive, royalty free,    */
/*license to use, modify and redistribute this software in source and        */
/*binary code form, provided that i) this copyright notice and license       */
/*appear on all copies of the software; and ii) Licensee does not utilize    */
/*the software in a manner which is disparaging to Miami University.         */
/*                                                                           */
/*This software is provided "AS IS" and any express or implied warranties,   */
/*including, but not limited to, the implied warranties of merchantability   */
/*and fitness for a particular purpose are disclaimed. It has been tested    */
/*and is believed to work as intended within Miami University's              */
/*environment. Miami University does not warrant this software to work as    */
/*designed in any other environment.                                         */
/*                                                                           */
/*                                                                           */
/*AUTHOR:       Don Kidd                                                     */
/*                                                                           */
/*DESCRIPTION: Package created to get a person's email address based on      */
/*  their UNIQUEID in Banner                                                 */
/*****************************************************************************/



  FUNCTION f_getemail_address(
          p_pidm                spriden.spriden_pidm%TYPE,
          p_emal_code           varchar2)
  RETURN VARCHAR2 AS
  	email varchar2(150);

  BEGIN
	select lower(szbuniq_unique_id) || '@miamioh.edu' into email from szbuniq where szbuniq_pidm = p_pidm;
	return email;

  END f_getemail_address;

END GZKEMAIL;
/
