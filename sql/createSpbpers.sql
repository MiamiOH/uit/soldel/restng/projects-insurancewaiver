BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SPBPERS';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
--------------------------------------------------------
--  DDL for Table SPBPERS
--------------------------------------------------------

  CREATE TABLE SPBPERS 
   (    SPBPERS_PIDM NUMBER(8,0), 
    SPBPERS_SSN VARCHAR2(15 BYTE), 
    SPBPERS_BIRTH_DATE DATE, 
    SPBPERS_LGCY_CODE VARCHAR2(1 CHAR), 
    SPBPERS_ETHN_CODE VARCHAR2(2 CHAR), 
    SPBPERS_MRTL_CODE VARCHAR2(1 CHAR), 
    SPBPERS_RELG_CODE VARCHAR2(2 CHAR), 
    SPBPERS_SEX VARCHAR2(1 CHAR), 
    SPBPERS_CONFID_IND VARCHAR2(1 CHAR), 
    SPBPERS_DEAD_IND VARCHAR2(1 CHAR), 
    SPBPERS_VETC_FILE_NUMBER VARCHAR2(10 CHAR), 
    SPBPERS_LEGAL_NAME VARCHAR2(500 BYTE), 
    SPBPERS_PREF_FIRST_NAME VARCHAR2(60 BYTE), 
    SPBPERS_NAME_PREFIX VARCHAR2(20 CHAR), 
    SPBPERS_NAME_SUFFIX VARCHAR2(20 CHAR), 
    SPBPERS_ACTIVITY_DATE DATE, 
    SPBPERS_VERA_IND VARCHAR2(1 CHAR), 
    SPBPERS_CITZ_IND VARCHAR2(1 CHAR), 
    SPBPERS_DEAD_DATE DATE, 
    SPBPERS_PIN RAW(1), 
    SPBPERS_CITZ_CODE VARCHAR2(2 CHAR), 
    SPBPERS_HAIR_CODE VARCHAR2(2 CHAR), 
    SPBPERS_EYES_CODE VARCHAR2(2 CHAR), 
    SPBPERS_CITY_BIRTH VARCHAR2(50 BYTE), 
    SPBPERS_STAT_CODE_BIRTH VARCHAR2(3 CHAR), 
    SPBPERS_DRIVER_LICENSE VARCHAR2(20 CHAR), 
    SPBPERS_STAT_CODE_DRIVER VARCHAR2(3 CHAR), 
    SPBPERS_NATN_CODE_DRIVER VARCHAR2(5 CHAR), 
    SPBPERS_UOMS_CODE_HEIGHT VARCHAR2(4 CHAR), 
    SPBPERS_HEIGHT NUMBER(2,0), 
    SPBPERS_UOMS_CODE_WEIGHT VARCHAR2(4 CHAR), 
    SPBPERS_WEIGHT NUMBER(4,0), 
    SPBPERS_SDVET_IND VARCHAR2(1 CHAR), 
    SPBPERS_LICENSE_ISSUED_DATE DATE, 
    SPBPERS_LICENSE_EXPIRES_DATE DATE, 
    SPBPERS_INCAR_IND VARCHAR2(1 CHAR), 
    SPBPERS_WEBID RAW(1), 
    SPBPERS_WEB_LAST_ACCESS RAW(1), 
    SPBPERS_PIN_DISABLED_IND RAW(1), 
    SPBPERS_ITIN NUMBER(9,0), 
    SPBPERS_ACTIVE_DUTY_SEPR_DATE DATE, 
    SPBPERS_DATA_ORIGIN VARCHAR2(30 CHAR), 
    SPBPERS_USER_ID VARCHAR2(30 CHAR), 
    SPBPERS_ETHN_CDE VARCHAR2(1 CHAR), 
    SPBPERS_CONFIRMED_RE_CDE VARCHAR2(1 CHAR), 
    SPBPERS_CONFIRMED_RE_DATE DATE, 
    SPBPERS_ARMED_SERV_MED_VET_IND VARCHAR2(1 BYTE), 
    SPBPERS_SURROGATE_ID NUMBER(19,0), 
    SPBPERS_VERSION NUMBER(19,0), 
    SPBPERS_VPDI_CODE VARCHAR2(6 CHAR)
   ) ;

   COMMENT ON COLUMN SPBPERS.SPBPERS_PIDM IS 'Internal Identification Number of Person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_SSN IS 'This field maintains person social security number.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_BIRTH_DATE IS 'This field maintains person birth date.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_LGCY_CODE IS 'This field maintains legacy code associated with person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_ETHN_CODE IS 'ETHNIC CODE: This field identifies the ethnic code referenced on the General Person Form (SPAPERS) and by the Source/Base Institution Year Form (SOABGIY). The code is mapped to other codes which are required  for government reporting.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_MRTL_CODE IS 'This field maintains martial status associated with person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_RELG_CODE IS 'This field maintains religious affiliation associated with person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_SEX IS 'This field maintains the sex of person. Valid values are: M - Male, F - Female, N - Unknown.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_CONFID_IND IS 'This field identifies if a person record is confidential Valid value is: Y - confidential.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_DEAD_IND IS 'This field indicates if a person is deceased. Valid value is: Y - deceased.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_VETC_FILE_NUMBER IS 'This field maintains veteran identification number associated with person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_LEGAL_NAME IS 'This field maintains legal name associated with person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_PREF_FIRST_NAME IS 'This field maintains the preferred first name associated with person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_NAME_PREFIX IS 'This field maintains the prefix (Mr, Mrs, etc) used before person name.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_NAME_SUFFIX IS 'This field maintains the suffix (Jr, Sr, etc) used after person name.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_ACTIVITY_DATE IS 'This field defines the most current date a record is added or changed.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_VERA_IND IS 'Veteran Category. (NULL) Not a Veteran, (B) Protected veteran choosing not to self-identify the classification, (O) Active Wartime or Campaign Badge Veteran, (V) Not a Protected Veteran';
   COMMENT ON COLUMN SPBPERS.SPBPERS_CITZ_IND IS 'Citizen Indicator.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_DEAD_DATE IS 'Person Deceased Date.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_PIN IS 'Person Identification Number.  This column is obsolete, now gobtpac_pin';
   COMMENT ON COLUMN SPBPERS.SPBPERS_CITZ_CODE IS 'Person Citizen Type';
   COMMENT ON COLUMN SPBPERS.SPBPERS_HAIR_CODE IS 'The hair color of the person being defined.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_EYES_CODE IS 'The eye color of the person being defined.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_CITY_BIRTH IS 'The city where the person was born.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_STAT_CODE_BIRTH IS 'The state in which the person was born.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_DRIVER_LICENSE IS 'The Driver License Number as it appears on the actual license.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_STAT_CODE_DRIVER IS 'The State code describing the state the driver license was issued.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_NATN_CODE_DRIVER IS 'The Nation code associated with the Nation the Driver license was issued.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_UOMS_CODE_HEIGHT IS 'The Unit of Measure describing the height value.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_HEIGHT IS 'The number value describing the height of the person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_UOMS_CODE_WEIGHT IS 'The Unit of Measure describing the weight value.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_WEIGHT IS 'The number value describing the weight of the person.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_SDVET_IND IS 'Indicator to identify an individual as a special disabled veteran.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_LICENSE_ISSUED_DATE IS 'The issue date of the individuals driver license.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_LICENSE_EXPIRES_DATE IS 'The expiration date of the individuals driver license.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_INCAR_IND IS 'The indication of the individuals incarceration.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_WEBID IS 'Web identification session token is now obsolete.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_WEB_LAST_ACCESS IS 'Web last access time stamp is now obsolete.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_PIN_DISABLED_IND IS 'Indicates whether or not the PIN number is disabled. This column is obsolete, now gobtpac_pin_disabled_ind';
   COMMENT ON COLUMN SPBPERS.SPBPERS_ITIN IS 'The international tax id number.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_ACTIVE_DUTY_SEPR_DATE IS 'Active Duty Separation Date: The Date that the person was separated from active duty.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_DATA_ORIGIN IS 'DATA ORIGIN: Source system that created or updated the row';
   COMMENT ON COLUMN SPBPERS.SPBPERS_USER_ID IS 'USER ID: User who inserted or last update the data';
   COMMENT ON COLUMN SPBPERS.SPBPERS_ETHN_CDE IS 'ETHNIC CODE: This field identifies the ethnic code defined by the U.S. government. The valid values are 1 - Not Hispanic or Latino, 2 - Hispanic or Latino, or null.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_CONFIRMED_RE_CDE IS 'RACE AND ETHNICITY CONFIRMED: This field identifies the race and ethnicity has been confirmed. Valid values are (Y)es, (N)o and Null.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_CONFIRMED_RE_DATE IS 'RACE AND ETHNICITY CONFIRMED DATE: This field identifies when the race and ethnicity has been confirmed.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_ARMED_SERV_MED_VET_IND IS 'ARMED FORCES SERVICE MEDAL INDICATOR:  Armed Forces Service Medal Indicator. Valid values are (Y)es, (N)o. Default value is N.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_SURROGATE_ID IS 'SURROGATE ID: Immutable unique key';
   COMMENT ON COLUMN SPBPERS.SPBPERS_VERSION IS 'VERSION: Optimistic lock token.';
   COMMENT ON COLUMN SPBPERS.SPBPERS_VPDI_CODE IS 'VPDI CODE: Multi-entity processing code.';
   COMMENT ON TABLE SPBPERS  IS 'Basic Person Base Table';
--------------------------------------------------------
--  DDL for Index SPBPERS_SSN_INDEX
--------------------------------------------------------

  CREATE INDEX SPBPERS_SSN_INDEX ON SPBPERS (SPBPERS_SSN) 
  ;
--------------------------------------------------------
--  DDL for Index SPBPERS_BIRTH_DATE_INDEX
--------------------------------------------------------

  CREATE INDEX SPBPERS_BIRTH_DATE_INDEX ON SPBPERS (SPBPERS_BIRTH_DATE) 
  ;
--------------------------------------------------------
--  DDL for Index PK_SPBPERS
--------------------------------------------------------

  CREATE UNIQUE INDEX PK_SPBPERS ON SPBPERS (SPBPERS_PIDM) 
  ;
  ALTER TABLE SPBPERS MODIFY (SPBPERS_PIDM NOT NULL ENABLE);
  ALTER TABLE SPBPERS MODIFY (SPBPERS_ACTIVITY_DATE NOT NULL ENABLE);
  ALTER TABLE SPBPERS MODIFY (SPBPERS_ARMED_SERV_MED_VET_IND NOT NULL ENABLE);
