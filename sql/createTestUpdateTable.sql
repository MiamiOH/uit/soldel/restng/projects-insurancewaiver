BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE stuins_test_update';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE stuins_test_update
   (
    stuins_uid varchar2(8),
    stuins_eligible varchar2(2),
    stuins_completed_date varchar2(24)
   ) ;
