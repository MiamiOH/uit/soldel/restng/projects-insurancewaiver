BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE WAS_CLIENT';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE WAS_CLIENT 
   (    CLIENT_ID NUMBER, 
    UNIQUEID VARCHAR2(128 BYTE), 
    DEFAULT_PASSWORD VARCHAR2(16 BYTE), 
    PIDM NUMBER, 
    TYPE VARCHAR2(1 BYTE), 
    PW_POLICY_ID NUMBER, 
    LAST_PW_CHANGE DATE, 
    GRACE_START DATE, 
    QUESTION VARCHAR2(300 BYTE), 
    ANSWER VARCHAR2(300 BYTE), 
    REFERS_TO NUMBER, 
    APPLICANT_TERM_START DATE, 
    FORCE_PW_CHANGE NUMBER(*,0)
   ) ;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE WAS_EXTAPP';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

  CREATE TABLE WAS_EXTAPP 
   (    EXTAPP_ID NUMBER, 
    NAME VARCHAR2(128 BYTE), 
    INSTRUCTIONS LONG, 
    TITLE VARCHAR2(128 BYTE), 
    DISPLAY VARCHAR2(1 BYTE), 
    APPLY VARCHAR2(1 BYTE), 
    SHOW VARCHAR2(1 BYTE), 
    STATUS VARCHAR2(1 BYTE), 
    COMPLETE VARCHAR2(1 BYTE), 
    BUTTON1_TYPE VARCHAR2(1 BYTE), 
    BUTTON1_LABEL VARCHAR2(128 BYTE), 
    BUTTON1_URL VARCHAR2(512 BYTE), 
    BUTTON2_TYPE VARCHAR2(1 BYTE), 
    BUTTON2_LABEL VARCHAR2(128 BYTE), 
    BUTTON2_URL VARCHAR2(512 BYTE), 
    SHOW_UI VARCHAR2(1 BYTE), 
    LOCATION VARCHAR2(512 BYTE), 
    NO_UI VARCHAR2(1 BYTE), 
    APPNAME VARCHAR2(32 BYTE), 
    HIT_RATE NUMBER(*,0), 
    NOTIFY_ONLY NUMBER(*,0), 
    ADMIN_SECRET VARCHAR2(128 BYTE)
   ) ;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE WAS_EXTAPP_LOG';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE WAS_EXTAPP_LOG 
   (    EXTAPP_ID NUMBER, 
    CLIENT_ID NUMBER, 
    SERVICE_ID NUMBER, 
    SESSION_ID VARCHAR2(256 BYTE), 
    CHOICE VARCHAR2(128 BYTE), 
    CHOICE_TYPE VARCHAR2(1 BYTE), 
    COMPLETE VARCHAR2(1 BYTE), 
    USER_DATA LONG, 
    TIMESTAMP DATE
   ) ;
/

