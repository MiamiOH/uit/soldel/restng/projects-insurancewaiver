BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SHRDGMR';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE "SHRDGMR"
   (	"SHRDGMR_PIDM" NUMBER(8,0) NOT NULL ENABLE,
	"SHRDGMR_SEQ_NO" NUMBER(2,0) NOT NULL ENABLE,
	"SHRDGMR_DEGC_CODE" VARCHAR2(6 CHAR),
	"SHRDGMR_DEGS_CODE" VARCHAR2(2 CHAR) NOT NULL ENABLE,
	"SHRDGMR_LEVL_CODE" VARCHAR2(2 CHAR),
	"SHRDGMR_COLL_CODE_1" VARCHAR2(2 CHAR),
	"SHRDGMR_MAJR_CODE_1" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_MINR_1" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_1" VARCHAR2(4 CHAR),
	"SHRDGMR_COLL_CODE_2" VARCHAR2(2 CHAR),
	"SHRDGMR_MAJR_CODE_2" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_MINR_2" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_2" VARCHAR2(4 CHAR),
	"SHRDGMR_APPL_DATE" DATE,
	"SHRDGMR_GRAD_DATE" DATE,
	"SHRDGMR_ACYR_CODE_BULLETIN" VARCHAR2(4 CHAR),
	"SHRDGMR_ACTIVITY_DATE" DATE NOT NULL ENABLE,
	"SHRDGMR_MAJR_CODE_MINR_1_2" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_1_2" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_1_3" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_MINR_2_2" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_2_2" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_2_3" VARCHAR2(4 CHAR),
	"SHRDGMR_TERM_CODE_STUREC" VARCHAR2(6 CHAR) NOT NULL ENABLE,
	"SHRDGMR_MAJR_CODE_1_2" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_2_2" VARCHAR2(4 CHAR),
	"SHRDGMR_CAMP_CODE" VARCHAR2(3 CHAR),
	"SHRDGMR_TERM_CODE_GRAD" VARCHAR2(6 CHAR),
	"SHRDGMR_ACYR_CODE" VARCHAR2(4 CHAR),
	"SHRDGMR_GRST_CODE" VARCHAR2(3 CHAR),
	"SHRDGMR_FEE_IND" VARCHAR2(1 CHAR),
	"SHRDGMR_FEE_DATE" DATE,
	"SHRDGMR_AUTHORIZED" VARCHAR2(33 CHAR),
	"SHRDGMR_TERM_CODE_COMPLETED" VARCHAR2(6 CHAR),
	"SHRDGMR_DEGC_CODE_DUAL" VARCHAR2(6 CHAR),
	"SHRDGMR_LEVL_CODE_DUAL" VARCHAR2(2 CHAR),
	"SHRDGMR_DEPT_CODE_DUAL" VARCHAR2(4 CHAR),
	"SHRDGMR_COLL_CODE_DUAL" VARCHAR2(2 CHAR),
	"SHRDGMR_MAJR_CODE_DUAL" VARCHAR2(4 CHAR),
	"SHRDGMR_DEPT_CODE" VARCHAR2(4 CHAR),
	"SHRDGMR_DEPT_CODE_2" VARCHAR2(4 CHAR),
	"SHRDGMR_PROGRAM" VARCHAR2(12 CHAR),
	"SHRDGMR_TERM_CODE_CTLG_1" VARCHAR2(6 CHAR),
	"SHRDGMR_DEPT_CODE_1_2" VARCHAR2(4 CHAR),
	"SHRDGMR_DEPT_CODE_2_2" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_121" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_122" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_123" VARCHAR2(4 CHAR),
	"SHRDGMR_TERM_CODE_CTLG_2" VARCHAR2(6 CHAR),
	"SHRDGMR_CAMP_CODE_2" VARCHAR2(3 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_221" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_222" VARCHAR2(4 CHAR),
	"SHRDGMR_MAJR_CODE_CONC_223" VARCHAR2(4 CHAR),
	"SHRDGMR_CURR_RULE_1" NUMBER(8,0),
	"SHRDGMR_CMJR_RULE_1_1" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_11_1" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_11_2" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_11_3" NUMBER(8,0),
	"SHRDGMR_CMJR_RULE_1_2" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_12_1" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_12_2" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_12_3" NUMBER(8,0),
	"SHRDGMR_CMNR_RULE_1_1" NUMBER(8,0),
	"SHRDGMR_CMNR_RULE_1_2" NUMBER(8,0),
	"SHRDGMR_CURR_RULE_2" NUMBER(8,0),
	"SHRDGMR_CMJR_RULE_2_1" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_21_1" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_21_2" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_21_3" NUMBER(8,0),
	"SHRDGMR_CMJR_RULE_2_2" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_22_1" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_22_2" NUMBER(8,0),
	"SHRDGMR_CCON_RULE_22_3" NUMBER(8,0),
	"SHRDGMR_CMNR_RULE_2_1" NUMBER(8,0),
	"SHRDGMR_CMNR_RULE_2_2" NUMBER(8,0),
	"SHRDGMR_DATA_ORIGIN" VARCHAR2(30 CHAR),
	"SHRDGMR_USER_ID" VARCHAR2(30 CHAR),
	"SHRDGMR_STSP_KEY_SEQUENCE" NUMBER(2,0),
	"SHRDGMR_SURROGATE_ID" NUMBER(19,0),
	"SHRDGMR_VERSION" NUMBER(19,0),
	"SHRDGMR_VPDI_CODE" VARCHAR2(6 CHAR),
	 CONSTRAINT "PK_SHRDGMR" PRIMARY KEY ("SHRDGMR_PIDM", "SHRDGMR_SEQ_NO") );

   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_PIDM" IS 'Internal identification number of the person';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_SEQ_NO" IS 'Degree Sequence Number. Number assigned to each degree student has.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DEGC_CODE" IS 'Degree Code. Students degree code (BS, BA, etc.).';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DEGS_CODE" IS 'Degree Status Code. Status of students degree, for example sought, awarded, etc.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_LEVL_CODE" IS 'Degree Level Code. Level to which degree applies (undergraduate, graduate, etc).';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_COLL_CODE_1" IS 'Degree College Code 1. College offering the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_1" IS 'Degree Major Code 1. First major associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_MINR_1" IS 'Degree Minor Code 1. First minor associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_1" IS 'Degree Concentration Code 1. First concentration associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_COLL_CODE_2" IS 'Degree College Code Secondary. Secondary college offering the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_2" IS 'Degree Major Code 1 Secondary. First major for the secondary curriculum associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_MINR_2" IS 'Degree Minor Code 1 Secondary. First minor for the secondary curriculum associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_2" IS 'Degree Concentration Code 1 Secondary. First concentration for the secondary curriculum associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_APPL_DATE" IS 'Degee Application Date. Date of degree application. Defaults to current date on entry of degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_GRAD_DATE" IS 'Graduation Date. Graduation date of student.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_ACYR_CODE_BULLETIN" IS 'Degree Bulletin Year. Year of bulletin or catalog under which degree is being offered.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_ACTIVITY_DATE" IS 'Activity Date. Specifies the date record was created or updated.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_MINR_1_2" IS 'Degree Minor Code 2. Second minor associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_1_2" IS 'Degree Concentration Code 2. Second concentration associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_1_3" IS 'Degree Concentration Code 3. Third concentration associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_MINR_2_2" IS 'Degree Minor Code 2 Secondary. Second minor for the secondary curriculum associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_2_2" IS 'Degree Concentration Code 2 Secondary. Second concentration for the secondary curriculum associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_2_3" IS 'Degree Concentration Code 3 Secondary. Third concentration for the secondary curriculum associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_TERM_CODE_STUREC" IS 'Term Code. The term associated with the general student record when degree information is defaulted.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_1_2" IS 'Degree Major Code 2. Second major associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_2_2" IS 'Degree Major Code 2 Secondary. Second major for the secondary curriculum associated with the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CAMP_CODE" IS 'Degree Campus Code. Campus offering the degree.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_TERM_CODE_GRAD" IS 'Term in which the student graduates.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_ACYR_CODE" IS 'Year in which the student graduates.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_GRST_CODE" IS 'Graduation status.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_FEE_IND" IS 'This field identifies whether a graduation fee has been assessed.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_FEE_DATE" IS 'This field identifies the date graduation fee has been assessed.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_AUTHORIZED" IS 'This field identifies the user and the grad status joined together as one field to identify the person who authorized graduation adds or changes.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_TERM_CODE_COMPLETED" IS 'Term designated as the term in which graduate work must be completed.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DEGC_CODE_DUAL" IS 'Degree code for dual degree';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_LEVL_CODE_DUAL" IS 'Level code for dual degree';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DEPT_CODE_DUAL" IS 'Department code for dual degree';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_COLL_CODE_DUAL" IS 'College code for dual degree';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_DUAL" IS 'Major code for dual degree';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DEPT_CODE" IS 'Department code for degree';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DEPT_CODE_2" IS '2nd department code for degree';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_PROGRAM" IS 'Curriculum 1 Program Code';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_TERM_CODE_CTLG_1" IS 'Curriculum 1 Catalog Term Code';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DEPT_CODE_1_2" IS 'Department 2, Curriculum 1 Code';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DEPT_CODE_2_2" IS 'Department 2, Curriculum 2 Code';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_121" IS 'Concentration Code 1 on  Major 2, Curriculum 1 Code';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_122" IS 'Concentration Code 2 on Major 2, Curriculum 1';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_123" IS 'Concentration Code 3 on Major 2, Curriculum 1';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_TERM_CODE_CTLG_2" IS 'Curriculum 2 Catalog Term Code';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CAMP_CODE_2" IS 'Curriculum 2 Campus Code';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_221" IS 'Concentration Code 1 on Major 2, Curriculum 2';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_222" IS 'Concentration Code 2 on Major 2, Curriculum 2';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_MAJR_CODE_CONC_223" IS 'Concentration Code 3 on Major 2, Curriculum 2';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CURR_RULE_1" IS 'Curriculum 1 Rule reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CMJR_RULE_1_1" IS 'Curriculum 1 Major 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_11_1" IS 'Concentration 1, Major 1, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_11_2" IS 'Concentration 2, Major 1, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_11_3" IS 'Concentration 3, Major 1, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CMJR_RULE_1_2" IS 'Major 2, Curriculum Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_12_1" IS 'Concentration 1, Major 2, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_12_2" IS 'Concentration 2, Major 2, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_12_3" IS 'Concentration 3, Major 2, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CMNR_RULE_1_1" IS 'Minor 1, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CMNR_RULE_1_2" IS 'Minor 2, Curriculum 1 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CURR_RULE_2" IS 'Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CMJR_RULE_2_1" IS 'Major 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_21_1" IS 'Concentration 1, Major 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_21_2" IS 'Concentration 2, Major 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_21_3" IS 'Concentration 3, Major 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CMJR_RULE_2_2" IS 'Major 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_22_1" IS 'Concentration 1, Major 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_22_2" IS 'Concentration 2, Major 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CCON_RULE_22_3" IS 'Concentration 3, Major 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CMNR_RULE_2_1" IS 'Minor 1, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_CMNR_RULE_2_2" IS 'Minor 2, Curriculum 2 Rule Reference';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_DATA_ORIGIN" IS 'DATA SOURCE: Source system that created or updated the row';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_USER_ID" IS 'USER ID: The most recent user to create or update a record.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_STSP_KEY_SEQUENCE" IS 'STUDY PATH SEQUENCE: This field is the key sequence of the study path of the learner curriculum the outcome curriculum originted from.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_SURROGATE_ID" IS 'SURROGATE ID: Immutable unique key';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_VERSION" IS 'VERSION: Optimistic lock token.';
   COMMENT ON COLUMN "SHRDGMR"."SHRDGMR_VPDI_CODE" IS 'VPDI CODE: Multi-entity processing code.';
   COMMENT ON TABLE "SHRDGMR"  IS 'Degree Table';

/
