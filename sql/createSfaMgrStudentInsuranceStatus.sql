BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE student_insurance_status';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE student_insurance_status
   (
    stuins_pidm NUMBER(8,0),
    stuins_termcode varchar2(6),
    stuins_eligible varchar(2),
    stuins_status varchar2(2),
    stuins_activity_date date
   ) ;

   COMMENT ON COLUMN student_insurance_status.stuins_pidm IS 'Internal Identification Number of Person.';
   COMMENT ON COLUMN student_insurance_status.stuins_termcode IS 'Academic term this status applies to.';
   COMMENT ON COLUMN student_insurance_status.stuins_termcode IS 'Eligible for insurance for academic term of this record.';
   COMMENT ON COLUMN student_insurance_status.stuins_status IS 'Student insurance status.';
   COMMENT ON COLUMN student_insurance_status.stuins_activity_date IS 'Activity date of last action.';
   COMMENT ON TABLE student_insurance_status  IS 'Student Insurance Status Table';

  CREATE UNIQUE INDEX PK_student_insurance_status ON student_insurance_status (stuins_pidm, stuins_termcode) ;
  CREATE INDEX student_pidm ON student_insurance_status (stuins_pidm) ;

  ALTER TABLE student_insurance_status MODIFY (stuins_pidm NOT NULL ENABLE);
  ALTER TABLE student_insurance_status MODIFY (stuins_activity_date NOT NULL ENABLE);
