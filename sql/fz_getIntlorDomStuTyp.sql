create or replace FUNCTION fz_getIntlorDomStuTyp(pidm spriden.spriden_pidm%TYPE) RETURN VARCHAR2 IS
 /******************************************************************************* 
  *  This function has been created to easily determine if a student is an 
  *   international student or a domestic one.  This function was specifically 
  *   written for the Student Health Waiver process to determine if there 
  *   was additional verbage that should be emphasized for international students
  *   verse domestic students.  
  *  
  *   This function looks to see if there is a visa on record for the student
  *   and then if there is non, if they are a first year student check to see
  *   if there is an international application for this semesters and based
  *   on the timing that of the application the visa info may not have been 
  *   provided to Miami yet.
  *
  *   Author:  Don Kidd 
  *   Date:    5/8/2015
  *
  *AUDIT TRAIL:
  *DATE		    PRJ-TSK			UNIQUE ID       VERSION
  *06/12/2017	                DUHY	  
  *Description:   Add paramater to FZ_GET_TERM to exclude Summer and Winter Term
  *
  ******************************************************************************/
  studentType  VARCHAR2(3) := '';
  VisaType varchar2(2) := '';
  term sgbstdn.sgbstdn_term_code_eff%type;
  levl sgbstdn.sgbstdn_levl_code%type;
  camp sgbstdn.sgbstdn_camp_code%type;
  styp sgbstdn.sgbstdn_styp_code%type;
  ApplStatus varchar2(100) := '';
  
   CURSOR check_for_application(pidm in spriden.spriden_pidm%TYPE ,levl in sgbstdn.sgbstdn_levl_code%type,camp in sgbstdn.sgbstdn_camp_code%type,term IN sgbstdn.sgbstdn_term_code_eff%type) IS
      select 'I'
      from saradap
      where saradap_pidm = pidm
      and saradap_admt_code like '%I'
      and saradap_levl_code = levl
      and saradap_camp_code = camp
      and saradap_term_code_entry = term
      and saradap_appl_no = FZ_GET_MAX_SARADAP_APPLICATION(pidm,term);
   

  BEGIN
    studentType := 'D';
      --Cal check Visa function to get the users current visa, if value is null then they don't have a valid visa
      select nvl(gokvisa.f_check_visa(spriden_id,sysdate),'XX') into VisaType
        from spriden
       where spriden_pidm = pidm
         and spriden_change_ind is null;
      if (VisaType in ('RA', 'PR', 'NV','ZZ','XX')) 
        THEN
     
        select sgbstdn_term_code_eff, sgbstdn_levl_code, sgbstdn_camp_code, sgbstdn_styp_code into term, levl, camp, styp
          from sgbstdn o
         where sgbstdn_pidm = pidm
           and sgbstdn_term_code_eff =  (select MAX(g2.SGBSTDN_TERM_CODE_EFF)
                                           FROM SGBSTDN g2
                                          WHERE g2.SGBSTDN_PIDM = o.sgbstdn_pidm
                                            and g2.sgbstdn_term_code_eff <= FZ_GET_TERM('N', 'S'));
          
        -- if Student is New, Transfer or Returning then check their application data 
        -- to see if they are and international applicant and they have not yet provided needed visa info.
        if (styp = 'N') OR (styp = 'T') or (styp = 'R') then 
          open check_for_application (pidm,levl,camp,term);
          FETCH check_for_application INTO ApplStatus;

          IF check_for_application%NOTFOUND THEN
          studentType := 'D';
          else 
          studentType := 'I';
          end if;
          close check_for_application;          
        END if;
      else 
        studentType := 'I';
      end if;

  RETURN studentType;

  END fz_getIntlorDomStuTyp;

/
